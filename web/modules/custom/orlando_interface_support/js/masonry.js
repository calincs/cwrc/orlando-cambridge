(function ($, Drupal, once) {
  "use strict";

  Drupal.behaviors.oiSupportMansory = {
    attach(context) {
      $(document).ajaxComplete(function(e, r, s) {
        if (typeof s.data !== 'undefined' && r.hasOwnProperty('responseJSON') && Array.isArray(r.responseJSON)) {
          r.responseJSON.forEach(function (data) {
            if (!data.hasOwnProperty('method') || data.method !== 'infiniteScrollInsertView') {
              return;
            }
            once('oiViewsLoadMore', '[data-oi-views-load-more]', context).forEach(
              (element) => {
                $(element).masonry(JSON.parse(element.dataset.masonry));
              },
            );
          });
        }
      });
    },
  };
})(jQuery, Drupal, once);
