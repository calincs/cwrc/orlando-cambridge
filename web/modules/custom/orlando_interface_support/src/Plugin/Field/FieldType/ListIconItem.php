<?php

namespace Drupal\orlando_interface_support\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Plugin implementation of the 'ois_svg_icon' field type.
 *
 * @FieldType(
 *   id = "ois_svg_icon",
 *   label = @Translation("List (svg icon)"),
 *   description = @Translation("This field stores icon machine names from a list and the settings to be used when generating the icon."),
 *   category = @Translation("Text"),
 *   default_widget = "ois_svg_icon_select",
 *   default_formatter = "list_default",
 * )
 */
class ListIconItem extends ListStringItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = parent::defaultStorageSettings();
    $settings['allowed_values'] = static::getAllowedValues();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['value']->setLabel(t('Icon machine name'));
    $properties['settings'] = DataDefinition::create('any')
      ->setLabel(t('Settings'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['settings'] = [
      'description' => 'The icon item settings value.',
      'type' => 'blob',
      'not null' => TRUE,
      'serialize' => TRUE,
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    $element['allowed_values']['#attributes']['disabled'] = TRUE;
    return $element;
  }

  private static function getAllowedValues() {
    return [
      'bibliography' => 'Bibliography',
      'connections' => 'Connections',
      'genre' => 'Genre',
      'organisation' => 'Organisation',
      'pen_nib' => 'Pen-nib',
      'people' => 'People',
      'place' => 'Place',
      'publisher' => 'Publisher',
      'timeline' => 'Timeline',
    ];
  }

}
