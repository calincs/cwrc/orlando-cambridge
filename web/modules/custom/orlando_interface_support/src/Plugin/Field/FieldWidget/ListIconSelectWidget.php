<?php

namespace Drupal\orlando_interface_support\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Plugin implementation of the 'ois_icon_default' widget.
 *
 * @FieldWidget(
 *   id = "ois_svg_icon_select",
 *   label = @Translation("Icon select list"),
 *   field_types = {
 *     "ois_svg_icon"
 *   }
 * )
 */
class ListIconSelectWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $this->required = $element['#required'];
    $this->multiple = $this->fieldDefinition->getFieldStorageDefinition()->isMultiple();
    $this->has_value = isset($items[0]->{$this->column});

    /** @var \Drupal\orlando_interface_support\Plugin\Field\FieldType\ListIconItem $item */
    $item = $items[$delta];
    $settings = $item->toArray()['settings'];
    $element += [
      '#type' => 'fieldset',
      'value' => [
        '#type' => 'select',
        '#title' => $this->t('Icon'),
        '#key_column' => $this->column,
        '#options' => $this->getOptions($items->getEntity()),
        '#default_value' => $this->getSelectedOptions($items),
      ],
      'settings' => [
        '#type' => 'details',
        '#title' => $this->t('SVG Settings'),
        '#tree' => TRUE,
      ],
    ];

    $color_field = [
      '#type' => 'select',
      '#options' => [
        '#ffffff' => $this->t('White'),
        '#000000' => $this->t('Black'),
        '#f6f6f6' => $this->t('Gray 100'),
        '#1d1d1d' => $this->t('Gray 500'),
        '#981915' => $this->t('Red 100'),
        '#6d0a18' => $this->t('Red 200'),
      ],
    ];
    $element['settings']['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $settings['width'] ?? 10,
      '#field_suffix' => 'px',
      '#min' => 10,
      '#max' => 50,
    ];
    $element['settings']['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $settings['height'] ?? 10,
      '#field_suffix' => 'px',
      '#min' => 10,
      '#max' => 50,
    ];
    $element['settings']['main_fill'] = [
      '#type' => 'select',
      '#title' => $this->t('Main fill'),
      '#options' => [
        'none' => $this->t('None'),
        'currentColor' => $this->t('Current color'),
      ],
      '#default_value' => $settings['main_fill'] ?? '',
    ];
    $element['settings']['color_primary'] = [
      '#title' => $this->t('Primary color'),
      '#default_value' => $settings['color_primary'] ?? '',
    ] + $color_field;
    $element['settings']['color_secondary'] = [
      '#title' => $this->t('Secondary color'),
      '#default_value' => $settings['color_secondary'] ?? '',
    ] + $color_field;
    $element['settings']['color_stroke'] = [
      '#title' => $this->t('Stroke color'),
      '#default_value' => $settings['color_stroke'] ?? '',
    ] + $color_field;
    $element['#element_validate'][] = [get_class($this), 'validateElement'];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if ($element['#required'] && $element['value']['#value'] === '_none') {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
      return;
    }
    else {
      // Filter out the 'none' option.
      $value = $element['value']['#value'] !== '_none' ? $element['value']['#value'] : '';
    }

    $settings = [];
    foreach (Element::children($element['settings']) as $setting) {
      $settings[$setting] = Xss::filter($element['settings'][$setting]['#value']);
    }
    $form_state->setValueForElement($element, [
      $element['value']['#key_column'] => $value,
      'settings' => $settings,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $error, array $form, FormStateInterface $form_state) {
    return $element['value'];
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    $empty_label = parent::getEmptyLabel();
    if (!$this->has_value) {
      $empty_label = t('- Select an icon -');
    }
    return $empty_label;
  }

}
