<?php

namespace Drupal\orlando_interface_support\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaInterface;
use Drupal\media\Plugin\Field\FieldFormatter\MediaThumbnailFormatter;

/**
 * Plugin implementation of the 'image_delta_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "oi_media_delta_formatter",
 *   label = @Translation("Media delta (OI)"),
 *   description = @Translation("Display specific deltas of a media field."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MediaDeltaFormatter extends MediaThumbnailFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'deltas' => '0',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['deltas'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delta'),
      '#description' => $this->t('Enter a delta, or a comma-separated list of deltas that should be shown. For example: 0, 1, 4.'),
      '#size' => 10,
      '#default_value' => $this->getSetting('deltas'),
      '#required' => TRUE,
      '#weight' => -20,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = parent::settingsSummary();

    $count = count(explode(',', $settings['deltas']));
    $args = [
      '@deltas' => trim($settings['deltas']),
    ];
    $delta_summary = $this->formatPlural($count, 'Delta: @deltas', 'Deltas: @deltas', $args);
    $summary[] = $delta_summary;

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if (empty($elements = parent::viewElements($items, $langcode))) {
      return $elements;
    }

    $media_items = $this->getEntitiesToView($items, $langcode);
    $entity = $items->getEntity();
    /** @var \Drupal\media\MediaInterface[] $media_items */
    foreach ($media_items as $delta => $media) {
      if (is_numeric($delta)) {
        $elements[$delta]['#link_text'] = $this->getMediaThumbnailLinkedImageText($media, $entity);
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $medias = parent::getEntitiesToView($items, $langcode);

    // Prepare an array of selected deltas from the entered string.
    if (mb_strpos($this->getSetting('deltas'), ',')) {
      $deltas = explode(',', $this->getSetting('deltas'));
      $deltas = array_map('trim', $deltas);
    }
    else {
      $delta = trim($this->getSetting('deltas'));
      $deltas = [$delta];
    }

    foreach (array_keys($medias) as $delta) {
      if (!in_array($delta, $deltas)) {
        unset($medias[$delta]);
      }
    }

    return $medias;
  }

  protected function getMediaThumbnailLinkedImageText(MediaInterface $media, EntityInterface $entity): array {
    $label = [];
    $image_link_setting = $this->getSetting('image_link');
    $text_render_array = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => ['class' => ['tw-sr-only']],
    ];

    if ($image_link_setting == 'content') {
      $label = $text_render_array + [
        '#value' => $entity->label(),
      ];
    }
    elseif ($image_link_setting === 'media') {
      $label = $text_render_array + [
        '#value' => $media->label(),
      ];
    }
    return $label;
  }

}
