<?php

namespace Drupal\orlando_interface_support\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Template\Attribute;

/**
 * @RenderElement("oi_scholarnote_citation_trigger")
 */
class ScholarNoteReferenceTriggerElement extends RenderElement {

  /**
   * @inheritDoc
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#pre_render' => [
        [$class, 'preRenderButton']
      ],
      '#attributes' => [],
      '#target' => 'references',
      '#popper_content_id' => '',
    ];
  }

  public static function preRenderButton(array $element) {
    $attributes = isset($element['#attributes']) ? new Attribute($element['#attributes']) : new Attribute([]);
    if ($element['#target'] === 'references') {
      $value = t('<span class="tw-sr-only">View reference</span>');
      $svg_width = 'tw-w-2.5';
      $svg_path_d = 'M0 8V6.29A2.29 2.29 0 002.29 4H0V0h4v4a4 4 0 01-4 4zm6 0V6.29A2.29 2.29 0 008.29 4H6V0h4v4a4 4 0 01-4 4z';
      $svg_viewBox = '0 0 10 8';
    }
    else {
      $value = t('<span class="tw-sr-only">View scholar note</span>');
      $svg_width = 'tw-w-1.5';
      $svg_path_d = 'M0 2.5v8c0 .83.67 1.5 1.5 1.5h7c.83 0 1.5-.67 1.5-1.5v-8C10 1.67 9.33 1 8.5 1H8V.5a.5.5 0 00-1 0V1H5.5V.5a.5.5 0 00-1 0V1H3V.5a.5.5 0 00-1 0V1h-.5C.67 1 0 1.67 0 2.5zm2.5.5a.5.5 0 00.5-.5V2h1.5v.5a.5.5 0 001 0V2H7v.5a.5.5 0 001 0V2h.5c.28 0 .5.22.5.5v8a.5.5 0 01-.5.5h-7a.5.5 0 01-.5-.5v-8c0-.28.22-.5.5-.5H2v.5c0 .28.22.5.5.5zM2 7.5c0-.28.22-.5.5-.5h5a.5.5 0 010 1h-5a.5.5 0 01-.5-.5zM2.5 9a.5.5 0 000 1h5a.5.5 0 000-1h-5zm5-3h-5a.5.5 0 010-1h5a.5.5 0 010 1z';
      $svg_viewBox = '0 0 10 12';
    }
    $attributes['data-popperjs-enabled'] = 'true';
    $attributes['aria-describedby'] = $element['#popper_content_id'];
    $attributes['data-popperjs-tooltip-id'] = $element['#popper_content_id'];
    $attributes['id'] = $element['#popper_content_id'] . '--button';
    $attributes->addClass('tw-inline-grid tw-place-content-center tw-w-4 tw-h-4 tw-rounded-full tw-bg-gray-100 focus:tw-text-white hover:tw-text-white hover:tw-bg-red-100 focus:tw-bg-red-100');

    $element['#attached']['library'][] = 'orlando_interface/scholarnote-reference';
    $element['button'] = [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $value,
      '#attributes' => $attributes,
    ];
    $element['button']['svg'] = [
      '#type' => 'html_tag',
      '#tag' => 'svg',
      '#attributes' => [
        'class' => [
          'tw-h-2',
          $svg_width,
        ],
        'fill' => 'currentColor',
        'xmlns' => 'http://www.w3.org/2000/svg',
        'viewBox' => $svg_viewBox,
      ],
      'path' => [
        '#type' => 'html_tag',
        '#tag' => 'path',
        '#attributes' => [
          'fill-rule' => 'evenodd',
          'clip-rule' => 'evenodd',
          'd' => $svg_path_d,
        ],
      ],
    ];
    return $element;
  }

}
