/**
 * @file
 *
 * Attaches behavior for cambridge core api ezproxy checking. This file should
 * only be loaded for anonymous user.
 */

(function (Drupal, drupalSettings, cookies) {
  const excludedPathNames = drupalSettings.cambridgeCoreApi.destinationPath.excludedPathNames;

  Drupal.cambridgeCoreApi = Drupal.cambridgeCoreApi || {};

  /**
   * Gets potential proxies from a url. Inspired by
   * https://github.com/zotero/zotero-connectors/blob/ca7791d95364c6498347e5752049dded8e871187/src/common/proxy.js#L563
   *
   * @param {string} URL
   * @returns {object} {{}}
   */
  Drupal.cambridgeCoreApi.getPotentialProxies = function(URL) {
    let urlToProxy = {};
    urlToProxy[URL] = null;

    // if there is a subdomain that is also a TLD, also test against URI with the domain
    // dropped after the TLD
    // (i.e., www.nature.com.mutex.gmu.edu => www.nature.com)
    const m = /^(https?:\/\/)([^\/]+)/i.exec(URL);
    if (!m) {
      return urlToProxy;
    }

    // First, drop the 0- if it exists (this is an III invention)
    let host = m[2];
    if (host.slice(0, 2) === "0-") host = host.slice(2);
    let hostnameParts = [host.split(".")];

    if (m[1] === 'https://') {
      // try replacing hyphens with dots for https protocol
      // to account for EZProxy HttpsHyphens mode
      hostnameParts.push(host.split('.'));
      hostnameParts[1].splice(0, 1, ...(hostnameParts[1][0].replace(/-/g, '.').split('.')));
    }

    for (let i=0; i < hostnameParts.length; i++) {
      let parts = hostnameParts[i];
      // If hostnameParts has two entries, then the second one is with replaced hyphens
      let dotsToHyphens = i === 1;
      // skip the lowest level subdomain, domain and TLD
      for (let j = 1; j < parts.length-2; j++) {
        // if a part matches a TLD, everything up to it is probably the true URL
        if (Drupal.cambridgeCoreApi.tlds[parts[j].toLowerCase()]) {
          const properHost = parts.slice(0, j+1).join(".");
          // protocol + properHost + /path
          const properURL = m[1]+properHost+URL.substr(m[0].length);
          const proxyHost = parts.slice(j+1).join('.');
          urlToProxy[properURL] = {scheme: '%h.' + proxyHost + '/%p', dotsToHyphens};
        }
      }
    }
    return urlToProxy;
  };

  /**
   * Checks if url is a proxy and the presence of ezproxy cookie.
   *
   * @param {string} url The url to check.
   * @returns {boolean} true or false depending on the url and cookie.
   */
  Drupal.cambridgeCoreApi.isEzproxy = function (url) {
    const proxies = Drupal.cambridgeCoreApi.getPotentialProxies(url);
    let hasProxy = false;
    Object.keys(proxies).forEach(function (key) {
      if (!proxies[key]) {
        return;
      }
      hasProxy = proxies[key].hasOwnProperty('scheme');
    });
    return hasProxy && !!cookies.get('ezproxy');
  };

  if (!Drupal.cambridgeCoreApi.isEzproxy(window.location.origin)) {
    return;
  }
  // When the path name is among the excluded paths we also don't do
  // anything.
  if (excludedPathNames.includes(window.location.pathname)) {
    return;
  }

  const siteFrontPage = drupalSettings.cambridgeCoreApi.ezproxy.siteFrontPage;
  let url = window.location.origin;
  if (window.location.pathname === '/' || window.location.pathname === siteFrontPage) {
    url = `${url}/user/login?destination=${siteFrontPage}`;
  }
  else {
    url = `${url}/user/login?destination=${window.location.pathname}`;
  }
  // Redirect to login page. Using replace method to avoid the user to
  // click the previous browser button.
  window.location.replace(url);

})(Drupal, drupalSettings, window.Cookies);
