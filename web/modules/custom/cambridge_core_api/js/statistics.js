(function ($, Drupal, drupalSettings) {

  Drupal.cambridgeCoreApi = Drupal.cambridgeCoreApi || {};

  Drupal.cambridgeCoreApi.statistics = {
    postData: function (url, data) {
      data.user_agent = window.navigator.userAgent;
      data.client_user_agent = data.user_agent;
      $.ajax({
        type: 'POST',
        cache: false,
        url: url,
        data: data,
      });
    }
  };

  $(document).ready(function () {
    const url = drupalSettings?.cambridgeCoreApi?.statistics?.url,
      data = drupalSettings?.cambridgeCoreApi?.statistics?.data;
    if (url && data) {
      Drupal.cambridgeCoreApi.statistics.postData(url, data);
    }
  });
})(jQuery, Drupal, drupalSettings);
