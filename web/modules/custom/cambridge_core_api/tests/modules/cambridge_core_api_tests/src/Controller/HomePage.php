<?php

namespace Drupal\cambridge_core_api_tests\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a home page controller.
 */
class HomePage extends ControllerBase {

  /**
   * The content callback.
   *
   * @return string[]
   *   The content.
   */
  public function content() {
    return [
      '#markup' => '<p>'. $this->t('Home page content') . '</p>',
    ];
  }

}
