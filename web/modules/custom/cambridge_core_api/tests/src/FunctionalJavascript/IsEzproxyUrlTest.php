<?php

namespace Drupal\Tests\cambridge_core_api\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

class IsEzproxyUrlTest extends WebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['cambridge_core_api'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';


  /**
   * Tests the js method to check if url is ezproxy.
   *
   * @param string $url
   *   The url to test.
   * @param string $cookie
   *   The fake cookie to set.
   * @param string $message
   *   The feedback message.
   * @param string $expected_assert
   *   The expected result bool.
   *
   * @dataProvider providerUrlsAndCookies
   */
  public function testIsEzproxy(string $url, string $cookie, string $message, string $expected_assert) {
    $this->drupalGet('<front>');
    $session = $this->getSession();
    $web_assert = $this->assertSession();

    $is_ezproxy_script = <<<JS
      if ("$cookie" !== '') {
        window.Cookies.set('ezproxy', "$cookie");
      }
      const isEzproxy = Drupal.cambridgeCoreApi.isEzproxy("$url");
      if (String(isEzproxy) === "$expected_assert") {
        let div = document.createElement('div');
        div.dataset.cambridgeCoreApiEzproxyTestMessage = 'true';
        div.append("$message");
        let body = document.querySelector('body');
        body.append(div);
      }
JS;

    $session->executeScript($is_ezproxy_script);
    $this->assertNotNull($web_assert->waitForElement('css', 'div[data-cambridge-core-api-ezproxy-test-message]'), $message);
    $web_assert->pageTextContains($message);
  }

  public function providerUrlsAndCookies() {
    $data = [];

    $url = 'https://orlando-cambridge-org.subzero.lib.uoguelph.ca';
    $cookie = 'randomCookieToEnsureThatTheCookieIsSet';
    $message = 'Valid hyphenated ezproxy with cookie available';
    $data[] = [$url, $cookie, $message, 'true'];

    $url = 'https://orlando-cambridge-org.login.ezproxy.library.ualberta.ca';
    $data[] = [$url, $cookie, $message, 'true'];

    $url = 'https://orlando.cambridge.org.proxy.example.com';
    $message = 'Valid dotted proxy with cookie available';
    $data[] = [$url, $cookie, $message, 'true'];

    $url = 'https://orlando-cambridge-org.subzero.lib.uoguelph.ca/';
    $cookie = '';
    $message = 'Valid hyphenated ezproxy but no cookie available';
    $data[] = [$url, $cookie, $message, 'false'];

    $url = 'https://orlando-cambridge-org.login.ezproxy.library.ualberta.ca';
    $data[] = [$url, $cookie, $message, 'false'];

    $url = 'https://orlando.cambridge.org.proxy.example.com';
    $message = 'Valid dotted proxy but no cookie available';
    $data[] = [$url, $cookie, $message, 'false'];

    $url = 'https://dev-orlando-2.lincsproject.ca';
    $message = 'Regular url with no proxy and no cookie available';
    $data[] = [$url, $cookie, $message, 'false'];

    $url = 'https://orlando.cambridge.org';
    $data[] = [$url, $cookie, $message, 'false'];

    return $data;
  }

}
