<?php

namespace Drupal\Tests\cambridge_core_api\Functional;

use Drupal\Tests\BrowserTestBase;

class IpAutoLoginTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable for this test.
   *
   * @var string[]
   */
  protected static $modules = [
    'cambridge_core_api',
    'cambridge_core_api_tests',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->config('cambridge_core_api.authentication_settings')
      ->set('uri', 'https://example.com/v1')
      ->set('api_key', 'owi7vmcI5IU1K4RpFAa0paNuJPwaPkKX')
      ->set('product_id', '8476351GNCSCFLJX91QFCXLVOBNOO1AB')
      ->set('ip_auto_login', TRUE)
      ->save();
  }

  public function testIpAutoLogin() {
    $assert_session = $this->assertSession();
    $this->drupalGet('/cambridge-core-api-tests-home-page');
    $this->drupalGet('/user');
    $assert_session->responseContains('University of Alberta');
  }

}
