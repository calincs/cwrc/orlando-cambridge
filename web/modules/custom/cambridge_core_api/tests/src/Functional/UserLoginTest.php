<?php

namespace Drupal\Tests\cambridge_core_api\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Ensure that login works as expected.
 *
 * @group cambridge_core_api
 */
class UserLoginTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable for this test.
   *
   * @var string[]
   */
  protected static $modules = [
    'block',
    'cambridge_core_api',
    'cambridge_core_api_tests',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
    // Add default settings.
    $this->config('cambridge_core_api.authentication_settings')
      ->set('uri', 'https://example.com/v1')
      ->set('api_key', 'owi7vmcI5IU1K4RpFAa0paNuJPwaPkKX')
      ->set('product_id', '8476351GNCSCFLJX91QFCXLVOBNOO1AB')
      ->save();
  }

  /**
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testBasicMethod() {
    $assert_session = $this->assertSession();
    $this->drupalGet('user/login');
    // The login form won't trigger the login based on the api if no settings
    // are available, hence depends on config:cambridge_core_api.authentication_settings, and
    // its cache tags should be present.
    $assert_session->responseHeaderContains('X-Drupal-Cache-Tags', 'config:cambridge_core_api.authentication_settings');

    $edit = ['name' => 'test.orlando@example.com', 'pass' => 'demopassowrd'];
    $this->submitForm($edit, 'Log in');
    // Check if the page contains identityId from the api fixture response.
    $assert_session->responseContains('test orlando');
  }

}
