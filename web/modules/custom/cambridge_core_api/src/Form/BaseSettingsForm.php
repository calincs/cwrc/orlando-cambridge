<?php

namespace Drupal\cambridge_core_api\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BaseSettingsForm extends ConfigFormBase {

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  /**
   * Constructs a SettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the configuration object factory.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    parent::__construct($config_factory);
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_tags.invalidator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->getConfigName());
    $form['uri'] = [
      '#type' => 'url',
      '#title' => $this->t('Server'),
      '#required' => TRUE,
      '#default_value' => $config->get('uri'),
      '#description' => $this->t('Please add the server URI without the endpoint path at the end and trailing slash. For example: "/auth/getWithEntitlement" shouldn\'t be included in the path above.'),
      '#weight' => 1,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
      '#weight' => 2,
    ];

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#description' => $this->t("If enable the system will record all the payload being sent to CUP and it's response. When disabled only failed request are logged."),
      '#default_value' => $config->get('debug_mode'),
      '#weight' => 97,
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['#weight'] = 99;
    return $form;
  }

  private function getConfigName(): string {
    return $this->getEditableConfigNames()[0];
  }

}
