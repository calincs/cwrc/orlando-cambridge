<?php

namespace Drupal\cambridge_core_api\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StatisticsSettingsForm extends BaseSettingsForm {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected ConditionManager $manager;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected ContextRepositoryInterface $contextRepository;

  /**
   * Constructs a SettingsForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the configuration object factory.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $manager
   *   The ConditionManager for building the visibility UI.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheTagsInvalidatorInterface $cache_tags_invalidator, ExecutableManagerInterface $manager, ContextRepositoryInterface $context_repository) {
    parent::__construct($config_factory, $cache_tags_invalidator);
    $this->manager = $manager;
    $this->contextRepository = $context_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_tags.invalidator'),
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cambridge_core_api.statistics_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cambridge_core_api_statistics_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cambridge_core_api.statistics_settings');
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable statistics collection'),
      '#description' => $this->t('When disabled no stats feature will work.'),
      '#default_value' => !empty($config->get('enabled')),
      '#weight' => 0,
    ];
    $form['application_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('application_id'),
      '#weight' => 3,
    ];
    $form['platform_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Platform Identifier'),
      '#required' => TRUE,
      '#default_value' => $config->get('platform_id'),
      '#weight' => 4,
    ];

    $form['visibility'] = [
      '#type' => 'container',
      '#weight' => 98,
      '#tree' => TRUE,
    ] + $this->buildVisibilityInterface([], $form_state, $config);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $this->validateVisibility($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('cambridge_core_api.statistics_settings');
    $form_state->cleanValues();

    $enabled = (bool) $form_state->getValue('enabled');
    $uri = $form_state->getValue('uri');
    $api_key = $form_state->getValue('api_key');
    $application_id = $form_state->getValue('application_id');
    $platform_id = $form_state->getValue('platform_id');
    $visibility = $this->submitVisibility($form, $form_state);

    $old_enabled = $config->get('enabled');
    $config->set('enabled', $enabled);
    $old_uri = $config->get('uri');
    $config->set('uri', $uri);
    $old_api_key = $config->get('api_key');
    $config->set('api_key', $api_key);
    $old_application_id = $config->get('application_id');
    $config->set('application_id', $application_id);
    $old_platform_id = $config->get('platform_id');
    $config->set('platform_id', $platform_id);
    $old_visibility = $config->get('visibility');
    $config->set('visibility', $visibility);

    $config->set('debug_mode', !empty($form_state->getValue('debug_mode')));

    if ($enabled !== $old_enabled || $uri != $old_uri || $api_key != $old_api_key || $application_id != $old_application_id || $platform_id != $old_platform_id || $visibility !== $old_visibility) {
      $this->cacheTagsInvalidator->invalidateTags($config->getCacheTags());
    }

    $config->save();
  }

  /**
   * Helper function for building the visibility UI form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the visibility UI added in.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function buildVisibilityInterface(array $form, FormStateInterface $form_state, $config) {
    $form['visibility_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Visibility'),
      '#parents' => ['visibility_tabs'],
    ];

    $definitions = $this->manager->getFilteredDefinitions('cambridge_core_api_statistics', $this->contextRepository->getAvailableContexts(), ['config' => $config]);
    $supported_condition_ids = [
      'user_role',
      'request_path',
      'entity_bundle:node',
      'entity_bundle:taxonomy_term',
      'entity_bundle:bibcite_reference',
    ];
    $visibility = $config->get('visibility');
    foreach ($definitions as $condition_id => $definition) {
      if (!in_array($condition_id, $supported_condition_ids)) {
        continue;
      }

      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->manager->createInstance($condition_id, $visibility[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'visibility_tabs';
      $form[$condition_id] = $condition_form;
    }
    return $form;
  }

  /**
   * Helper function to independently validate the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function validateVisibility(array $form, FormStateInterface $form_state) {
    // Validate visibility condition settings.
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      if (array_key_exists('negate', $values)) {
        $form_state->setValue(['visibility', $condition_id, 'negate'], (bool) $values['negate']);
      }

      // Allow the condition to validate the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->validateConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));
    }
  }

  /**
   * Helper function to independently submit the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitVisibility(array $form, FormStateInterface $form_state) {
    $visibility = [];
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      // Allow the condition to submit the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->submitConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));

      $visibility[$condition_id] = $condition->getConfiguration();
    }
    return $visibility;
  }

}
