<?php

namespace Drupal\cambridge_core_api\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AuthenticationSettingsForm extends BaseSettingsForm {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cambridge_core_api.authentication_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cambridge_core_api_authentication_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cambridge_core_api.authentication_settings');

    $form['product_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Product ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('product_id'),
      '#weight' => 3,
    ];

    $form['ip_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable IP Login'),
      '#description' => $this->t('When an anonymous user accesses the login page of the site, the module will attempt to log them in automatically using their ip address.'),
      '#default_value' => $config->get('ip_login'),
      '#weight' => 4,
    ];

    $form['ip_auto_login'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable IP Auto Login'),
      '#description' => $this->t('Enable attempt to auto login based on the anonymous user ip address.'),
      '#default_value' => !empty($config->get('ip_auto_login')),
      '#weight' => 5,
    ];

    $form['login_message_prompt'] = [
      '#title' => $this->t('Login message prompt'),
      '#type' => 'text_format',
      '#default_value' => $config->get('login_message_prompt')['value'] ?? 'Please click on login button to check your access and/or enter your credentials. Alternatively if you don’t have access, please click on “Subscribe”.',
      '#rows' => 6,
      '#format' => $config->get('login_message_prompt')['format'] ?? filter_default_format(),
      '#editor' => TRUE,
      '#weight' => 6,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('cambridge_core_api.authentication_settings');
    $form_state->cleanValues();

    $uri = $form_state->getValue('uri');
    $api_key = $form_state->getValue('api_key');
    $product_id = $form_state->getValue('product_id');
    $ip_login = $form_state->getValue('ip_login');
    $ip_auto_login = $form_state->getValue('ip_auto_login');
    $login_message_prompt = $form_state->getValue('login_message_prompt');

    $old_uri = $config->get('uri');
    $config->set('uri', $uri);
    $old_api_key = $config->get('api_key');
    $config->set('api_key', $api_key);
    $old_product_id = $config->get('product_id');
    $config->set('product_id', $product_id);
    $old_ip_login = $config->get('ip_login');
    $config->set('ip_login', $ip_login);
    $old_ip_auto_login = $config->get('ip_auto_login');
    $config->set('ip_auto_login', ($ip_auto_login && $ip_login));
    $old_login_message_prompt = $config->get('login_message_prompt');
    $config->set('login_message_prompt', $login_message_prompt);

    $config->set('debug_mode', !empty($form_state->getValue('debug_mode')));

    if ($uri != $old_uri || $api_key != $old_api_key || $product_id != $old_product_id || $ip_login != $old_ip_login || $ip_auto_login != $old_ip_auto_login || $login_message_prompt !== $old_login_message_prompt) {
      $tags = array_merge(['cambridge_core_api_login'], $config->getCacheTags());
      $this->cacheTagsInvalidator->invalidateTags($tags);
    }

    $config->save();
  }

}
