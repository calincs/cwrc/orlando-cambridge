<?php

namespace Drupal\cambridge_core_api\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * The event dispatched when collecting the statistics api metadata.
 */
class StatisticsApiMetadataCollectorEvent extends Event {

  protected array $metadata;

  protected RouteMatchInterface $routeMatch;

  public function __construct(array $metadata, RouteMatchInterface $route_match) {
    $this->metadata = $metadata;
    $this->routeMatch = $route_match;
  }

  public function setProperty(string $property, string $value) {
    $this->metadata[$property] = $value;
  }

  public function getProperty(string $property) {
    return $this->metadata[$property] ?? '';
  }

  public function getMetadata(): array {
    return $this->metadata;
  }

  public function getRouteMatch(): RouteMatchInterface {
    return $this->routeMatch;
  }

}
