<?php

namespace Drupal\cambridge_core_api\Event;

use Drupal\Component\EventDispatcher\Event;

class StatisticsApiInputFilterArgumentsGetterEvent extends Event {

  protected array $arguments;

  public function __construct(array $arguments) {
    $this->arguments = $arguments;
  }

  public function setArgument(string $property, array $value) {
    $this->arguments[$property] = $value;
  }

  public function getArgumentValue(string $property): array {
    return $this->arguments[$property] ?? [];
  }

  public function getArguments(): array {
    return $this->arguments;
  }

}
