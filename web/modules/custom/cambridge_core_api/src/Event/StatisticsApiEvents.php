<?php

namespace Drupal\cambridge_core_api\Event;

final class StatisticsApiEvents {

  /**
   * The name of the event triggerred when collecting the stats api metadata.
   *
   * @see \Drupal\cambridge_core_api\Event\StatisticsApiMetadataCollectorEvent
   *
   * @Event
   *
   * @var string
   */
  const METADATA_COLLECTOR = 'cambridge_core_api.statistics_metadata_collector';

  const INPUT_FILTER_ARGUMENTS_GETTER = 'cambridge_core_api.statistics_input_filter_arguments_getter';

}
