<?php

namespace Drupal\cambridge_core_api\PageCache;

use Drupal\cambridge_core_api\StackMiddleware\IpAutoLoginUserDataRequester;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\Routing\RouteMatch;
use Symfony\Component\HttpFoundation\Request;

/**
 * A policy evaluating to static::DENY for the user login page.
 */
class UserLoginPageRequests implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request) {
    // Ensure that we don't deliver cached pages for users who can be logged in
    // automatically.
    if ($request->attributes->get(IpAutoLoginUserDataRequester::AUTO_LOGIN_USER_DATA_ATTR_KEY)) {
      return static::DENY;
    }
    $route_match = RouteMatch::createFromRequest($request);
    if ($route_match && $route_match->getRouteName() === 'user.login') {
      return self::DENY;
    }
  }

}
