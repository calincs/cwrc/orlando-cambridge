<?php

namespace Drupal\cambridge_core_api\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelResponseSubscriber implements EventSubscriberInterface {

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 0];
    return $events;
  }

  public function onKernelResponse(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    // Setting a cookie to allow us to do not try to auto login if an Ip login
    // was logged out.
    $ip_login_logged_out = $event->getRequest()->attributes->get('cambridgeCoreApiIpLoginLoggedOut');
    if ($ip_login_logged_out !== NULL) {
      // This cookie flag is used in
      // Drupal\cambridge_core_api\StackMiddleware\IpAutoLoginUserDataRequester::handle
      // to prevent the auto login to be attempted.
      $event->getResponse()->headers->setCookie(new Cookie('cambridgeCoreApiIpLoginLoggedOut', $ip_login_logged_out));
    }
  }

}
