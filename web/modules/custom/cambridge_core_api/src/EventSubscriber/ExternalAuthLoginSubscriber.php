<?php

namespace Drupal\cambridge_core_api\EventSubscriber;

use Drupal\cambridge_core_api\ExternalAuthRunnerInterface;
use Drupal\cambridge_core_api\IpLogin;
use Drupal\cambridge_core_api\IpLogoutInterface;
use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthLoginEvent;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExternalAuthLoginSubscriber implements EventSubscriberInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The drupal kernel service.
   *
   * @var \Drupal\Core\DrupalKernelInterface
   */
  protected $drupalKernel;

  /**
   * The IP logout service.
   *
   * @var \Drupal\cambridge_core_api\IpLogoutInterface
   */
  protected $ipLogout;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The private temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStoreFactory;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Http\RequestStack|\Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs the ExternalAuthLoginSubscriber instance.
   *
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\DrupalKernelInterface $drupal_kernel
   *   The drupal kernel service.
   * @param \Drupal\cambridge_core_api\IpLogoutInterface $ip_logout
   *   The IP logout service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user account.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   */
  public function __construct(RequestStack $request_stack, DrupalKernelInterface $drupal_kernel, IpLogoutInterface $ip_logout, AccountInterface $current_user, PrivateTempStoreFactory $private_temp_store_factory) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->drupalKernel = $drupal_kernel;
    $this->ipLogout = $ip_logout;
    $this->currentUser = $current_user;
    $this->requestStack = $request_stack;
    // The temp store is not being instantiated here just in case this class is
    // created while the user is not authenticated yet.
    $this->privateTempStoreFactory = $private_temp_store_factory;
  }

  /**
   * Runs on login to redirect the user to the front page if needed.
   *
   * @param \Drupal\externalauth\Event\ExternalAuthLoginEvent $event
   *   The event.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function onLogin(ExternalAuthLoginEvent $event) {
    $provider = ExternalAuthRunnerInterface::PROVIDER;
    if ($event->getProvider() !== $provider) {
      return;
    }

    // Let flag that we are logging in so that the autoLogout method below won't
    // be triggered right away.
    $temp_store = $this->privateTempStoreFactory->get($provider);
    $temp_store->set('logging_in', TRUE);

    $this->updateUserIpAddress($event->getAccount());
  }

  /**
   * Runs on kernel request to attempt to logout the user who have used their ip
   * address to login but it has since changed.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function autoLogout(RequestEvent $event) {
    if ($this->currentUser->isAnonymous()) {
      return;
    }

    $temp_store = $this->privateTempStoreFactory->get(ExternalAuthRunnerInterface::PROVIDER);
    // Let not try the auto-logout if this request is coming from login.
    if (!$temp_store->get('logging_in')) {
      $master_request = $this->requestStack->getMainRequest();
      $this->ipLogout->run($event->getRequest(), $master_request);
    }
    else {
      // Making sure that we set the logging_in flag back to false, in order to
      // allow the auto logout to work for subsequent requests.
      $temp_store->set('logging_in', FALSE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ExternalAuthEvents::LOGIN => ['onLogin'],
      KernelEvents::REQUEST => ['autoLogout'],
    ];
  }

  /**
   * Updates the IP address of the user who just log in.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account who just logged in.
   */
  private function updateUserIpAddress(UserInterface $account) {
    /** @var \Drupal\externalauth\AuthmapInterface $auth_map */
    $auth_map = \Drupal::service('externalauth.authmap');
    $user_data = _cambridge_core_api_load_user_external_authentication_metadata($account);
    if(!$user_data || empty($user_data['ip_login'])) {
      return;
    }

    $request_ip = IpLogin::getIpFromRequest($this->currentRequest);
    if ($request_ip === $user_data['ip_login']) {
      return;
    }

    // Updating user ip in case it changed.
    $user_data['ip_login'] = $request_ip;
    $provider = ExternalAuthRunnerInterface::PROVIDER;
    $auth_map->save($account, $provider, $account->getAccountName(), $user_data);
  }

}
