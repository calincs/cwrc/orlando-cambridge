<?php

namespace Drupal\cambridge_core_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

abstract class ApiCaller {

  /**
   * The api config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a UserAuth.
   *
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param ClientInterface $http_client
   *   The HTTP client to get the user api data with.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerInterface $logger) {
    $this->config = $this->getConfig($config_factory);
    $this->httpClient = $http_client;
    $this->logger = $logger;
  }

  public function getData(array $payload): ?\stdClass {
    $uri = $this->config->get('uri') . $this->getUriEndpointPath();

    try {
      $user_agent = '';
      if (!empty($payload['user_agent'])) {
        $user_agent = $payload['user_agent'];
        unset($payload['user_agent']);
      }

      $instantiated_payload = $this->instantiatePayload($payload);
      $params = Json::encode($instantiated_payload);
      $api_key = $this->config->get('api_key');

      if ($this->config->get('debug_mode')) {
        // Masking the password for logging purpose.
        if (!empty($instantiated_payload->password)) {
          $instantiated_payload->password = '******';
        }
        $this->logger->warning('Request payload: @payload', [
          '@payload' => Json::encode($instantiated_payload),
        ]);
      }
      $headers = [
        'Accept' => 'application/json',
        'X-API-KEY' => $api_key,
        'Content-Type' => 'application/json',
      ];

      if ($user_agent) {
        $headers['User-Agent'] = $user_agent;
      }

      $response = new ApiResponse($this->httpClient
        ->request('POST', $uri, ['headers' => $headers, 'body' => $params]));
      return $this->processResponseData($response->getData());
    }
    catch (\Exception $e) {
      $message = '%type: @message in %function (line %line of %file).';
      $variables = Error::decodeException($e);
      $this->logger->log(RfcLogLevel::ERROR, $message, $variables);
      return NULL;
    }
  }

  /**
   * Process the response data.
   *
   * @param $data
   *   The data to process.
   *
   * @return mixed
   *   The processed data.
   */
  protected function processResponseData($data) {
    return $data;
  }

  /**
   * Instantiates a payload array.
   *
   * @param array $payload
   *   The payload array.
   *
   * @return object
   *   The instantiated payload.
   */
  abstract protected function instantiatePayload(array $payload): \stdClass;

  /**
   * Gets the uri endpoint path.
   *
   * @return string
   *   The endpoint.
   */
  abstract protected function getUriEndpointPath(): string;

  /**
   * Gets the immutable configs.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The retrieve config.
   */
  abstract protected function getConfig(ConfigFactoryInterface $config_factory): ImmutableConfig;

}
