<?php

namespace Drupal\cambridge_core_api\StackMiddleware;

use Drupal\cambridge_core_api\IpLogin;
use Drupal\cambridge_core_api\UserAuthInterface;
use Drupal\Core\Config\BootstrapConfigStorageFactory;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides HTTP middleware to implement IP auto login.
 *
 * The role of this "data requester" middleware is to determine if a user can
 * be logged in automatically based on their current IP. If so, a request
 * attribute is set and IpAutoLogin middleware does the actual login, because
 * that needs to happen after the Drupal kernel is initialized by
 * \Drupal\Core\StackMiddleware\KernelPreHandle.
 */
class IpAutoLoginUserDataRequester implements HttpKernelInterface {

  use ContainerAwareTrait;

  const AUTO_LOGIN_USER_DATA_ATTR_KEY = 'cca_ip_auto_login_user_data';

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * The session service name.
   *
   * @var string
   */
  protected string $sessionServiceName;

  /**
   * Constructs an EarlyIpLoginMiddleware.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param string $service_name
   *   The name of the session service, defaults to "session".
   */
  public function __construct(HttpKernelInterface $http_kernel, string $service_name = 'session') {
    $this->httpKernel = $http_kernel;
    $this->sessionServiceName = $service_name;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    // The config factory might not be ready yet, so we bypass the container by
    // using the bootstrap factory.
    $config_storage = BootstrapConfigStorageFactory::get();
    $settings = $config_storage->read('cambridge_core_api.authentication_settings');
    // Bail out early if we already determined that we cannot auto-login or if
    // this is not a web request or When the user is attempting to log in let
    // not disrupt the flow
    if (
      empty($settings['ip_auto_login']) ||
      $this->isExcludedPath($request) ||
      // When this session cookies contains a flag which shows that we have
      // already attempted to auto login, the user let not try again.
      $request->cookies->get('cambridgeCoreApiIpAutoLoginAttempted', NULL) ||
      // When this session cookies contains a flag which shows that this user
      // has manually logged out let not attempt to auto log in them.
      $request->cookies->get('cambridgeCoreApiIpLoginLoggedOut', NULL) ||
      PHP_SAPI === 'cli'
    ) {
      return $this->httpKernel->handle($request, $type, $catch);
    }

    $uid = NULL;
    if ($type === self::MASTER_REQUEST) {
      // Put the current (unprepared) request on the stack, so we can
      // initialize the session.
      $this->container->get('request_stack')->push($request);

      $session = $this->container->get($this->sessionServiceName);
      $session->start();
      $uid = $session->get('uid');

      // Remove the unprepared request from the stack,
      // \Drupal\Core\StackMiddleware\KernelPreHandle::handle() adds the proper
      // one.
      $this->container->get('request_stack')->pop();
    }

    // Do nothing if the user is logged in.
    if ($uid) {
      return $this->httpKernel->handle($request, $type, $catch);
    }

    if ($user_data = $this->getUserData($request)) {
      $request->attributes->set(self::AUTO_LOGIN_USER_DATA_ATTR_KEY, $user_data);
    }

    $result = $this->httpKernel->handle($request, $type, $catch);
    // If we couldn't retrieve the user data above using the ip address, set a
    // session cookie so that we don't repeat the user IP check for this
    // browser session.
    if (!$user_data) {
      $result->headers->setCookie(new Cookie('cambridgeCoreApiIpAutoLoginAttempted', 1));
    }
    return $result;
  }

  private function getUserData(Request $request): ?\stdClass {
    if (!($ip = IpLogin::getIpFromRequest($request))) {
      return NULL;
    }

    // Until we figured out how to set the HTTP_X_FORWARDED_FOR during tests, we
    // need to comment the if statement above and uncomment the hard-coded ip
    // address below; otherwise the tests will fail.
    // $ip = '127.0.0.1';

    $user_auth = $this->container->get('cambridge_core_api.user_auth');
    $payload = [
      'authMethod' => UserAuthInterface::IP_METHOD,
      'ip' => $ip,
    ];
    if (!($user_data = $user_auth->authenticate($payload))) {
      return NULL;
    }
    $user_data->ipLogin = $ip;
    return  $user_data;
  }

  private function isExcludedPath(Request $request): bool {
    # Do not start any login checks on the following pages
    $urls = ['/user/login', '/user/register', '/user/logout', '/user/reset', '/admin/'];
    $is_excluded_path = FALSE;
    foreach($urls as $url) {
      if(strpos($request->getPathInfo(), $url) !== FALSE) {
        $is_excluded_path = TRUE;
        break;
      }
    }
    return $is_excluded_path;
  }

}
