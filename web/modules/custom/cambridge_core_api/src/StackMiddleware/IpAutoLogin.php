<?php

namespace Drupal\cambridge_core_api\StackMiddleware;

use Drupal\cambridge_core_api\ExternalAuthRunnerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides an HTTP middleware to implement IP auto login.
 */
class IpAutoLogin implements HttpKernelInterface {

  use ContainerAwareTrait;

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * Constructs an IpLoginMiddleware.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
    if ($user_data = $request->attributes->get(IpAutoLoginUserDataRequester::AUTO_LOGIN_USER_DATA_ATTR_KEY)) {
      $request->attributes->remove(IpAutoLoginUserDataRequester::AUTO_LOGIN_USER_DATA_ATTR_KEY);
      /** @var \Drupal\cambridge_core_api\ExternalAuthRunnerInterface $external_auth_runner */
      $external_auth_runner = $this->container->get('cambridge_core_api.external_auth');
      $external_auth_runner->loginRegister($user_data->identityId, $user_data);
    }
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
