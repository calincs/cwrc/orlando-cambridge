<?php

namespace Drupal\cambridge_core_api;

use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints\Ip;
use Symfony\Component\Validator\Validation;

class IpLogin implements IpLoginInterface {

  protected $currentRequest;

  protected $userAuth;

  protected $externalAuthRunner;

  public function __construct(RequestStack $request_stack, UserAuthInterface $user_auth, ExternalAuthRunnerInterface $external_auth) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->userAuth = $user_auth;
    $this->externalAuthRunner = $external_auth;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(): ?AccountInterface {
    if (!($ip = static::getIpFromRequest($this->currentRequest))) {
      return NULL;
    }

    $payload = [
      'authMethod' => UserAuthInterface::IP_METHOD,
      'ip' => $ip,
    ];
    if (!($user_data = $this->userAuth->authenticate($payload))) {
      return NULL;
    }
    $user_data->ipLogin = $ip;
    return $this->externalAuthRunner->loginRegister($user_data->identityId, $user_data);
  }

  /**
   * Gets the IP address from the HTTP_X_FORWARDED_FOR then validates it before
   * returning it.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current requests.
   *
   * @return string
   *   The validated IP address or an empty string.
   */
  public static function getIpFromRequest(Request $request) {
    $ip = $request->server->get('HTTP_X_FORWARDED_FOR');
    $validator = Validation::createValidatorBuilder()->getValidator();
    $constraint = new Ip();
    return $validator->validate($ip, [$constraint])->count() == 0 ? $ip : '';
  }

}
