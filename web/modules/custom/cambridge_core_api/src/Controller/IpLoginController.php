<?php

namespace Drupal\cambridge_core_api\Controller;

use Drupal\cambridge_core_api\ExternalAuthRunner;
use Drupal\cambridge_core_api\ExternalAuthRunnerInterface;
use Drupal\cambridge_core_api\IpLoginInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\user\Form\UserLoginForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class IpLoginController extends ControllerBase {

  protected $ipLogin;

  protected $currentRequest;

  public function __construct(IpLoginInterface $ip_login, RequestStack $request_stack) {
    $this->ipLogin = $ip_login;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cambridge_core_api.ip_login'),
      $container->get('request_stack')
    );
  }

  /**
   * Handle login for the cambridge core api.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The login form or redirect the user logged in through ip to home page.
   */
  public function login() {
    $config = $this->config('cambridge_core_api.authentication_settings');
    if (!empty($config->get('ip_login'))) {
      $account = $this->ipLogin->authenticate();
      if ($account) {
        $url = $this->getDestinationRedirectUrl();
        return $this->redirect($url->getRouteName(), $url->getRouteParameters(), $url->getOptions());
      }
    }

    return $this->formBuilder()->getForm(UserLoginForm::class);
  }

  /**
   * Gets the form's redirect URL from 'destination' provide in the request.
   *
   * @return \Drupal\Core\Url|null
   *   The redirect URL or NULL if dialog should just be closed.
   */
  protected function getDestinationRedirectUrl() {
    // \Drupal\Core\Routing\RedirectDestination::get() cannot be used directly
    // because it will use <current> if 'destination' is not in the query
    // string.
    if ($this->currentRequest->query->has('destination') && $destination = $this->getRedirectDestination()->get()) {
      return Url::fromUserInput('/' . ltrim($destination, '/'));
    }
    return Url::fromRoute('<front>');
  }

}
