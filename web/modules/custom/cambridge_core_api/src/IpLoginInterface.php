<?php

namespace Drupal\cambridge_core_api;

use Drupal\Core\Session\AccountInterface;

interface IpLoginInterface {

  /**
   * Authenticates the user using their ip address and the cambridge core api.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The authenticated account if login succeeded or null otherwise.
   */
  public function authenticate(): ?AccountInterface;

}
