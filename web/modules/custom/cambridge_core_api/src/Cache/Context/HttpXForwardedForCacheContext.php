<?php

namespace Drupal\cambridge_core_api\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\Context\RequestStackCacheContextBase;

/**
 * Defines the HttpXForwardedForCacheContext service, for "per http X forwarded
 * for IP address" caching.
 *
 * Cache context ID: 'http_x_forwarded_for'.
 */
class HttpXForwardedForCacheContext extends RequestStackCacheContextBase implements CacheContextInterface {

  /**
   * @inheritDoc
   */
  public static function getLabel() {
    return t('HTTP X Forwarded For IP address');
  }

  /**
   * @inheritDoc
   */
  public function getContext() {
    $this->requestStack->getCurrentRequest()->server->get('HTTP_X_FORWARDED_FOR');
  }

  /**
   * @inheritDoc
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
