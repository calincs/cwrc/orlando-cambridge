<?php

namespace Drupal\cambridge_core_api;

interface ExternalAuthRunnerInterface {

  const PROVIDER = 'cambridge_core_api';

  public function loginRegister(string $username, \stdClass $user_data);

}
