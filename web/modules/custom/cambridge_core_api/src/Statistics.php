<?php

namespace Drupal\cambridge_core_api;

use Drupal\cambridge_core_api\Event\StatisticsApiEvents;
use Drupal\cambridge_core_api\Event\StatisticsApiInputFilterArgumentsGetterEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Statistics extends ApiCaller {

  protected EventDispatcherInterface $eventDispatcher;

  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerInterface $logger, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($config_factory, $http_client, $logger);
    $this->eventDispatcher = $event_dispatcher;
  }

  public function post(array $payload) {
    return $this->getData($payload);
  }

  /**
   * {@inheritdoc}
   */
  protected function processResponseData($data) {
    return (object) parent::processResponseData($data);
  }

  public function getInputFilterArguments(): array {
    $arguments = [
      'identities' => [
        'flags' => FILTER_REQUIRE_ARRAY,
        'required' => TRUE,
      ],
      'used_identities' => [
        'flags' => FILTER_REQUIRE_ARRAY,
        'required' => TRUE,
      ],
      'authentication_methods' => [
        'flags' => FILTER_REQUIRE_ARRAY,
        'required' => TRUE,
      ],
      'session_id' => [
        'filter' => FILTER_SANITIZE_ENCODED,
        'flags'  => FILTER_REQUIRE_SCALAR,
        'required' => TRUE,
      ],
      'event_code' => [
        'filter' => FILTER_DEFAULT,
        'flags'  => FILTER_REQUIRE_SCALAR,
        'required' => TRUE,
      ],
      'client_ip' => [
        'filter' => FILTER_VALIDATE_IP,
        'flags'  => FILTER_REQUIRE_SCALAR,
        'required' => TRUE,
      ],
      'event_context' => [
        'filter' => FILTER_DEFAULT,
        'flags'  => FILTER_REQUIRE_SCALAR,
      ],
      'product_id' => [
        'filter' => FILTER_DEFAULT,
        'flags'  => FILTER_REQUIRE_SCALAR,
      ],
      'client_user_agent' => [
        'filter' => FILTER_DEFAULT,
        'flags'  => FILTER_REQUIRE_SCALAR,
      ],
    ];
    $event = new StatisticsApiInputFilterArgumentsGetterEvent($arguments);
    $this->eventDispatcher->dispatch($event, StatisticsApiEvents::INPUT_FILTER_ARGUMENTS_GETTER);
    return $event->getArguments();
  }

  public function httpPostHasAllRequiredValues($values, array $arguments): bool {
    $required_args = array_keys(array_filter($arguments, fn($value) => !empty($value['required'])));
    return !array_diff($required_args, array_keys($values));
  }

  protected function instantiatePayload(array $payload): \stdClass {
    foreach ($payload as $key => $value) {
      if (!$value || is_bool($value)) {
        unset($payload[$key]);
        continue;
      }

      $transformed_key = $this->transformSnakeCaseToCamelCase($key);
      if ($transformed_key !== $key) {
        $payload[$transformed_key] = $value;
        unset($payload[$key]);
      }
    }

    $payload['applicationId'] = $this->config->get('application_id');
    $payload['platformIdentifier'] = $this->config->get('platform_id');
    $payload += [
      'shareId' => '',
    ];

    return (object) $payload;
  }

  /**
   * {@inheritdoc}
   */
  protected function getUriEndpointPath(): string {
    return '/v1/events';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfig(ConfigFactoryInterface $config_factory): ImmutableConfig {
    return $config_factory->get('cambridge_core_api.statistics_settings');
  }

  private function transformSnakeCaseToCamelCase(string $input, string $separator = '_'): string {
    $input = strtolower($input);
    if (strpos($input, $separator) === FALSE) {
      return $input;
    }
    return lcfirst(str_replace($separator, '', ucwords($input, $separator)));
  }

}
