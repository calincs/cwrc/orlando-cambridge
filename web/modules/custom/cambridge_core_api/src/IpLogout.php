<?php

namespace Drupal\cambridge_core_api;

use Drupal\Core\Session\AccountInterface;
use Drupal\externalauth\AuthmapInterface;
use Symfony\Component\HttpFoundation\Request;

class IpLogout implements IpLogoutInterface {

  protected $currentUser;

  protected $authMap;

  public function __construct(AccountInterface $current_user, AuthmapInterface $auth_map) {
    $this->currentUser = $current_user;
    $this->authMap = $auth_map;
  }

  public function run(Request $request, Request $master_request) {
    if(!($auth_data = $this->authMap->getAuthData($this->currentUser->id(), ExternalAuthRunnerInterface::PROVIDER))) {
      return;
    }

    $user_data = unserialize($auth_data['data']);
    if (empty($user_data['ip_login'])) {
      return;
    }

    // Logout the user if their ip address have changed and let them log in
    // again.
    if (IpLogin::getIpFromRequest($request) !== $user_data['ip_login']) {
      // @see cambridge_core_api_user_logout(), why we are setting this flag
      // here.
      $master_request->attributes->set('cambridgeCoreApiIpAutoLogoutTriggerred', TRUE);
      user_logout();
    }
  }

}
