<?php

namespace Drupal\cambridge_core_api;

use Drupal\cambridge_core_api\EventSubscriber\ExternalAuthLoginSubscriber;
use Drupal\Core\Session\AccountInterface;
use Drupal\externalauth\ExternalAuthInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ExternalAuthRunner implements ExternalAuthRunnerInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  public function __construct(RequestStack $request_stack, ExternalAuthInterface $external_auth, AccountInterface $current_user) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->externalAuth = $external_auth;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function loginRegister(string $username, \stdClass $user_data) {
    if ($this->currentUser->isAuthenticated()) {
      return $this->currentUser;
    }

    $account_data = [
      'name' => $username,
      'mail' => $username . '@example.com',
    ];
    $authmap_data = [
      'identity_type' => $user_data->identityType,
      'auth_method' => $user_data->authMethod,
      'identity_country_code' => $user_data->identityCountryCode ?? '',
      'identity_display_name' => $user_data->identityDisplayName ?? '',
      'ip_login' => $user_data->ipLogin ?? '',
      'associated_identities' => $user_data->associatedIdentities ?? [],
      'used_identities' => $user_data->usedIdentities ?? [],
    ];

    return $this->externalAuth->loginRegister($username, self::PROVIDER, $account_data, $authmap_data);
  }

}
