<?php

namespace Drupal\cambridge_core_api;

use Drupal\cambridge_core_api\Event\StatisticsApiEvents;
use Drupal\cambridge_core_api\Event\StatisticsApiMetadataCollectorEvent;
use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\MissingValueContextException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class StatisticsAttachmentsApplier implements ContainerInjectionInterface {

  use ConditionAccessResolverTrait;

  /**
   * The statistics config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The event dispatcher used to notify subscribers.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The cambridge_core_api module path.
   *
   * @var string
   */
  protected string $modulePath;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected ContextRepositoryInterface $contextRepository;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected ConditionManager $conditionManager;

  /**
   * The plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected ContextHandlerInterface $contextHandler;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  protected AliasManagerInterface $pathAliasManager;

  /**
   * Constructs the StatisticsAttachmentsApplier instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stacks.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Current route match service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher used to notify subscribers of config import events.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $manager
   *   The ConditionManager for building the visibility UI.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   The ContextHandler for applying contexts to conditions properly.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RequestStack $request_stack,
    RouteMatchInterface $route_match,
    EventDispatcherInterface $event_dispatcher,
    ModuleExtensionList $extension_list_module,
    ExecutableManagerInterface $manager,
    ContextRepositoryInterface $context_repository,
    ContextHandlerInterface $context_handler,
    AliasManagerInterface $path_alias_manager
  ) {
    $this->config = $config_factory->get('cambridge_core_api.statistics_settings');
    $this->requestStack = $request_stack;
    $this->routeMatch = $route_match;
    $this->eventDispatcher = $event_dispatcher;
    $this->modulePath = $extension_list_module->getPath('cambridge_core_api');
    $this->conditionManager = $manager;
    $this->contextRepository = $context_repository;
    $this->contextHandler = $context_handler;
    $this->pathAliasManager = $path_alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('current_route_match'),
      $container->get('event_dispatcher'),
      $container->get('extension.list.module'),
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('context.handler'),
      $container->get('path_alias.manager')
    );
  }

  public function run(array &$attachments) {
    $attachments['#cache']['tags'] = Cache::mergeTags($attachments['#cache']['tags'] ?? [], $this->config->getCacheTags());
    if (!$this->shouldApplyAttachment()) {
      return;
    }

    $session_id = session_id();
    $metadata = $session_id ? $this->collectApiMetadata() : [];
    if (!$metadata || empty($metadata['event_code'])) {
      return;
    }

    $current_request = $this->requestStack->getCurrentRequest();
    $attachments['#attached']['library'][] = 'cambridge_core_api/statistics';
    $attachments['#attached']['drupalSettings']['cambridgeCoreApi']['statistics'] = [
      'data' => [
        'session_id' => $session_id,
        'client_ip' => IpLogin::getIpFromRequest($current_request),
      ] + $metadata,
      'url' => $current_request->getBasePath() . '/' . $this->modulePath . '/statistics.php',
    ];
  }

  /**
   * Checks if the tracking should be visible to the visited page.
   *
   * @return bool
   *   TRUE if visible, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function shouldApplyAttachment(): bool {
    if (!$this->config->get('enabled') || !$this->config->get('application_id') || !$this->config->get('platform_id') || !$this->config->get('api_key') || !$this->config->get('uri')) {
      return FALSE;
    }
    $visibility_conditions = $this->config->get('visibility');
    if (!$visibility_conditions) {
      return TRUE;
    }

    $conditions = [];
    $missing_context_conditions = [];
    $missing_value_conditions = [];
    foreach ($visibility_conditions as $condition_id => $configuration) {
      $condition = $this->conditionManager->createInstance($condition_id, $configuration);
      if ($condition instanceof ContextAwarePluginInterface) {
        try {
          $contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
          $this->contextHandler->applyContextMapping($condition, $contexts);
        }
        catch (MissingValueContextException $e) {
          $missing_value_conditions[$condition_id] = $condition;
        }
        catch (ContextException $e) {
          $missing_context_conditions[$condition_id] = $condition;
        }
      }
      $conditions[$condition_id] = $condition;
    }

    if ($missing_context_conditions) {
      $access = AccessResult::forbidden()->setCacheMaxAge(0);
    }
    elseif ($missing_value_conditions) {
      $conditions_to_evaluate = array_diff_key($conditions, $missing_value_conditions);
      $allow = $this->resolveConditions($conditions_to_evaluate, 'and') === TRUE;
      $reason = "One of the visibility conditions ('%s') denied access.";
      $access = $allow ? AccessResult::allowed() : AccessResult::forbidden(sprintf($reason, implode("', '", array_keys($conditions_to_evaluate))));
    }
    elseif ($this->resolveConditions($conditions, 'and') !== FALSE) {
      $access = AccessResult::allowed();
    }
    else {
      $reason = count($conditions) > 1
        ? "One of the visibility conditions ('%s') denied access."
        : "Something else visibility condition '%s' denied access.";
      $access = AccessResult::forbidden(sprintf($reason, implode("', '", array_keys($conditions))));
    }

    $this->mergeCacheabilityFromConditions($access, $conditions);
    return $access->isAllowed();
  }

  /**
   * Merges cacheable metadata from conditions onto the access result object.
   *
   * @param \Drupal\Core\Access\AccessResult $access
   *   The access result object.
   * @param \Drupal\Core\Condition\ConditionInterface[] $conditions
   *   List of visibility conditions.
   */
  protected function mergeCacheabilityFromConditions(AccessResult $access, array $conditions) {
    foreach ($conditions as $condition) {
      if ($condition instanceof CacheableDependencyInterface) {
        $access->addCacheTags($condition->getCacheTags());
        $access->addCacheContexts($condition->getCacheContexts());
        $access->setCacheMaxAge(Cache::mergeMaxAges($access->getCacheMaxAge(), $condition->getCacheMaxAge()));
      }
    }
  }

  /**
   * Collects the metadata needed by the api for the current request.
   *
   * @return array
   *   The metadata.
   */
  protected function collectApiMetadata(): array {
    $current_user = $this->currentUser();
    if (!$current_user->isAuthenticated() || !($user_data = _cambridge_core_api_load_user_external_authentication_metadata($current_user))) {
      return [];
    }

    // Collect identities and used identities metadata for external the
    // external user. If not available, don't collect another metadata.
    if (empty($user_data['associated_identities']) || empty($user_data['used_identities'])) {
      return [];
    }

    $current_request = $this->requestStack->getCurrentRequest();
    $query_string = $current_request->getQueryString();
    $current_path = $current_request->getPathInfo();
    $event_context = $this->pathAliasManager->getAliasByPath($current_path);;
    if ($query_string) {
      $event_context .= "?${query_string}";
    }
    $default_metadata = [
      'identities' => $user_data['associated_identities'],
      'used_identities' => $user_data['used_identities'],
      'authentication_methods' => [[
        'type' => strtolower($user_data['auth_method'] === UserAuthInterface::BASIC_METHOD ? 'personal' : $user_data['auth_method']),
        'ids' => $user_data['associated_identities'],
      ]],
      'event_context' => $event_context,
    ];
    $event = new StatisticsApiMetadataCollectorEvent($default_metadata, $this->routeMatch);
    $this->eventDispatcher->dispatch($event, StatisticsApiEvents::METADATA_COLLECTOR);
    return $event->getMetadata();
  }

  protected function currentUser(): AccountInterface {
    if (!isset($this->currentUser)) {
      $this->currentUser = \Drupal::currentUser();
    }
    return $this->currentUser;
  }

}
