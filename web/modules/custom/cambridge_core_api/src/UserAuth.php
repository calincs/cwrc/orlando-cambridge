<?php

namespace Drupal\cambridge_core_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Gets user authentication data based on provided payload.
 */
class UserAuth extends ApiCaller implements UserAuthInterface {

  /**
   * {@inheritdoc}
   */
  public function authenticate(array $payload): ?\stdClass {
    $data = $this->getData($payload);
    $warning_message = '';
    if (!$data) {
      $warning_message = 'We got an empty response using the following parameters: <pre>@payload</pre>';
    }
    elseif (empty($data->productEntitlements)) {
      $warning_message = 'We got an empty product entitlements using the following parameters: <pre>@payload</pre>';
    }

    if (!$warning_message) {
      // Storing authMethod as part of the returned data.
      $data->authMethod = $payload['authMethod'];
      return $data;
    }

    // Masking the password for logging purpose.
    if ($payload['authMethod'] === UserAuthInterface::BASIC_METHOD && !empty($instantiated_payload->password)) {
      $payload->password = '******';
    }
    $this->logger->warning($warning_message, [
      '@payload' => Json::encode($payload),
    ]);
    // Returning null to make the authentication to fail.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function processResponseData($data) {
    if (!($data = parent::processResponseData($data))) {
      return NULL;
    }

    if (!is_array($data)) {
      $data = [$data];
    }

    $associated_identities = [];
    $used_identities = [];
    $processed_data = NULL;
    foreach ($data as $identity) {
      $identity = (object) $identity;
      // Instantiating the processed data variable with the first identity.
      if (!$processed_data) {
        $processed_data = $identity;
      }

      // Building the associated identity array to be stored in the main data
      // object.
      if (!in_array($identity->identityId, $associated_identities, TRUE)) {
        $associated_identities[] = $identity->identityId;
      }
      foreach ((array)($identity->associatedIdentities ?? []) as $associated_identity) {
        if (!in_array($associated_identity, $associated_identities, TRUE)) {
          $associated_identities[] = $associated_identity;
        }
      }

      // Extracting the product entitlements identity id and store them as
      // used identities so that they can be used by the static API.
      foreach ((array)($identity->productEntitlements ?? []) as $product_entitlement) {
        $product_entitlement = (object) $product_entitlement;
        $product_entitlement_identity_id = $product_entitlement->identityId ?? '';
        if ($product_entitlement_identity_id && !in_array($product_entitlement_identity_id, $used_identities, TRUE)) {
          $used_identities[] = $product_entitlement_identity_id;
        }
      }
    }

    // The response data need to have at least one identity within a product
    // entitlement in order for us to allow access.
    if (!$used_identities) {
      return NULL;
    }

    $processed_data->associatedIdentities = $associated_identities;
    $processed_data->usedIdentities = $used_identities;
    return $processed_data;
  }

  /**
   * {@inheritdoc}
   */
  protected function instantiatePayload(array $payload): \stdClass {
    $payload += [
      'productId' => $this->config->get('product_id'),
    ];
    return (object) $payload;
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfig(ConfigFactoryInterface $config_factory): ImmutableConfig {
    return $config_factory->get('cambridge_core_api.authentication_settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function getUriEndpointPath(): string {
    return UserAuthInterface::ENDPOINT;
  }

}
