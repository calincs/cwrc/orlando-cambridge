<?php

namespace Drupal\cambridge_core_api;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * The Api response wrapper.
 */
class ApiResponse extends Response {

  /**
   * The data.
   *
   * @var object|Null
   */
  protected $data;

  /**
   * Constructs an ApiResponse.
   *
   * @param ResponseInterface $response
   *   A response.
   */
  public function __construct(ResponseInterface $response) {
    parent::__construct($response->getStatusCode(), $response->getHeaders(), $response->getBody(), $response->getProtocolVersion(), $response->getReasonPhrase());
    $this->decodeJsonResponse();
  }

  /**
   * Gets the response data.
   *
   * @return Null|object
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Decodes the response json.
   *
   * @return $this|null
   */
  private function decodeJsonResponse() {
    $response_body = $this->getBody()->getContents();
    if (empty($response_body)) {
      // @todo check if we should throw an exception.
      return $this;
    }

    try {
      $this->data = Json::decode($response_body);
      if (is_string($this->data)) {
        $this->data = Json::decode($this->data);
      }
    }
    catch (\Exception $e) {
      watchdog_exception('cambridge_core_api', $e);
    }
    return $this;
  }

}
