<?php

namespace Drupal\cambridge_core_api\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block to display the current user organization image.
 *
 * @Block(
 *   id = "cambridge_core_api_oi",
 *   admin_label = @Translation("Organization Image"),
 * )
 */
class OrganizationImage extends BlockBase {

  /**
   * @inheritDoc
   */
  public function build() {
    return [
      '#theme' => 'cambridge_core_api_organization_image',
    ];
  }

}
