<?php

/**
 * @file
 * Post update functions for the Cambridge Core API.
 */

/**
 * Implements hook_post_update_NAME() for rename_default_config_object.
 *
 * Rename "cambridge_core_api.cambridge_core_api.settings" to "cambridge_core_api.authentication_settings".
 */
function cambridge_core_api_post_update_rename_default_config_object(&$sandbox) {
  $old_name = 'cambridge_core_api.settings';
  $new_name = 'cambridge_core_api.authentication_settings';
  \Drupal::configFactory()->rename($old_name, $new_name);
}

/**
 * Implements hook_post_update_NAME() for disable_stats_collection_by_default.
 *
 * Disable the statistics collection by default.
 */
function cambridge_core_api_post_update_disable_stats_collection_by_default(&$sandbox) {
  if ($config = \Drupal::service('config.factory')->getEditable('cambridge_core_api.statistics_settings')) {
    $config->set('enabled', FALSE);
    $config->save();
  }
}
