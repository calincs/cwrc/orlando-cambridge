<?php

/**
 * @file
 * Handles stats post of supported route via AJAX with minimal bootstrap.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

chdir('../../../');

$autoloader = require_once 'autoload.php';

$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();
$container = $kernel->getContainer();

/** @var \Drupal\cambridge_core_api\Statistics $statistics */
$statistics = $container->get('cambridge_core_api.statistics');
$arguments = $statistics->getInputFilterArguments();

$data = filter_input_array(INPUT_POST, $arguments);

if ($statistics->httpPostHasAllRequiredValues($data, $arguments)) {
  $container->get('request_stack')->push(Request::createFromGlobals());
  $statistics->post($data);
}
