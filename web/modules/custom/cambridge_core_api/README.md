# Cambridge Core API

A module which integrates drupal login authentication with the Cambridge Core
API authentication and statistics.

## Dependencies

1. Drupal core ^8.8 || ^9
2. [composer require drupal/externalauth](https://drupal.org/project/externalauth)
3. For:
   1. Authentication: A valid api server uri endpoint, api key and product id.
   2. Statistics: A valid api server uri endpoint, api key, application id,
      platform id and product id.

## Installation and configuration

1. [Enable the module](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules#s-step-2-enable-the-module)
2. Navigate to the module configuration page:
   1. at `/admin/config/people/cambridge-core-api-authentication` and set up the
      requested information for the authentication feature
   2. at `/admin/config/system/cambridge-core-api-statistics` to set up the
      statistics related features
3. Enjoy

## Statistics implementation and extensibility

The statistics collection feature is disable by default. It can be enabled and
disabled at the settings page.

Apart from api related information such as application id, platform identifier
and product id, the statistics feature also send the current user session id,
client ip address, identity, used identity, an empty string share id and an
event code.

- `sessionId`: is collected using the regular php [`session_id()`](https://www.php.net/manual/en/function.session-id.php).
              This module in its current implementation is not starting a
              session. This means that if a session id is not found, no stats
              will be collected. For Drupal, it implies that no stats collected
              for anonymous users.
- `identity` and `usedIdentity`: when a user is authenticated/logged in, and they
              have authenticated using this module authentication feature,
              their authentication `identityId` is sent to statistics as
              identity and used identity.
- `eventCode`: this module provides a way for other modules to set the event
              code being sent to the api endpoint. For example:
  - the `orlando_interface_search` module, responsible for implementing search
    features, set the event code value to `SE-RE` when a user perform a search
    and visit the search page.
  - the `orlando_inteface_ingestion` module, that implement features related to
    author profiles, people, organizations and bibliographies ingestion,
    set the event code to `RV-DI` when a user visit an author profile, a person,
    or an organization page. This module also set `RV-DI` as event code when a
    user view one of the author profile tabs such as page author profile
    connections or timeline.
  - finally there is also a javascript script, implemented in the site main
    theme, that send statistics data when a user click on the button to
    download the author profile source xml. The event code set is `DL-DI`.

## Issues

For any issues or feature request go to
https://gitlab.com/calincs/cwrc/orlando-cambridge/-/issues
