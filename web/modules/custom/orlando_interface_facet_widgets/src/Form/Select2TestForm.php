<?php


namespace Drupal\orlando_interface_facet_widgets\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class Select2TestForm extends FormBase {

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'orlando_interface_facet_widgets__select2_test_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('basex_facet_mapping');
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $tag_mapping */
    $tag_mapping = $storage->load('tag');
    $form['data'] = [
      '#type' => 'select2',
      '#title' => $this->t('Data'),
      '#options' => $tag_mapping->getFacetWidget()->getOptions(),
      '#multiple' => TRUE,
      '#default_value' => ['AREA', 'AUTHORITY'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => ['tw-mt-5'],
      ],
    ];
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $demo = 'boom';
  }

}
