<?php

namespace Drupal\orlando_interface_facet_widgets\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Site\Settings;
use Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget\OptionsSelect2WithExclude;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;
use Drupal\orlando_interface_search\RequestSwapperTrait;
use Drupal\orlando_interface_search\ResultsRetrieverTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Select2AutocompleteController extends ControllerBase {

  use RequestSwapperTrait;
  use ResultsRetrieverTrait;

  /**
   * The pager parameters service.
   *
   * @var \Drupal\Core\Pager\PagerParametersInterface
   */
  protected $pagerParameters;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $controller = parent::create($container);
    $controller->currentPathStack = $container->get('path.current');
    $controller->requestStack = $container->get('request_stack');
    $controller->router = $container->get('router');
    $controller->pathProcessor = $container->get('path_processor_manager');

    return $controller;
  }

  public function handleAutocomplete(Request $request, FacetMappingInterface $basex_facet_mapping, string $facet_query_key, string $selection_settings_key) {
    $matches['results'] = [];
    if ($input = $request->query->get('q')) {
      $selection_settings = $this->keyValue(OptionsSelect2WithExclude::KEY_VALUE_STORE_COLLECTION)
        ->get($selection_settings_key, FALSE);
      if ($selection_settings === FALSE) {
        throw new AccessDeniedHttpException();
      }
      else {
        $facet_mapping_id = $basex_facet_mapping->id();
        $selection_settings_hash = Crypt::hmacBase64(serialize($selection_settings) . $facet_mapping_id . $facet_query_key, Settings::getHashSalt());
        if (!hash_equals($selection_settings_hash, $selection_settings_key)) {
          // Disallow access when the selection settings hash does not match the
          // passed-in key.
          throw new AccessDeniedHttpException('Invalid selection settings key.');
        }
      }

      /** @var \Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget\OptionsSelect2WithExclude $facet_widget */
      $facet_widget = $basex_facet_mapping->getFacetWidget();
      $all_options = $facet_widget->getOptions();

      // Only try to get the facets results on the search form.
      $typed_string = mb_strtolower($input);
      if (!$selection_settings['is_preview'] && !empty($selection_settings['has_facet_results'])) {
        // Swap the request to the one used by the original path.
        $this->requestSwapWithPath($request, $selection_settings['path']);

        $facet_results = $this->getResultSetsFromRequest($this->requestStack()->getCurrentRequest())
          ->getFacets();
        $results = $facet_results[$facet_mapping_id]['items'] ?? [];
        foreach ($results as $facet_item) {
          /** @var \Drupal\orlando_interface_search\Item\Facet $facet_item */
          $name = $facet_item->getName();
          $display_value = $all_options[$name] ?? '';
          if (!$display_value || !$this->matchFound($selection_settings['match_operator'], mb_strtolower($display_value), $typed_string)) {
            continue;
          }
          $matches['results'][] = [
            'id' => $name,
            'text' => sprintf('%s (%d)', $display_value, $facet_item->getCount()),
          ];
        }

        // Restoring the swapped request.
        $this->restoreRequestStack();
      }
      else {
        foreach ($all_options as $key => $display_value) {
          /** @var \Drupal\orlando_interface_search\Item\Facet $facet_item */
          if (!$this->matchFound($selection_settings['match_operator'], mb_strtolower($display_value), $typed_string)) {
            continue;
          }
          $matches['results'][] = [
            'id' => $key,
            'text' => $display_value,
          ];
        }
      }
    }

    return new JsonResponse($matches);
  }

  protected function matchFound($operator, $haystack, $needle) {
    return !($operator === 'CONTAINS' && strpos($haystack, $needle) === FALSE || ($operator === 'STARTS_WITH' && strpos($haystack, $needle) !== 0));
  }

}
