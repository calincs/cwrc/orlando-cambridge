<?php

namespace Drupal\orlando_interface_facet_widgets\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * @FormElement("oi_fancytree")
 */
class FancyTree extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#options' => [],
      '#flat_tree_option_keys' => [],
      '#process' => [
        [$class, 'processTree'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  public static function processTree(&$element, FormStateInterface $form_state = NULL, &$complete_form = []) {
    if (empty($element['#options'])) {
      return $element;
    }

    $element['#wrapper_attributes']['class'][] = 'tw-bg-white';
    $element['#attached']['library'][] = 'orlando_interface_facet_widgets/fancytree-init';

    $parents = $element['#parents'];
    $id_prefix = implode('-', $parents);
    $wrapper_id = Html::getUniqueId($id_prefix . '--fancy-tree');

    $options = $element['#options'];
    if (!empty($element['#flat_tree_option_keys'])) {
      // This allow the drupal form validator to do not flag values selected as
      // illegal. See Drupal\Core\Form\FormValidator::performRequiredValidation()
      $element['#options'] = array_combine($element['#flat_tree_option_keys'], $element['#flat_tree_option_keys']);
    }

    $element['fancy_tree_filter'] = [
      '#type' => 'container',
      'search' => [
        '#type' => 'textfield',
        '#title' => t('Filter'),
        '#title_display' => 'invisible',
        '#attributes' => [
          'data-fancytree-element-filter-tree-id' => $wrapper_id,
          'autocomplete' => 'off',
          'placeholder' => t('Filter...'),
          'class' => [
            'tw-border-b-0',
            'tw-border-l-0',
            'tw-border-t-0',
            'tw-rounded-b-none',
            'tw-rounded-tr-none',
          ],
        ],
        '#wrapper_attributes' => ['class' => ['tw-flex-grow']],
      ],
      'clear' => [
        '#type' => 'html_tag',
        '#tag' => 'button',
        '#attributes' => [
          'data-fancytree-element-filter-tree-id' => $wrapper_id,
          'class' => [
            'tw-bg-gray-100',
            'tw-flex',
            'tw-items-center',
            'tw-justify-items-center',
            'tw-p-2',
          ],
        ],
        'text' => [
          '#markup' => '<span class="tw-sr-only">' . t('Clear filter') . '</span>',
        ],
        'svg' => [
          '#type' => 'html_tag',
          '#tag' => 'svg',
          '#attributes' => [
            'class' => ['tw-w-6', 'tw-h-6'],
            'fill' => 'currentColor',
            'viewBox' => '0 0 20 20',
            'xmlns' => 'http://www.w3.org/2000/svg',
          ],
          'path' => [
            '#type' => 'html_tag',
            '#tag' => 'path',
            '#attributes' => [
              'fill-rule' => 'evenodd',
              'd' => 'M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z',
              'clip-rule' => 'evenodd',
            ],
          ],
        ],
      ],
      '#attributes' => [
        'class' => [
          'fancy-tree-filter--wrapper',
          'tw-border',
          'tw-flex',
          'tw-rounded-t-sm',
        ],
      ],
      '#suffix' => "<div class='$wrapper_id--matches text-align-center tw-text-gray-450 tw-border-l tw-border-r tw-h-6 tw-px-2'></div>",
    ];

    $default_values = $element['#default_value'] ?? [];
    $element['fancy_tree_data'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $wrapper_id,
        'data-fancytree-element' => 'true',
        'data-fancytree-element-name' => $element['#name'],
        'data-fancytree-source' => Json::encode($options),
        'data-fancytree-source-default-value' => Json::encode($default_values),
        'class' => [
          'tw-border-b',
          'tw-border-l',
          'tw-border-r',
          'tw-max-h-60',
          'tw-overflow-auto',
          'tw-pb-1',
          'tw-px-2',
          'tw-rounded-b-sm',
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $selected_values = [];
    $flat_tree_option_keys = $element['#flat_tree_option_keys'] ?? [];
    if ($flat_tree_option_keys && !empty($input) && is_array($input)) {
      foreach ($input as $selected_value) {
        if (in_array($selected_value, $flat_tree_option_keys)) {
          $selected_values[] = $selected_value;
        }
      }
    }
    return $selected_values;
  }

}
