<?php

namespace Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginBase;

abstract class BaseOptionsWidget extends BaseXFacetWidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'key_tag_name' => '',
      'label_tag_name' => '',
      'element_type' => 'checkboxes',
      'select_all_label' => '',
      'is_multiple' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['key_tag_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key tag name'),
      '#description' => $this->t('The name of the tag containing the key value for the options.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['key_tag_name'],
    ];
    $form['label_tag_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label tag name'),
      '#description' => $this->t('The name of the tag containing the label for the options.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['label_tag_name'],
    ];
    $form['element_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Element type'),
      '#description' => $this->t('The form element type to use.'),
      '#options' => [
        'checkboxes' => $this->t('Checkboxes'),
        'radios' => $this->t('Radios'),
        'select' => $this->t('Select box/dropdown'),
      ],
      '#required' => TRUE,
      '#default_value' => $this->configuration['element_type'],
    ];
    $form['select_all_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select all label'),
      '#default_value' => $this->configuration['select_all_label'],
      '#states' => [
        'visible' => [
          [':input[name="facet_widget_config[element_type]"]' => ['value' => 'checkboxes']],
          [':input[name="facet_widget_config[is_multiple]"]' => ['checked' => TRUE]],
        ],
        'empty' => [
          [':input[name="facet_widget_config[is_multiple]"]' => ['checked' => FALSE]],
        ],
      ],
    ];
    $form['is_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can the user select multiple value?'),
      '#deafult_value' => !empty($this->configuration['is_multiple']),
      '#states' => [
        'checked' => [':input[name="facet_widget_config[element_type]"]' => ['value' => 'checkboxes']],
        'unchecked' => [':input[name="facet_widget_config[element_type]"]' => ['value' => 'radios']],
      ],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function processedMappings() {
    $raw_mappings = $this->facetMapping->getMappings();
    $mappings = $raw_mappings['mapping'] ?? [];
    $mappings = $mappings ?: ($raw_mappings['mappings']['mapping'] ?? []);
    return $mappings ?: [];
  }

  public function buildXQueryString(array $widget_values): string {
    $xquery = '';
    foreach ($widget_values as $query_key => $values) {
      if (!$values || !is_array($values) || !$this->validQueryParameters($values)) {
        continue;
      }
      $xquery .= '"' . $query_key . '":(';
      $xquery .= '"' . implode('","' , $values) . '"';
      $xquery .= '), ';
    }

    return $xquery;
  }

  abstract public function extractSelectedValues($data);

  abstract public function generateSelectedValuesString($values, string $query_key): string;

  abstract public function validQueryParameters(array $values, array $allowed_values = []): bool;

  /**
   * Helper method to uppercase the first character string of a word.
   *
   * @param $string
   *   The string.
   *
   * @return string
   *   The transformed string.
   */
  protected function xucwords($string) {
    $words = explode(' ', $string);
    $new_string = [];

    foreach ($words as $word) {
      if(!preg_match('/^m{0,4}(cm|cd|d?c{0,3})(xc|xl|l?x{0,3})(ix|iv|v?i{0,3})$/', $word)) {
        $word = ucfirst($word);
      }
      else {
        $word = strtoupper($word);
      }
      $new_string[] = $word;
    }

    return implode(' ', $new_string);
  }

}
