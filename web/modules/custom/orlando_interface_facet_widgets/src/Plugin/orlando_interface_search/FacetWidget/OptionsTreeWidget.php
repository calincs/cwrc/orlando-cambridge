<?php

namespace Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Core\Form\FormStateInterface;

/**
 * @OrlandoInterfaceSearchBaseXFacetWidget(
 *   id = "options_tree",
 *   label = @Translation("Options tree widget"),
 * )
 */
class OptionsTreeWidget extends BaseOptionsWidget {

  const ROOT_PARENT = '__root__';

  protected $treeMappings = [];

  /**
   * Array of mapping parents keyed by child mapping ID.
   *
   * @var array
   */
  protected $treeParents = [];

  /**
   * Array of mapping ancestors keyed by parent mapping ID.
   *
   * @var array
   */
  protected $treeChildren = [];

  /**
   * Array of mapping ancestors keyed by parent mapping ID based on the results
   * from basex search.
   *
   * @var array
   */
  protected $treeChildrenFromResults;

  protected $trees = [];

  protected $treeOptions = [];

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'element_type' => 'oi_fancytree',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['element_type']['#options'] = [
      'oi_fancytree' => $this->t('FancyTree'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    $element = parent::formElement($form, $form_state, $results);
    $element['value']['#type'] = $this->configuration['element_type'];
    $element['value']['#key_property_name'] = $this->configuration['key_tag_name'];
    $element['value']['#title_property_name'] = $this->configuration['label_tag_name'];

    $options = $results ? $this->getOptionsByResults($results) : $this->getAllOptions();

    // Set default value.
    $query_key = $this->getQueryKeys()['value'];
    $parameters = $form['#query_parameters'][$query_key] ?? [];
    if ($parameters && $options && $this->validQueryParameters($parameters, $options)) {
      $element['value']['#default_value'] = $parameters;
    }
    $element['value']['#flat_tree_option_keys'] = array_merge(array_keys($this->treeChildren), array_keys($this->treeParents));
    $element['value']['#options'] = $options;
    return $element;
  }

  public function getOptionsByResults(array $results = []) {
    $this->initTreeProperties();

    $label_tag_name = $this->configuration['label_tag_name'];
    $options = [];
    // Used to track children added to a parent and avoid duplicates.
    $children_tracker = [];
    foreach ($results as $facet_result) {
      /** @var \Drupal\orlando_interface_search\Item\Facet $facet_result */
      $key = $facet_result->getName();
      $count = $facet_result->getCount();

      $tree_item = new \stdClass();
      $tree_item->key = $key;
      $tree_item->resultsCount = $count;
      $tree_item->title = $this->t('@name (@count)', [
        '@name' => $this->xucwords($this->treeMappings[$key][$label_tag_name]),
        '@count' => $count,
      ])->render();
      foreach ($this->treeParents[$key] as $parent) {
        if (!isset($options[$parent])) {
          $parent_item = new \stdClass();
          $parent_item->key = $parent;
          $parent_item->title = $this->xucwords($this->treeMappings[$parent][$label_tag_name]);
          $parent_item->expanded = 'true';
          $parent_item->children = [];
          $options[$parent] = $parent_item;
        }

        if (!isset($children_tracker[$parent][$key])) {
          $options[$parent]->children[] = $tree_item;
          $children_tracker[$parent][$key] = $key;
          $this->treeChildrenFromResults[$parent][] = $key;
        }
      }
    }
    return array_values($options);
  }

  public function getAllOptions() {
    $tree = $this->loadTree();
    $label_tag_name = $this->configuration['label_tag_name'];
    $key_tag_name = $this->configuration['key_tag_name'];
    $options = [];
    if ($tree) {
      $index = [];
      // Used to track children added to a parent and avoid duplicates.
      $children_tracker = [];
      foreach ($tree as $item) {
        $item = array_shift($tree);
        $key = $item->{$key_tag_name};
        $tree_item = new \stdClass();
        $tree_item->key = $key;
        $tree_item->title = $this->xucwords($item->{$label_tag_name});

        foreach ($item->parents as $parent) {
          if ($parent === static::ROOT_PARENT) {
            $tree_item->expanded = 'true';
            $options[$key] = $tree_item;
            $index[$key] = &$options[$key];
          }
          elseif (isset($index[$parent])) {
            if (!isset($index[$parent]->children)) {
              $index[$parent]->children = [];
              $children_tracker[$parent] = [];
            }

            if (!isset($children_tracker[$parent][$tree_item->key])) {
              $index[$parent]->children[] = $tree_item;
              $children_tracker[$parent][$key] = $key;
            }
          }
          else {
            array_push($tree, $item);
          }
        }
      }
      unset($index);
      unset($children_tracker);
    }

    return array_values($options);
  }

  public function extractSelectedValues($data) {
    if (!$data || empty($data['value']) || !is_array($data['value'])) {
      return NULL;
    }

    // Initializing the tree structure.
    $this->initTreeProperties();

    // Let remove the fancy tree filter if it exists.
    if (isset($data['value']['fancy_tree_filter'])) {
      unset($data['value']['fancy_tree_filter']);
    }

    $selected_values = [];
    foreach ($data['value'] as $selected_value) {
      // If the selected value is not a parent but it's a children and not
      // added in the selected values yet, then we add it.
      if (!isset($this->treeChildren[$selected_value]) && isset($this->treeParents[$selected_value]) && !in_array($selected_value, $selected_values)) {
        $selected_values[] = $selected_value;
        continue;
      }

      // Check if the parent was selected as value. This deal with skipped
      // parents above by extracting their children as selected values.
      $parents_children = $this->treeChildren[$selected_value] ?? [];
      foreach ($parents_children as $child) {
        if (!in_array($child, $selected_values)) {
          $selected_values[] = $child;
        }
      }
    }

    $facet_id = $this->getFacetMapping()->id();
    return [$facet_id => $selected_values];
  }

  public function validQueryParameters(array $parameters, array $allowed_values = []): bool {
    $this->initTreeProperties();
    // Getting children of the parents. We can't directly use
    // array_keys($this->treeParents) since it also contains root elements.
    $allowed_values = $this->treeChildrenFromResults ?? $this->treeChildren;
    if (empty($allowed_values)) {
      return FALSE;
    }
    // Getting the array of children only.
    $allowed_values = array_values($allowed_values);
    // Flatten the array of children.
    $allowed_values = array_merge(...$allowed_values);
    return !empty(array_intersect($parameters, $allowed_values));
  }

  public function generateSelectedValuesString($values, string $query_key): string {
    if (!$values || !is_array($values)|| !$this->validQueryParameters($values)) {
      return '';
    }

    $label_key_tag = $this->configuration['label_tag_name'];
    $selected_values = [];
    foreach ($values as $value) {
      $selected_values[] = $this->treeMappings[$value][$label_key_tag];
    }
    return implode(', ', array_values($selected_values));
  }

  private function loadTree() {
    $this->initTreeProperties();

    $parent = static::ROOT_PARENT;
    $max_depth = count($this->treeChildren);
    $tree = [];

    // Keeps track of the parents we have to process, the last entry is used
    // for the next processing step.
    $process_parents = [];
    $process_parents[] = $parent;

    // Loops over the parent mappings and adds its children to the tree array.
    // Uses a loop instead of a recursion, because it's more efficient.
    while (count($process_parents)) {
      $parent = array_pop($process_parents);
      // The number of parents determines the current depth.
      $depth = count($process_parents);
      if ($max_depth > $depth && !empty($this->treeChildren[$parent])) {
        $has_children = FALSE;
        $child = current($this->treeChildren[$parent]);
        do {
          if (empty($child)) {
            break;
          }
          $mapping = (object) $this->treeMappings[$child];
          if (isset($this->treeParents[$mapping->machineName])) {
            // Clone the mapping so that the depth attribute remains correct
            // in the event of multiple parents.
            $mapping = clone $mapping;
          }
          $mapping->depth = $depth;
          $machine_name = $mapping->machineName;
          $mapping->parents = $this->treeParents[$machine_name];
          $tree[] = $mapping;
          if (!empty($this->treeChildren[$machine_name])) {
            $has_children = TRUE;

            $process_parents[] = $parent;
            $process_parents[] = $machine_name;

            // Moving the pointer to the first child of the current mapping.
            reset($this->treeChildren[$machine_name]);
            next($this->treeChildren[$parent]);
            break;
          }
        } while ($child = next($this->treeChildren[$parent]));

        if (!$has_children) {
          // We processed all mappings in this hierarchy-level, reset pointer
          // so that this function works the next time it gets called.
          reset($this->treeChildren[$parent]);
        }
      }
    }

    return $tree;
  }

  private function initTreeProperties() {
    if (!empty($this->treeChildren) && !empty($this->treeMappings) && !empty($this->treeParents)) {
      return;
    }

    $this->treeChildren = [];
    $this->treeParents = [];
    $this->treeMappings = [];
    $key_tag_name = $this->configuration['key_tag_name'];
    $parent_tag_name = 'parentMachineNames';
    $parent_elt_tag_name = 'parentMachineName';
    foreach ($this->processedMappings() as $mapping) {
      $machine_name = $mapping[$key_tag_name];
      $parents = [];
      if (!empty($mapping[$parent_tag_name][$parent_elt_tag_name])) {
        $parents = is_string($mapping[$parent_tag_name][$parent_elt_tag_name]) ? [$mapping[$parent_tag_name][$parent_elt_tag_name]] : $mapping[$parent_tag_name][$parent_elt_tag_name];
      }
      if (!$parents) {
        $parents[] = static::ROOT_PARENT;
      }

      foreach ($parents as $parent_machine_name) {
        $this->treeChildren[$parent_machine_name][] = $machine_name;
      }
      $this->treeParents[$machine_name] = $parents;
      // Removing the unneeded keys.
      foreach (['@attributes', $parent_tag_name, 'names'] as $unneeded_key) {
        if (isset($mapping[$unneeded_key])) {
          unset($mapping[$unneeded_key]);
        }
      }
      $this->treeMappings[$machine_name] = $mapping;
    }
  }

}
