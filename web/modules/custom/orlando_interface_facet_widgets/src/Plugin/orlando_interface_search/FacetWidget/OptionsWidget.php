<?php

namespace Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXFacetWidget;

/**
 * @OrlandoInterfaceSearchBaseXFacetWidget(
 *   id = "options",
 *   label = @Translation("Options widget"),
 * )
 */
class OptionsWidget extends BaseOptionsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    $element = parent::formElement($form, $form_state);
    $element['value']['#type'] = $this->configuration['element_type'];
    $all_options = $this->getOptions();
    $has_facet_results = !empty($results);
    $options = $has_facet_results ? $this->extractOptionsFromResults($results, $all_options) : $all_options;
    $element['value']['#options'] = $options;
    // Property element to signal that these element options are coming from
    // the basex facet results. See
    // Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget::processSelect2Autocomplete
    // and
    // Drupal\orlando_interface_facet_widgets\Controller\Select2AutocompleteController::handleAutocomplete on
    // how it's being used.
    $element['value']['#has_facet_results'] = $has_facet_results;

    // Set default value.
    $query_key = $this->getQueryKeys()['value'];
    $selected_values = $form['#query_parameters'][$query_key] ?? [];
    if ($selected_values && $options && $this->validQueryParameters($selected_values, array_keys($options))) {
      $element['value']['#default_value'] = $selected_values;
    }
    return $element;
  }

  public function getOptions() {
    $key_tag_name = $this->configuration['key_tag_name'];
    $label_tag_name = $this->configuration['label_tag_name'];
    $options = [];
    foreach ($this->processedMappings() as $mapping) {
      $mapping_key = $mapping[$key_tag_name] ?? '';
      $mapping_label = $mapping[$label_tag_name] ?? '';
      if ($mapping_key && $mapping_label && is_string($mapping_key) && is_string($mapping_label)) {
        $options[$mapping_key] = $this->xucwords($mapping_label);
      }
    }
    return $options;
  }

  public function extractSelectedValues($data) {
    if (!$data || empty($data['value'])) {
      return NULL;
    }

    $selected_values = $data['value'];
    $facet_id = $this->getFacetMapping()->id();
    return [$facet_id => array_keys(array_filter($selected_values, function($value){
      return !empty($value);
    }))];
  }

  public function generateSelectedValuesString($values, string $query_key): string {
    if (!$values || !is_array($values) || !$this->validQueryParameters($values)) {
      return '';
    }

    $selected_values = array_intersect_key($this->getOptions(), array_flip($values));
    return implode(', ', array_values($selected_values));
  }

  public function validQueryParameters(array $values, array $allowed_values = []): bool {
    $allowed_values = $allowed_values ?: array_keys($this->getOptions());
    return !empty(array_intersect($values, $allowed_values));
  }

  protected function extractOptionsFromResults($results, $all_options) {
    $options = [];
    foreach ($results as $facet_result) {
      /** @var \Drupal\orlando_interface_search\Item\Facet $facet_result */
      $name = $facet_result->getName();
      if (!isset($all_options[$name])) {
        continue;
      }
      $options[$name] = sprintf('%s (%d)', $all_options[$name], $facet_result->getCount());
    }
    return $options;
  }

}
