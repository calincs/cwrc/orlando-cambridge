<?php

namespace Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @OrlandoInterfaceSearchBaseXFacetWidget(
 *   id = "options_select2_exclude",
 *   label = @Translation("Options select2 with exclude widget"),
 * )
 */
class OptionsSelect2WithExclude extends OptionsWidget {

  const KEY_VALUE_STORE_COLLECTION = 'orlando_interface_facet_widgets.autocomplete';

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The key-value store for entity_autocomplete.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $key_value_store
   *   The key value store.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request, KeyValueStoreInterface $key_value_store) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRequest = $request;
    $this->keyValueStore = $key_value_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('keyvalue')->get(self::KEY_VALUE_STORE_COLLECTION)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'autocomplete' => FALSE,
      'match_operator' => 'CONTAINS',
      'width' => '100%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field width'),
      '#default_value' => $this->configuration['width'],
      '#description' => $this->t("Define a width for the select2 field. It can be either 'element', 'computedstyle', 'style', 'resolve' or any possible CSS unit. E.g. 500px, 50%, 200em. See the <a href='https://select2.org/appearance#container-width'>select2 documentation</a> for further explanations."),
      '#required' => TRUE,
      '#size' => '12',
      '#pattern' => "([0-9]*\.[0-9]+|[0-9]+)(cm|mm|in|px|pt|pc|em|ex|ch|rem|vm|vh|vmin|vmax|%)|element|computedstyle|style|resolve|auto|initial|inherit",
    ];
    $form['autocomplete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autocomplete'),
      '#default_value' => $this->configuration['autocomplete'],
      '#description' => $this->t('Options will be lazy loaded. This is recommended for lists with a lot of values.'),
    ];
    $form['match_operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Autocomplete matching'),
      '#default_value' => $this->configuration['match_operator'],
      '#options' => $this->getMatchOperatorOptions(),
      '#description' => $this->t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of entities.'),
      '#states' => [
        'visible' => [
          ':input[name$="facet_widget_config[autocomplete]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    $element = parent::formElement($form, $form_state, $results);
    $autocomplete = !empty($this->configuration['autocomplete']);
    $element['#fieldset_wrapper_attributes']['class'][] = 'tw-space-y-4';
    $element['#attached']['library'][] = 'orlando_interface_facet_widgets/select2-init';

    $element['value']['#type'] = 'select2';
    $element['value']['#label_attributes']['class'][] = 'tw-font-semibold';
    $element['value']['#title'] = $this->t('Include Only');
    $element['value']['#autocomplete'] = $autocomplete;
    $element['value']['#facet_mapping_id'] = $this->getFacetMapping()->id();
    $element['value']['#facet_options'] = $element['value']['#options'] ?? [];
    $element['value']['#multiple'] = !empty($this->configuration['is_multiple']);
    $element['value']['#bordered'] = FALSE;
    $element['value']['#select2'] = [
      'width' => $this->configuration['width'],
    ];
    $element['value']['#attributes']['data-select2-element'] = 'true';
    $element['value']['#attributes']['data-select2-autocomplete'] = (string) $autocomplete;
    $element['value']['#query_key'] = $this->getQueryKeys()['value'];
    $element['value']['#is_preview'] = $form_state === NULL;
    unset($element['value']['#title_display']);

    if ($autocomplete) {
      $element['value']['#autocomplete_options_callback'] = [
        get_class($this),
        'getValidSelectedOptionsAutocomplete'
      ];
      $element['value']['#autocomplete_route_callback'] = [
        $this,
        'processSelect2Autocomplete',
      ];
    }

    $query_key = $this->getQueryKeys()['exclude'];
    $element['exclude'] = $element['value'];
    $element['exclude']['#title'] = $this->t('Exclude');
    $element['exclude']['#default_value'] = [];
    $element['exclude']['#query_key'] = $query_key;
    // Set default value for the exclude.
    $options = $element['exclude']['#options'];
    $selected_values = $form['#query_parameters'][$query_key] ?? [];
    if ($selected_values && $options && $this->validQueryParameters($selected_values, array_keys($options))) {
      $element['exclude']['#default_value'] = $selected_values;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryKeys(): array {
    $facet_id = $this->getFacetMapping()->id();
    return [
      'value' => $facet_id . 'Included',
      'exclude' => $facet_id . 'Excluded',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function extractSelectedValues($data) {
    if (!$data || (empty($data['value']) && empty($data['exclude']))) {
      return NULL;
    }

    $values = [];
    $facet_id = $this->getFacetMapping()->id();
    if (!empty($data['value'])) {
      $values[$facet_id . 'Included'] = array_keys(array_filter($data['value'], function($value){
        return !empty($value);
      }));
    }
    if (!empty($data['exclude'])) {
      $values[$facet_id . 'Excluded'] = array_keys(array_filter($data['exclude'], function($value){
        return !empty($value);
      }));
    }

    return $values;
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getMatchOperatorOptions() {
    return [
      'STARTS_WITH' => $this->t('Starts with'),
      'CONTAINS' => $this->t('Contains'),
    ];
  }

  public function processSelect2Autocomplete(array &$element) {
    $selection_settings = [
      'path' => $this->currentRequest->getUri(),
      'match_operator' => $this->configuration['match_operator'],
      'is_preview' => !empty($element['#is_preview']),
      'has_facet_results' => !empty($element['#has_facet_results']),
    ];

    // Store the selection settings in the key/value store and pass a hashed key
    // in the route parameters.
    $facet_mapping_id = $this->getFacetMapping()->id();
    $data = serialize($selection_settings) . $facet_mapping_id . $element['#query_key'];
    $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());
    if (!$this->keyValueStore->has($selection_settings_key)) {
      $this->keyValueStore->set($selection_settings_key, $selection_settings);
    }
    $element['#autocomplete_route_name'] = 'orlando_interface_facet_widgets.select2_autocomplete';
    $element['#autocomplete_route_parameters'] = [
      'basex_facet_mapping' => $facet_mapping_id,
      'facet_query_key' => $element['#query_key'],
      'selection_settings_key' => $selection_settings_key,
    ];
    return $element;
  }

  public static function getValidSelectedOptionsAutocomplete(array $element, FormStateInterface $form_state) {
    if (empty($element['#facet_options'])) {
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $storage */
      $storage = \Drupal::entityTypeManager()
        ->getStorage('basex_facet_mapping');
      $facet_mapping_id = $element['#facet_mapping_id'];
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping */
      $facet_mapping = $storage->load($facet_mapping_id);
      /** @var \Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget\OptionsSelect2WithExclude $widget */
      $widget = $facet_mapping->getFacetWidget();
      $all_options = $widget->getOptions();
      $allowed_values = array_keys($widget->getOptions());
    }
    else {
      $all_options = $element['#facet_options'];
      $allowed_values = array_keys($element['#facet_options']);
    }

    $options = [];
    if (!empty($values)) {
      $values = is_array($element['#value']) ? $element['#value'] : [$element['#value']];
      $valid_ids = array_intersect($values, $allowed_values);
      foreach ($valid_ids as $id) {
        $options[$id] = $all_options[$id];
      }
    }
    else {
      $options = $all_options;
    }

    return $options;
  }

}
