<?php

namespace Drupal\orlando_interface_facet_widgets\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @OrlandoInterfaceSearchBaseXFacetWidget(
 *   id = "period",
 *   label = @Translation("Period"),
 * )
 */
class PeriodWidget extends OptionsWidget {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'lifespan_width' => '100%',
      'lifespan_is_multiple' => TRUE,
      'monarch_reign_width' => '100%',
      'monarch_reign_is_multiple' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(): bool {
    return is_file($this->getLifespanMappingFileUri()) && is_file($this->getMonarchReignMappingFileUri());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['lifespan_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lifespan Field width'),
      '#default_value' => $this->configuration['lifespan_width'],
      '#description' => $this->t("Define a width for the select2 lifespan field. It can be either 'element', 'computedstyle', 'style', 'resolve' or any possible CSS unit. E.g. 500px, 50%, 200em. See the <a href='https://select2.org/appearance#container-width'>select2 documentation</a> for further explanations."),
      '#required' => TRUE,
      '#size' => '12',
      '#pattern' => "([0-9]*\.[0-9]+|[0-9]+)(cm|mm|in|px|pt|pc|em|ex|ch|rem|vm|vh|vmin|vmax|%)|element|computedstyle|style|resolve|auto|initial|inherit",
    ];

    $form['lifespan_is_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can the user select multiple lifespan?'),
      '#default_value' => !empty($this->configuration['lifespan_is_multiple']),
    ];

    $form['monarch_reign_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Monarch reign field width'),
      '#default_value' => $this->configuration['monarch_reign_width'],
      '#description' => $this->t("Define a width for the select2 lifespan field. It can be either 'element', 'computedstyle', 'style', 'resolve' or any possible CSS unit. E.g. 500px, 50%, 200em. See the <a href='https://select2.org/appearance#container-width'>select2 documentation</a> for further explanations."),
      '#required' => TRUE,
      '#size' => '12',
      '#pattern' => "([0-9]*\.[0-9]+|[0-9]+)(cm|mm|in|px|pt|pc|em|ex|ch|rem|vm|vh|vmin|vmax|%)|element|computedstyle|style|resolve|auto|initial|inherit",
    ];

    $form['monarch_reign_is_multiple'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can the user select multiple monarch reigns?'),
      '#default_value' => !empty($this->configuration['monarch_reign_is_multiple']),
    ];

    $facet_mapping_id = $this->facetMapping->isNew() ? NULL : $this->facetMapping->id();
    if ($facet_mapping_id) {
      $parents_prefix = 'facet_widget_config';
      foreach (['lifespan_facet_mapping_file_upload', 'monarch_reign_facet_mapping_file_upload'] as $mapping_file_upload_key) {
        if ($mapping_file_upload_key === 'lifespan_facet_mapping_file_upload') {
          $description = is_file($this->getLifespanMappingFileUri()) ? $this->t('Replace the existing lifespan mapping file.') : $this->t('Upload the lifespan facet mapping file.');
          $input_name = 'files[' . $parents_prefix . '_lifespan_facet' . ']';
          $title = $this->t('Lifespan facet mapping file');
        }
        else {
          $description = is_file($this->getMonarchReignMappingFileUri()) ? $this->t('Replace the existing monarch reign mapping file.') : $this->t('Upload the monarch reign facet mapping file.');
          $input_name = 'files[' . $parents_prefix . '_monarch_reign_facet' . ']';
          $title = $this->t('Monarch reign facet mapping file');
        }
        $form[$mapping_file_upload_key] = [
          '#type' => 'file',
          '#name' => $input_name,
          '#title' => $title,
          '#multiple' => FALSE,
          '#description' => $description,
          '#upload_validators' => [
            'file_validate_extensions' => ['xml'],
          ],
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $all_files = $this->getRequest()->files->get('files', []);
    $is_new = $this->facetMapping->isNew();
    foreach (['lifespan_facet_mapping_file_upload', 'monarch_reign_facet_mapping_file_upload'] as $mapping_file_upload_key) {
      if ($mapping_file_upload_key === 'lifespan_facet_mapping_file_upload') {
        $uri = $this->getLifespanMappingFileUri();
        $error_msg = $this->t('Lifespan mapping file field is required.');
        $input_name = 'facet_widget_config_lifespan_facet';
      }
      else {
        $uri = $this->getMonarchReignMappingFileUri();
        $error_msg = $this->t('Monarch reign mapping file field is required.');
        $input_name = 'facet_widget_config_monarch_reign_facet';
      }

      // Use this variable to determine if an existing file exists or if we
      // should trigger an error in case the user didn't upload anything.
      $is_valid_file = is_file($uri);
      // Extract the temporary destination path where the was file uploaded.
      if (!empty($all_files[$input_name])) {
        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file_upload */
        $file_upload = $all_files[$input_name];
        if ($file_upload->isValid()) {
          $form_state->setValue($mapping_file_upload_key, $file_upload->getRealPath());
          // Since the use is uploading a file le mark the mapping file is valid.
          $is_valid_file = TRUE;
        }
      }

      // Validate the input.
      if (!$is_new && !$is_valid_file) {
        $form_state->setError($form[$mapping_file_upload_key], $error_msg);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    foreach (['lifespan_facet_mapping_file_upload', 'monarch_reign_facet_mapping_file_upload'] as $mapping_file_upload_key) {
      // Remove the mapping file upload keys from values.
      if (isset($values[$mapping_file_upload_key])) {
        unset($values[$mapping_file_upload_key]);
      }

      $from = $form_state->getValue($mapping_file_upload_key, '');
      if ($from) {
        $uri = $mapping_file_upload_key === 'lifespan_facet_mapping_file_upload' ? $this->getLifespanMappingFileUri() : $this->getMonarchReignMappingFileUri();
        $to = $file_system->getDestinationFilename($uri, FileSystemInterface::EXISTS_REPLACE);
        if ($to) {
          $file_system->moveUploadedFile($from, $to);
          $file_system->chmod($to);
        }
      }
    }

    // Set back the cleaned up values.
    $form_state->setValues($values);
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    $facet_mapping_id = $this->getFacetMapping()->id();
    $historic_period_results = $results[$facet_mapping_id] ?? [];
    $element = parent::formElement($form, $form_state, $historic_period_results);
    $element['value']['#title'] = $this->t('Historic Period');
    unset($element['value']['#title_display']);

    $element['#attached']['library'][] = 'orlando_interface_facet_widgets/period-widget';

    // Building the form input for the author lifetime and monarch reign.
    foreach (['lifespan', 'monarch_reign'] as $item_key) {
      $all_options = $this->getOtherItemOptions($item_key);
      if ($item_key === 'lifespan') {
        $item_period_results = $results['lifespan_' . $facet_mapping_id] ?? [];
        $item_title = $this->t('Lifespan period');
        $is_multiple = !empty($this->configuration['lifespan_is_multiple']);
        $width_conf = $this->configuration['lifespan_width'];
        $placeholder = $this->t('Author lifetime')->render();
      }
      else {
        $item_period_results = $results['monarch_' . $facet_mapping_id] ?? [];
        $item_title = $this->t('Monarch reign period');
        $is_multiple = !empty($this->configuration['monarch_reign_is_multiple']);
        $width_conf = $this->configuration['monarch_reign_width'];
        $placeholder = $this->t('Monarch reign')->render();
      }
      $options = $item_period_results ? $this->extractOptionsFromResults($item_period_results, $all_options) : $all_options;
      // Build the widget input item.
      $element[$item_key] = [
        '#type' => 'select2',
        '#title' => $item_title,
        '#title_display' => 'hidden',
        '#options' => $options,
        '#multiple' => $is_multiple,
        '#select2' => [
          'width' => $width_conf,
          'placeholder' => $placeholder,
        ],
        '#wrapper_attributes' => ['class' => ['tw-mt-5']],
      ];

      // Set default value.
      $query_key = $this->getQueryKeys()[$item_key];
      $original_selected_values = $form['#query_parameters'][$query_key] ?? [];
      if ($original_selected_values && $options) {
        $selected_values = $is_multiple ? $original_selected_values : [$original_selected_values];
        if ($this->validQueryParameters($selected_values, array_keys($options))) {
          $element[$item_key]['#default_value'] = $original_selected_values;
        }
      }
    }

    // Building the form input for the date period inputs.
    $element['date'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => ['class' => ['facet-widget-date', 'tw-mt-5', 'tw-space-y-2']],
    ];
    $query_key = $this->getQueryKeys()['date'];
    // Get the default value for date from input.
    $selected_from_date = $form['#query_parameters'][$query_key]['from'] ?? '';
    $element['date']['from'] = $this->buildDateWidget('from', $selected_from_date);
    // Get the default value for date to input.
    $selected_to_date = $form['#query_parameters'][$query_key]['to'] ?? '';
    $element['date']['to'] = $this->buildDateWidget('to', $selected_to_date);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryKeys(): array {
    return [
      'value' => $this->getFacetMapping()->id(),
      'lifespan' => 'lifespan',
      'monarch_reign' => 'monarch_reign',
      'date' => 'date',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultKeys(): array {
    $facet_mapping_id = $this->getFacetMapping()->id();
    return [
      $facet_mapping_id,
      'lifespan_' . $facet_mapping_id,
      'monarch_' . $facet_mapping_id,
    ];
  }

  public function getLifespanMappingFileUri() {
    $facet_mapping_id = $this->getFacetMapping()->id();
    if (!$facet_mapping_id) {
      return '';
    }

    $filename = 'lifespan_' . $facet_mapping_id . '_mapping.xml';
    return 'private://orlando-2-0-c-modelling/entities/' . $filename;
  }

  public function getMonarchReignMappingFileUri() {
    $facet_mapping_id = $this->getFacetMapping()->id();
    if (!$facet_mapping_id) {
      return '';
    }

    $filename = 'monarch_reign_' . $facet_mapping_id . '_mapping.xml';
    return 'private://orlando-2-0-c-modelling/entities/' . $filename;
  }

  public function getOtherItemMappings(string $item_key) {
    $mappings = [];
    $filename = $item_key === 'lifespan' ? $this->getLifespanMappingFileUri() : $this->getMonarchReignMappingFileUri();
    if ($filename) {
      $mappings = TaxonomyTermRepository::transformXmlToArray($filename);
    }
    return isset($mappings['mappings']) ? $mappings['mappings'] : $mappings;
  }

  public function processedOtherItemMappings(string $item_key) {
    $mappings = $this->getOtherItemMappings($item_key);
    return $mappings['mapping'] ?? [];
  }

  public function getOtherItemOptions(string $item_key) {
    $key_tag_name = $this->configuration['key_tag_name'];
    $label_tag_name = $this->configuration['label_tag_name'];
    $options = [];
    foreach ($this->processedOtherItemMappings($item_key) as $mapping) {
      $key = $mapping[$key_tag_name] ?? '';
      $label = $mapping[$label_tag_name] ?? '';
      if ($key && $label && is_string($key) && is_string($label)) {
        $options[$key] = $this->xucwords($label);
      }
    }
    return $options;
  }

  public function validQueryParameters(array $values, array $allowed_values = []): bool {
    $is_valid = parent::validQueryParameters($values, $allowed_values);
    if (!$is_valid) {
      if ($allowed_values) {
        $is_valid = !empty(array_intersect($values, $allowed_values));
      }
      else {
        $is_valid = !empty(array_intersect($values, array_keys($this->getOtherItemOptions('lifespan'))));
      }
    }
    if (!$is_valid) {
      if ($allowed_values) {
        $is_valid = !empty(array_intersect($values, $allowed_values));
      }
      else {
        $is_valid = !empty(array_intersect($values, array_keys($this->getOtherItemOptions('monarch_reign'))));
      }
    }
    return $is_valid;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileUris(): array {
    if (!$this->isValid()) {
      return [];
    }
    return [
      'lifespan' => $this->getLifespanMappingFileUri(),
      'monarch' => $this->getMonarchReignMappingFileUri(),
    ];
  }

  public function extractSelectedValues($data) {
    if (!$data || (empty($data['value']) && empty($data['lifespan']) && empty($data['date_from']) && empty($data['date_to']))) {
      return NULL;
    }
    $values = [];
    $facet_id = $this->getFacetMapping()->id();

    // Extract the value for the period, lifespan and monarch reign.
    foreach (['value', 'lifespan', 'monarch_reign'] as $period_key) {
      if (!empty($data[$period_key])) {
        if ($period_key === 'value') {
          $is_multiple = $this->configuration['element_type'] === 'checkboxes';
          $value_key = $facet_id;
        }
        else {
          $is_multiple = !empty($this->configuration[$period_key . '_is_multiple']);
          $value_key = $period_key;
        }
        $values[$value_key] = $is_multiple ? array_keys(array_filter($data[$period_key], function($value){
          return !empty($value);
        })) : $data[$period_key];
      }
    }
    if (!empty($data['date']['from']['year'])) {
      $values['date']['from'] = $this->processDatePart($data['date']['from']);
    }
    if (!empty($data['date']['to']['year'])) {
      $values['date']['to'] = $this->processDatePart($data['date']['to'], FALSE);
    }

    return $values;
  }

  public function buildLabelForSelectedValuesString(string $query_key): string {
    if ($query_key === 'date') {
      return $this->t('Date period')->render();
    }
    if ($query_key === 'lifespan') {
      return $this->t('Lifespan period')->render();
    }
    if ($query_key === 'monarch_reign' || $query_key === 'monarch') {
      return $this->t('Monarch Reign period')->render();
    }
    return $this->t('Historic Period')->render();
  }

  public function generateSelectedValuesString($values, string $query_key): string {
    if ($query_key === 'date') {
      return $this->t('From @start To @end', [
        '@start' => $this->processDatePartFromQueryParams($values['from'] ?? ''),
        '@end' => $this->processDatePartFromQueryParams($values['to'] ?? '', FALSE),
      ])->render();
    }
    // Lifespan or monarch reign options field is not a multiple values input.
    if ($query_key === 'lifespan' || $query_key === 'monarch_reign') {
      $is_multiple = !empty($this->configuration[$query_key . '_is_multiple']);
      $options = $this->getOtherItemOptions($query_key);

      if ($is_multiple && is_array($values)) {
        $selected_values = array_intersect_key($options, array_flip($values));
        return implode(', ', array_values($selected_values));
      }
      return $options[$values] ?? '';
    }
    return parent::generateSelectedValuesString($values, $query_key);
  }

  public function buildXQueryString(array $widget_values): string {
    $xquery = '';

    // Extract periods related data.
    $periods = [];
    foreach (['period', 'lifespan', 'monarch_reign'] as $period_key) {
      if ($period_key === 'period') {
        $is_multiple = $this->configuration['element_type'] === 'checkboxes';
      }
      else {
        $is_multiple = !empty($this->configuration[$period_key . '_is_multiple']);
      }
      if (isset($widget_values[$period_key]) && is_array($widget_values[$period_key]) && $this->validQueryParameters($widget_values[$period_key])) {
        $period_items = $is_multiple ? $widget_values[$period_key] : [$widget_values[$period_key]];
        $periods = array_merge($periods, $period_items);
      }
    }

    // Building xquery string of data grouped under period.
    if ($periods) {
      $xquery .= '"period":(';
      $xquery .= '"' . implode('","' , $periods) . '"';
      $xquery .= '), ';
    }

    // Building xquery string of data grouped under date.
    if (isset($widget_values['date']['from']) || isset($widget_values['date']['to'])) {
      $from_date = $widget_values['date']['from'] ?? '';
      $to_date = $widget_values['date']['to'] ?? '';
      $xquery .= '"date":map{';
      $xquery .= '"start":"' . $this->processDatePartFromQueryParams($from_date) . '", ';
      $xquery .= '"end":"' . $this->processDatePartFromQueryParams($to_date, FALSE) . '"';
      $xquery .= '}, ';
    }
    return $xquery;
  }

  /**
   * Gets the request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request object.
   */
  protected function getRequest() {
    return $this->requestStack->getCurrentRequest();
  }

  protected function buildDateWidget(string $range, $default_date): array {
    $date_parts = $this->extractDatePartsFromQueryParameter($default_date);
    return [
      '#type' => 'fieldset',
      '#title' => $range === 'from' ? $this->t('From') : $this->t('To'),
      '#tree' => TRUE,
      'common_era' => [
        '#type' => 'radios',
        '#title' => $this->t('Common Era'),
        '#title_display' => 'invisible',
        '#options' => [
          'bce' => $this->t('BCE'),
          'ce' => $this->t('CE'),
        ],
        '#default_value' => $date_parts['common_era'],
        '#wrapper_attributes' => ['class' => ['tw-flex', 'tw-space-x-4']],
      ],
      'year' => [
        '#type' => 'textfield',
        '#title' => $this->t('Year'),
        '#title_display' => 'invisible',
        '#attributes' => [
          'placeholder' => $this->t('YYYY'),
          'pattern' => '[0-9]{4}',
          'minlength' => 4,
          'class' => ['tw-text-center'],
        ],
        '#maxlength' => 4,
        '#size' => 5,
        '#wrapper_attributes' => [
          'class' => ['facet-widget-date--item', 'facet-widget-date--year'],
        ],
        '#default_value' => $date_parts['year'],
      ],
      'month' => [
        '#type' => 'textfield',
        '#title' => $this->t('Month'),
        '#title_display' => 'invisible',
        '#attributes' => [
          'placeholder' => $this->t('MM'),
          'pattern' => '[0-9]{2}',
          'minlength' => 2,
          'class' => ['tw-text-center'],
        ],
        '#maxlength' => 2,
        '#size' => 3,
        '#wrapper_attributes' => [
          'class' => ['facet-widget-date--item', 'facet-widget-date--month'],
        ],
        '#default_value' => $date_parts['month'],
      ],
      'day' => [
        '#type' => 'textfield',
        '#title' => $this->t('Day'),
        '#title_display' => 'invisible',
        '#attributes' => [
          'placeholder' => $this->t('DD'),
          'pattern' => '[0-9]{2}',
          'minlength' => 2,
          'class' => ['tw-text-center'],
        ],
        '#maxlength' => 2,
        '#size' => 3,
        '#wrapper_attributes' => [
          'class' => ['facet-widget-date--item', 'facet-widget-date--day'],
        ],
        '#default_value' => $date_parts['day'],
      ],
    ];
  }

  protected function processDatePart($data, $is_start = TRUE) {
    $is_ce = TRUE;
    if (!empty($data['common_era']) && $data['common_era'] === 'bce') {
      $is_ce = FALSE;
    }
    $date = !$is_ce ? '-' : '';

    if (!empty($data['year']) && is_numeric($data['year'])) {
      $date .= $data['year'] . '-';
    }
    else {
      $date .= $is_start ? '-9999-' : '9999-';
    }

    if (!empty($data['month']) && is_numeric($data['month']) && $data['month'] <= 12) {
      $date .= str_pad($data['month'], 2, '0', STR_PAD_LEFT) . '-';
    }
    else {
      $date .= $is_start ? '01-' : '12-';
    }

    if (!empty($data['day']) && is_numeric($data['day']) && $data['day'] <= 31) {
      $date .= str_pad($data['day'], 2, '0', STR_PAD_LEFT);
    }
    else {
      $date .= $is_start ? '01' : '31';
    }

    return $date;
  }

  protected function processDatePartFromQueryParams(string $date, $is_start = TRUE) {
    $default_date = $is_start ? '-9999-01-01' : '9999-12-31';
    $length = strlen($date);
    $is_bce = $length === 11;

    // Validating date string length.
    if ((!$is_bce && $length != 10) || ($is_bce && substr($date, 0, 1) !== '-')) {
      return $default_date;
    }

    $date = ltrim($date, '-');
    // Extract date parts from validation.
    [$year, $month, $day] = explode('-', $date);

    return checkdate($month, $day, $year) ? $date : $default_date;
  }

  protected function extractDatePartsFromQueryParameter(string $date) {
    $date_parts = [
      'common_era' => 'ce',
      'year' => '',
      'month' => '',
      'day' => '',
    ];
    $length = strlen($date);
    $is_bce = $length === 11;

    if ((!$is_bce && $length != 10) || ($is_bce && substr($date, 0, 1) !== '-')) {
      return $date_parts;
    }

    // Determine bce or ce based on the length of the date.
    $date_parts['common_era'] = $is_bce ? 'bce' : 'ce';

    $date = ltrim($date, '-');
    $data = explode('-', $date);
    [$year, $month, $day] = $data;

    // Only assigning the date parts regular values if the year is correct.
    if (checkdate($month, $day, $year)) {
      $date_parts['year'] = $year;
      $date_parts['month'] = $month;
      $date_parts['day'] = $day;
    }

    return $date_parts;
  }

}
