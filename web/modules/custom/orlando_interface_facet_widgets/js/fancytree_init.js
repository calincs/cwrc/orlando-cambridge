(function($, Drupal) {

  Drupal.oiFacetWidgetsFancyTree = {};

  Drupal.oiFacetWidgetsFancyTree.formSubmit = function (fancyTreeEltId, eltName, filter) {
    filter.value = '';
    $.ui.fancytree.getTree(`#${fancyTreeEltId}`).generateFormElements(eltName + '[]');
  };

  Drupal.oiFacetWidgetsFancyTree.applyFilter = function (searchInput, resetBtn, matchPlaceholder, event) {
    const match = searchInput.value;
    const tree = $.ui.fancytree.getTree(`#${searchInput.dataset.fancytreeElementFilterTreeId}`);

    if ((event && event.which === $.ui.keyCode.ESCAPE) || $.trim(match) === ''){
      resetBtn.click();
      return;
    }

    const n = tree.filterNodes.call(tree, match, {});
    resetBtn.removeAttribute('disabled');
    matchPlaceholder.innerText = Drupal.formatPlural(n,'(1 match)', '(@count matches)', {
      '@count': n,
    });
  }

  Drupal.oiFacetWidgetsFancyTree.resetFilter = function (btn, searchInput, matchPlaceholder, event) {
    event.preventDefault();
    searchInput.value = '';
    matchPlaceholder.innerText = '';
    $.ui.fancytree.getTree(`#${btn.dataset.fancytreeElementFilterTreeId}`).clearFilter();
    btn.setAttribute('disabled', true);
  }

  Drupal.behaviors.oiFacetWidgetsFancytreeInit = {
    attach: function (context) {
      const fancyTreeElements = context.querySelectorAll('[data-fancytree-element="true"]');

      fancyTreeElements.forEach((elt, i) => {
        if (elt.classList.contains('js-processed')) { return; }
        elt.classList.add('js-processed');

        const eltId = elt.getAttribute('id');
        const eltForm = elt.closest('form');
        const filter = context.querySelector(`input[data-fancytree-element-filter-tree-id="${eltId}"]`);
        if (eltForm) {
          eltForm.addEventListener('submit', (e) => {
            Drupal.oiFacetWidgetsFancyTree.formSubmit(eltId, elt.dataset.fancytreeElementName, filter);
          });
        }

        // Initializing the tree.
        $(`#${eltId}`).fancytree({
          source: JSON.parse(elt.dataset.fancytreeSource),
          aria: true, // Enable WAI-ARIA support.
          autoActivate: true, // Automatically activate a node when it is focused (using keys).
          autoScroll: true, // Automatically scroll nodes into visible area.
          checkbox: true, // Show checkboxes.
          debugLevel: 2, // 0:quiet, 1:normal, 2:debug
          focusOnSelect: false, // Set focus when node is checked by a mouse click
          idPrefix: `fancytree-${i}--`, // Used to generate node id´s like <span id='fancytree-id-<key>'>.
          keyboard: true, // Support keyboard navigation.
          minExpandLevel: 1, // 1: root node is not collapsible
          quicksearch: true, // Navigate to next node by typing the first letters.
          selectMode: 3, // 1:single, 2:multi, 3:multi-hier
          titlesTabbable: true, // Node titles can receive keyboard focus
          extensions: ["filter"],
          filter: {
            autoApply: true,   // Re-apply last filter if lazy data is loaded
            autoExpand: false, // Expand all branches that contain matches while filtered
            counter: true,     // Show a badge with number of matching child nodes near parent icons
            fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
            hideExpandedCounter: false,  // Hide counter badge if parent is expanded
            hideExpanders: false,       // Hide expanders if all child nodes are hidden by filter
            highlight: true,   // Highlight matches by wrapping inside <mark> tags
            leavesOnly: true, // Match end nodes only
            nodata: true,      // Display a 'no data' status node if result is empty
            mode: 'hide',       // hiding unmatched node (pass "dimm" to grayout unmatched nodes instead)
          },
          createNode: function(event, data) {
            // This is probably a parent then it will not be a default value.
            if (data.node.children || !data.tree.data.fancytreeSourceDefaultValue.length) {
              return;
            }

            data.node.setSelected(data.tree.data.fancytreeSourceDefaultValue.includes(data.node.key));
          },
        });

        // Setting up the filter.
        const resetFilter = context.querySelector(`button[data-fancytree-element-filter-tree-id="${eltId}"]`);
        const matchPlaceholder = context.querySelector(`.${eltId}--matches`);
        filter.addEventListener('keyup', function (e) {
          Drupal.oiFacetWidgetsFancyTree.applyFilter(this, resetFilter, matchPlaceholder, e);
        });
        resetFilter.addEventListener('click', function(e) {
          Drupal.oiFacetWidgetsFancyTree.resetFilter(this, filter, matchPlaceholder, e);
        });
      });
    }
  };
})(jQuery, Drupal);
