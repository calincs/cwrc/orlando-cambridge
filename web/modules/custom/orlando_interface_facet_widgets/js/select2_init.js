(function($, Drupal) {
  Drupal.behaviors.oiFacetWidgetsFancytreeInit = {
    attach: function (context) {
      const select2Elements = context.querySelectorAll('[data-select2-element="true"]');
      select2Elements.forEach(function (sel, i) {
        if (sel.classList.contains('js-processed')) { return; }
        sel.classList.add('js-processed');

        const select2_widget = $(sel);
        select2_widget.on('select2:select select2:unselect', function (item) {
          select2_widget.trigger('facets_filter', [item.params.data.id]);
        });
        select2_widget.on('facets_filtering.select2', function () {
          select2_widget.prop('disabled', true);
        });
      });
    }
  };
})(jQuery, Drupal);
