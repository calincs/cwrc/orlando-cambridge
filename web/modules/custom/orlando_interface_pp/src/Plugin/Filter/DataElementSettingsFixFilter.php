<?php

namespace Drupal\orlando_interface_pp\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to display embedded entities based on data attributes.
 *
 * @Filter(
 *   id = "oi_data_element_settings_fix",
 *   title = @Translation("Data element settings fix"),
 *   description = @Translation("Fix any double quoted html data-element-settings attribute before it get affected by blazy."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 1,
 * )
 */
class DataElementSettingsFixFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (str_contains($text, 'data-element-settings="{"target":"')) {
      $text = str_replace(['data-element-settings="{"', '"}">'], ['data-element-settings=\'{"', '"}\'>'], $text);
    }
    return new FilterProcessResult($text);
  }

}
