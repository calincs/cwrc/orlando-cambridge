<?php

namespace Drupal\orlando_interface_pp\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Taxonomy term tid default argument for the person vocabulary.
 *
 * @ViewsArgumentDefault(
 *   id = "oi_author_profile_taxonomy_tid",
 *   title = @Translation("Author profile taxonomy term tid from URL")
 * )
 */
class AuthorPersonTid extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  protected $entityTypeManager;

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Constructs a new AuthorPersonTid instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->vocabularyStorage = $entity_type_manager->getStorage('taxonomy_vocabulary');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $node = $this->routeMatch->getParameter('node');
    if ($node && is_numeric($node)) {
      $node = $this->entityTypeManager->getStorage('node')
        ->load($node);
    }
    if ($node instanceof NodeInterface && $node->bundle() === 'author_profile') {
      return $node->get('field_person')->target_id;
    }

    $taxonomy_term = $this->routeMatch->getParameter('taxonomy_term');
    $vid = $taxonomy_term instanceof TermInterface ? $taxonomy_term->bundle() : '';
    if ($taxonomy_term && is_numeric($taxonomy_term)) {
      $taxonomy_term = $this->vocabularyStorage->load($taxonomy_term);
      $vid = $taxonomy_term->bundle();
    }
    if ($vid === 'person' || $vid === 'organization') {
      return $taxonomy_term->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    foreach ($this->vocabularyStorage->loadMultiple(['person', 'organization']) as $vocabulary) {
      $dependencies[$vocabulary->getConfigDependencyKey()][] = $vocabulary->getConfigDependencyName();
    }
    return $dependencies;
  }

}
