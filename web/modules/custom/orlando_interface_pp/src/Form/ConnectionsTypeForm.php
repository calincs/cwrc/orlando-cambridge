<?php

namespace Drupal\orlando_interface_pp\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for connection type forms.
 */
class ConnectionsTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $connections_type = $this->entity;

    if (!$connections_type->isNew()) {
      $form['#title'] = (t('Edit %title connection type', [
        '%title' => $connections_type->label(),
      ]));
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $connections_type->label(),
      '#description' => $this->t('Label for the Connections type.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $connections_type->id(),
      '#machine_name' => [
        'exists' => 'oi_connections_type_load',
      ],
      '#maxlength' => 32,
      '#disabled' => !$connections_type->isNew(),
    ];
    $form['description'] = [
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $connections_type->getDescription(),
      '#description' => t('This text will be displayed on the <em>Add new connection</em> page.'),
    ];

    return $form;
  }

}
