<?php

namespace Drupal\orlando_interface_pp\EventSubscriber;

use Drupal\orlando_interface_ingestion\Event\BasicBodySetEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_pp\HtmlProcessor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IngestionSubscriber implements EventSubscriberInterface {

  protected $htmlProcessor;

  public function __construct(HtmlProcessor $html_processor) {
    $this->htmlProcessor = $html_processor;
  }

  /**
   * @inheritDoc
   */
  public static function getSubscribedEvents() {
    return [
      IngestionEvents::BEFORE_BODY_SET => ['parseParagraphBody'],
    ];
  }

  public function parseParagraphBody(BasicBodySetEvent $event) {
    $processed_data = $this->htmlProcessor->processParagraphBody($event->getBody(), $event->getSource(), $event->getParagraph());
    $event->setBody($processed_data['html']);
    $event->setExtractedConnections($processed_data['connection_ids']);
  }

}
