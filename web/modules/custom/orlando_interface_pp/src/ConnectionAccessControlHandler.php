<?php

namespace Drupal\orlando_interface_pp;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller handler for the connection entity.
 *
 * @see \Drupal\orlando_interface_pp\Entity\Connection
 */
class ConnectionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\orlando_interface_ingestion\Entity\ConnectionInterface $entity */
    if ($operation === 'view' && $entity->getTarget() != NULL) {
      return $entity->getTarget()->access('view', $account, TRUE);
    }
    return parent::checkAccess($entity, $operation, $account);
  }

}
