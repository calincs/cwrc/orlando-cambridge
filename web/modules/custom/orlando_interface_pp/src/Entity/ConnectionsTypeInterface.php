<?php

namespace Drupal\orlando_interface_pp\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface ConnectionsTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of this connection type.
   */
  public function getDescription();

}
