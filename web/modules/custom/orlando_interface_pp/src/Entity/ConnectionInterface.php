<?php

namespace Drupal\orlando_interface_pp\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\paragraphs\ParagraphInterface;

interface ConnectionInterface extends ContentEntityInterface {

  public function setSource(ContentEntityInterface $source);

  public function getSource(): ContentEntityInterface;

  public function setTarget(ContentEntityInterface $subject);

  public function getTarget(): ContentEntityInterface;

  public function setParagraph(ParagraphInterface $paragraph);

  public function getParagraph(): ParagraphInterface;

  public function setContext(string $context);

  public function getContext(): string;

  public function setParagraphExternalId(string $id);

  public function getParagraphExternalId(): string;

}
