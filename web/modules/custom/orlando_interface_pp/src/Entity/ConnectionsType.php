<?php

namespace Drupal\orlando_interface_pp\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ConnectionsType entity.
 *
 * @ConfigEntityType(
 *   id = "oi_connections_type",
 *   label = @Translation("Connections type"),
 *   label_collection = @Translation("Connections types"),
 *   label_singular = @Translation("Connections type"),
 *   label_plural = @Translation("Connections types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Connections type",
 *     plural = "@count Connections types",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "list_builder" = "Drupal\orlando_interface_pp\Controller\ConnectionsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\orlando_interface_pp\Form\ConnectionsTypeForm",
 *       "edit" = "Drupal\orlando_interface_pp\Form\ConnectionsTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "connections_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "oi_connection",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/oi-connections-type/{oi_connections_type}",
 *     "delete-form" = "/admin/structure/oi-connections-type/{oi_connections_type}/delete",
 *     "collection" = "/admin/structure/oi-connections-type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class ConnectionsType extends ConfigEntityBundleBase implements ConnectionsTypeInterface {

  /**
   * The ConnectionsType ID.
   *
   * @var string
   */
  public $id;

  /**
   * The ConnectionsType label.
   *
   * @var string
   */
  public $label;

  /**
   * A brief description of this connection type.
   *
   * @var string
   */
  public $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

}
