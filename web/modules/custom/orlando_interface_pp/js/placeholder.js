(function(Drupal, once) {
  Drupal.behaviors.oiParagraphParserPlaceholder = {
    attach: function (context) {
      // Paragraph wrapper.
      once('paragraph-wrapper-placeholder-processed', '.paragraph-wrapper', context).forEach(
        (wrapper) => {
          let html = wrapper.innerHTML;
          // Removing line breaks.
          html = html.replace(/\n|\r\n/g, '');
          html = html.replaceAll("</span> '", "</span>'");
          html = html.replaceAll("</span> ’", "</span>’");
          html = html.replaceAll('</span> ,', '</span>,');
          html = html.replaceAll('</span> .', '</span>.');
          html = html.replaceAll('</span> )', '</span>)');
          html = html.replaceAll('</span> :', '</span>:');
          html = html.replaceAll('</span> ;', '</span>;');
          html = html.replaceAll('</span> !', '</span>!');
          html = html.replaceAll('</span> ?', '</span>?');
          html = html.replaceAll('</span> ...', '</span>...');
          html = html.replaceAll('</span> –', '</span>–');

          ['a', 'span'].forEach(function (tag) {
            html = html.replaceAll(`</${tag}></span>`, `</${tag}></span> `);
            // Fix spacing after punctuation.
            html = html.replaceAll(`</${tag}></span> '`, `</${tag}></span>'`);
            html = html.replaceAll(`</${tag}></span> ’`, `</${tag}></span>’`);
            html = html.replaceAll(`</${tag}></span> ,`, `</${tag}></span>,`);
            html = html.replaceAll(`</${tag}> </span>,`, `</${tag}></span>,`);
            html = html.replaceAll(`</${tag}></span> .`, `</${tag}></span>.`);
            html = html.replaceAll(`</${tag}> </span>.`, `</${tag}></span>.`);
            html = html.replaceAll(`</${tag}></span> )`, `</${tag}></span>)`);
            html = html.replaceAll(`</${tag}> </span>)`, `</${tag}></span>)`);
            html = html.replaceAll(`</${tag}></span> :`, `</${tag}></span>:`);
            html = html.replaceAll(`</${tag}></span> ;`, `</${tag}></span>;`);
            html = html.replaceAll(`</${tag}></span> !`, `</${tag}></span>!`);
            html = html.replaceAll(`</${tag}></span> ?`, `</${tag}></span>?`);
            html = html.replaceAll(`</${tag}></span> ...`, `</${tag}></span>...`);
            html = html.replaceAll(`</${tag}></span> –`, `</${tag}></span>–`);
            html = html.replaceAll(`</${tag}><span data-oi-element-context`, `</${tag}> <span data-oi-element-context`);
            html = html.replaceAll(`</div><${tag} data-oi-element-context`, `</div> <${tag} data-oi-element-context`);
            wrapper.innerHTML = html.replaceAll(`</${tag}><span data-embed`, `</${tag}> <span data-embed`);
          });
        }
      );
    }
  };
})(Drupal, once);
