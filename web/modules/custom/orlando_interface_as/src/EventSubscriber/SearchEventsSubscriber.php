<?php

namespace Drupal\orlando_interface_as\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orlando_interface_search\Event\ResultsSummaryEvent;
use Drupal\orlando_interface_search\Event\SearchEvents;
use Drupal\orlando_interface_search\Event\AddingXquerySearchFunctionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SearchEventsSubscriber implements EventSubscriberInterface {

  /**
   * The syntax component entity storage.
   *
   * @var \Drupal\orlando_interface_as\SyntaxComponentStorage
   */
  protected $componentStorage;

  /**
   * Constructs the SearchEventsSubscriber instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->componentStorage = $entity_type_manager->getStorage('oi_syntax_component');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SearchEvents::ADDING_XQUERY_SEARCH_FN => ['onAdds'],
      SearchEvents::RESULT_SUMMARY => ['resultsSummary'],
    ];
  }

  /**
   * Overwriting the default added function when the advanced variable has been
   * applied.
   *
   * @param \Drupal\orlando_interface_search\Event\AddingXquerySearchFunctionEvent $event
   *   The event.
   */
  public function onAdds(AddingXquerySearchFunctionEvent $event) {
    if (in_array('advanced', $event->getAppliedVariables())) {
      $event->setFunction('search:getCompleteAdvancedSearchResults');
    }
  }

  public function resultsSummary(ResultsSummaryEvent $event) {
    $parameters = $event->getQueryParameters();
    $advanced = $parameters['advanced'] ?? [];
    if (!$advanced) {
      $query_keys = [
        'tag' => 'value',
        'attribute' => 'attribute',
        'attributeValue' => 'attributeValue',
      ];
      $advanced = array_intersect_key($parameters, $query_keys);
    }
    if (empty($advanced['value']) && empty($advanced['tag'])) {
      return;
    }
    $tag_xml = $advanced['value'] ?? $advanced['tag'];
    if (!($tag = $this->componentStorage->loadByMachineName($tag_xml))) {
      return;
    }

    $args = $event->getArgs();
    $message = $event->getMessage();
    $args['@tag'] = $tag->label();
    $message .= empty($args['@keys']) ? ' <span class="tw-font-bold">@tag</span>' : ' for <span class="tw-font-bold">@tag</span>';
    if (!empty($advanced['attribute']) && ($attr = $this->componentStorage->loadByMachineName($advanced['attribute']))) {
      $args['@attribute'] = $attr->label();
      $message .= ' with attribute <span class="tw-font-bold">@attribute';
      if (!empty($advanced['attributeValue']) && ($val = $this->componentStorage->loadByMachineName($advanced['attributeValue']))) {
        $args['@attributeValue'] = $val->label();
        $message .= ':@attributeValue';
      }
      $message .= '</span>';
    }

    $event->setMessage($message);
    $event->setArgs($args);
  }

}
