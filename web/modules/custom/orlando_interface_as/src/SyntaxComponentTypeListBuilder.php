<?php

namespace Drupal\orlando_interface_as;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of syntax component type entities.
 *
 * @see \Drupal\orlando_interface_as\Entity\SyntaxComponentType
 */
class SyntaxComponentTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['children_label'] = $this->t('Children Label');
    $header['children_machine_name'] = $this->t('Children machine name');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentTypeInterface $entity */

    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['children_label'] = $entity->getChildrenLabel();
    $row['children_machine_name'] = $entity->getChildrenMachineName();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No syntax component types available. <a href=":link">Add syntax component type</a>.',
      [':link' => Url::fromRoute('entity.oi_syntax_component_type.add_form')->toString()]
    );

    return $build;
  }

}
