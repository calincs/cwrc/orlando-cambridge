<?php

namespace Drupal\orlando_interface_as;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\orlando_interface_as\Entity\SyntaxComponentInterface;

class SyntaxComponentStorage extends SqlContentEntityStorage {

  /**
   * Loads syntax components by type.
   *
   * @param string $bundle
   *   The bundle to look into.
   *
   * @return array|\Drupal\orlando_interface_as\Entity\SyntaxComponentInterface[]
   *   An array of syntax component objects indexed by their ids.
   */
  public function loadByType(string $bundle) {
    // Build a query to fetch the entity IDs.
    $entity_query = $this->getQuery();
    $entity_query->accessCheck(FALSE);
    $this->buildPropertyQuery($entity_query, ['type' => $bundle]);
    $entity_query->sort('title', 'ASC');
    $result = $entity_query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

  public function loadChildrenByMachineName(string $machine_name, string $type, string $array_key = 'id') {
    return ($component = $this->loadByMachineName($machine_name, $type)) ? $component->getChildren($array_key) : [];
  }

  public function loadByMachineName(string $machine_name, string $type = '') :?SyntaxComponentInterface {
    $machine_name = strtoupper($machine_name);
    if (!($ids = $this->loadIdByMachineName($machine_name, $type))) {
      return NULL;
    }
    $id = reset($ids);
    return $this->load($id);
  }

  public function loadIdByMachineName(string $machine_name, string $type = '') :array {
    $ids = [];
    $query = $this->getQuery()->accessCheck(TRUE);
    if ($type) {
      $query->condition('type', $type);
    }
    return $query->condition('machine_name', $machine_name)
      ->range(0, 1)
      ->execute();
  }

}
