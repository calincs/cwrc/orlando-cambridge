<?php

namespace Drupal\orlando_interface_as;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity\BulkFormEntityListBuilder;

class SyntaxComponentListBuilder extends BulkFormEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label (Machine name)');
    $header['type'] = $this->t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface */

    $type = $entity->bundle();
    // A shortcut to avoid loading the bundle entity and then get its label.
    $known_types = [
      'tag' => 'Tag',
      'attribute' => 'Attribute',
      'attributeValue' => 'Attribute value',
    ];
    $row['title']['data']['#markup'] = $entity->label() . ' (' . $entity->getMachineName() . ')';
    $row['type']['data']['#markup'] = $known_types[$type] ?? $type;
    return $row + parent::buildRow($entity);
  }

}
