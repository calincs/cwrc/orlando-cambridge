<?php

namespace Drupal\orlando_interface_as\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Constraint(
 *   id = "UniqueFieldByBundle",
 *   label = @Translation("Unique field by bundle constraint", context = "Validation"),
 * )
 */
class UniqueFieldByBundle extends Constraint {

  public $message = 'A @entity_type of @bundle with @field_name %value already exists.';

}
