<?php

namespace Drupal\orlando_interface_as\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\MachineNameWidget as BaseMachineNameWidget;

/**
 * Alters the core machine name widget to extend its applicability.
 */
class MachineNameWidget extends BaseMachineNameWidget {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This widget is only available to fields that have a 'UniqueField' or
    // 'UniqueFieldByBundle' constraint.
    $constraints = $field_definition->getConstraints();
    return isset($constraints['UniqueField']) || isset($constraints['UniqueFieldByBundle']);
  }

}
