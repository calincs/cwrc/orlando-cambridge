<?php

namespace Drupal\orlando_interface_as\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable\Filters;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "advanced",
 *   label = @Translation("Advanced"),
 *   reserved = FALSE,
 *   allow_empty = FALSE,
 *   weight = 6,
 * )
 */
class Advanced extends Filters {

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $advanced_parameters = $parameters['advanced'] ?? [];
    if (!$advanced_parameters) {
      $query_keys = [
        'tag' => 'value',
        'attribute' => 'attribute',
        'attributeValue' => 'attributeValue',
      ];
      $advanced_parameters = array_intersect_key($parameters, $query_keys);
    }
    if (!$advanced_parameters) {
      return '';
    }
    if (empty($advanced_parameters['tag'])) {
      $advanced_parameters['tag'] = $advanced_parameters['value'];
      unset($advanced_parameters['value']);
    }
    return parent::buildXqueryVariable($connection, $advanced_parameters);
  }

}
