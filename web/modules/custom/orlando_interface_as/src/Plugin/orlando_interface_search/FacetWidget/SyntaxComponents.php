<?php

namespace Drupal\orlando_interface_as\Plugin\orlando_interface_search\FacetWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @OrlandoInterfaceSearchBaseXFacetWidget(
 *   id = "syntax_components",
 *   label = @Translation("Syntax components"),
 * )
 */
class SyntaxComponents extends BaseXFacetWidgetPluginBase {

  protected $entityTypeManager;

  /**
   * The syntax component storage.
   *
   * @var \Drupal\orlando_interface_as\SyntaxComponentStorage
   */
  protected $storage;

  /**
   * Constructs the SyntaxComponent instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->storage = $entity_type_manager->getStorage('oi_syntax_component');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    $element = parent::formElement($form, $form_state);

    // This part deal with when the widget is loaded on the search form.
    if (!empty($form['#query_parameters'])) {
      $advanced_parameters = $form['#query_parameters']['advanced'] ?? [];
      if (!$advanced_parameters) {
        $query_keys = array_flip($this->getQueryKeys());
        $advanced_parameters = array_intersect_key($form['#query_parameters'], $query_keys);
      }
      $advanced_parameters = is_array($advanced_parameters) ? $advanced_parameters : [];
      return $this->persistSelectedComponentsAsHiddenInputs($element, $advanced_parameters, $form);
    }

    $element['#attribute_dropdown_wrapper'] = 'syntax_components--attribute-dropdown-wrapper';
    $element['#attribute_value_dropdown_wrapper'] = 'syntax_components--attribute-value-dropdown-wrapper';
    $none_option = ['' => $this->t('- None -')];

    $display_context = $form['#display_context'] ?? '';
    $standalone = $display_context === 'standalone';
    $tag_options = $this->getOptions('tag');
    $element['value']['#type'] = 'select';
    $element['value']['#title'] = $this->t('Tags');
    // Help with the identification of the triggering element.
    $element['value']['#component_type'] = 'tag';
    $element['value']['#title_display'] = 'before';
    $element['value']['#options'] = $none_option + $tag_options;
    $element['value']['#ajax'] = [
      'callback' => [static::class, 'componentChildrenDropdownCallback'],
      'wrapper' => $element['#attribute_dropdown_wrapper'],
      'method' => 'replace',
      'effect' => 'fade',
    ];
    if ($standalone) {
      // Remove fieldset when we are in standalone mode.
      $element['#type'] = 'container';
      $element['#attributes']['class'][] = 'tw-mt-8';
      $element['#attributes']['class'][] = 'tw-flex';
      $element['#attributes']['class'][] = 'tw-justify-center';
      $element['value']['#required'] = TRUE;
      $element['value']['#title_display'] = 'invisible';
      $element['value']['#field_prefix'] = '<span class="tw-font-semibold tw-mr-2">' . $this->t('Search within tag') . '</span>';
    }

    // Extract the parent tree minus current element.
    $parents = [];
    $triggering_element = $form_state ? $form_state->getTriggeringElement() : NULL;
    if (!empty($triggering_element['#component_type']) && !empty($triggering_element['#array_parents'])) {
      $parents = array_slice($triggering_element['#array_parents'], 0, 1);
    }

    $selected_tag = $this->extractSelectedComponent('value', $parents, $form_state);
    // Set the default value for the tag options and initialize the children
    // attribute options if available.
    $attribute_options = [];
    if ($selected_tag && $tag_options && $this->validQueryParameters([$selected_tag], array_keys($tag_options))) {
      $element['value']['#default_value'] = $selected_tag;
      $attribute_options = $this->getOptions('tag', $selected_tag);
    }

    // Initializing the attribute component element.
    $element['attribute'] = $this->initChildComponentElement($element['#attribute_dropdown_wrapper']);
    // Initializing the attribute value component element.
    $element['attributeValue'] = $this->initChildComponentElement($element['#attribute_value_dropdown_wrapper']);
    if (!$attribute_options) {
      return $element;
    }

    unset($element['attribute']['#value']);
    $element['attribute']['#type'] = 'select';
    $element['attribute']['#title'] = $this->t('Attribute');
    $element['attribute']['#options'] = $none_option + $attribute_options;
    $element['attribute']['#component_type'] = 'attribute';
    $element['attribute']['#ajax'] = [
      'callback' => [static::class, 'componentChildrenDropdownCallback'],
      'wrapper' => $element['#attribute_value_dropdown_wrapper'],
      'method' => 'replace',
      'effect' => 'fade',
    ];
    if ($standalone) {
      $element['attribute']['#title_display'] = 'invisible';
      $element['attribute']['#field_prefix'] = '<span class="tw-font-semibold tw-ml-4 tw-mr-2">' . $this->t('With attribute') . '</span>';
    }

    $attribute_value_options = [];
    $selected_attribute = $this->extractSelectedComponent('attribute', $parents, $form_state);
    if ($selected_attribute && $this->validQueryParameters([$selected_attribute], array_keys($attribute_options))) {
      $element['attribute']['#default_value'] = $selected_attribute;
      $attribute_value_options = $this->getOptions('attribute', $selected_attribute);
    }

    if (!$attribute_value_options) {
      return $element;
    }

    unset($element['attributeValue']['#value']);
    $element['attributeValue']['#type'] = 'select';
    $element['attributeValue']['#title'] = $this->t('Value');
    $element['attributeValue']['#options'] = $none_option + $attribute_value_options;
    if ($standalone) {
      $element['attributeValue']['#attributes']['class'][] = 'tw-ml-2';
      $element['attributeValue']['#wrapper_attributes']['class'][] = 'tw-ml-4';
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryKeys(): array {
    return [
      'value' => 'tag',
      'attribute' => 'attribute',
      'attributeValue' => 'attributeValue',
    ];
  }

  public function getOptions(string $type, string $selected_machine_name = ''): array {
    $options = [];
    // Tag is the root element.
    if ($type === 'tag' && !$selected_machine_name) {
      $tags = $this->storage->loadByType('tag');
      foreach ($tags as $tag) {
        $options[$tag->getMachineName()] = $tag->label();
      }
      return $options;
    }
    // We probably want to return child elements corresponding to this machine
    // name.
    if ($selected_machine_name && in_array($type, ['tag', 'attribute'])) {
      $children = $this->storage->loadChildrenByMachineName($selected_machine_name, $type, 'machine_name');
      foreach ($children as $key => $child) {
        $options[$key] = $child->label();
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function extractSelectedValues($data) {
    if (!$data || (empty($data['value']) && empty($data['advanced']['value']))) {
      return NULL;
    }
    if (isset($data['advanced'])) {
      $data = $data['advanced'];
    }

    $values = ['tag' => $data['value']];
    if (!empty($data['attribute'])) {
      $values['attribute'] = $data['attribute'];
    }
    if (!empty($data['attributeValue'])) {
      $values['attributeValue'] = $data['attributeValue'];
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function generateSelectedValuesString($values, string $query_key): string {
    // We don't want any of the syntax components displayed under the render
    // filter items clear.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function processedMappings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildXQueryString(array $widget_values): string {
    $validated = FALSE;
    foreach ($widget_values as $type => $value) {
      if (empty($value)) {
        continue;
      }
      $validated = (bool) $this->storage->loadIdByMachineName($value, $type);
    }
    if (!$validated) {
      return '';
    }

    $xquery = '"tag":"' . $widget_values['tag'] . '"';
    if (!empty($widget_values['attribute'])) {
      $xquery .= ', "attribute":"' . $widget_values['attribute'] . '"';
      if (!empty($widget_values['attributeValue'])) {
        $xquery .= ', "attributeValue":"' . $widget_values['attributeValue'] . '"';
      }
    }
    return $xquery;
  }

  /**
   * {@inheritdoc}
   */
  public function validQueryParameters(array $values, array $allowed_values = []): bool {
    if ($allowed_values) {
      return !empty(array_intersect($values, $allowed_values));
    }

    return (bool) $this->storage->loadIdByMachineName(reset($values));
  }

  public static function componentChildrenDropdownCallback(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $component_type = $triggering_element['#component_type'];
    $element = NestedArray::getValue($form, array_slice($triggering_element['#array_parents'], 0, -1));
    return $component_type === 'tag' ? $element['attribute'] : $element['attributeValue'];
  }

  private function initChildComponentElement(string $wrapper_id) {
    return [
      '#type' => 'hidden',
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#value' => '',
    ];
  }

  private function extractSelectedComponent(string $element_key, $parents, FormStateInterface $form_state = NULL) {
    // Setting the default value.
    $query_key = $this->getQueryKeys()[$element_key];
    $selected_component = $form['#query_parameters'][$query_key] ?? '';
    // Checking for $triggering_element['#component_type'] below to ensure that
    // we are dealing with our element.
    if (!$selected_component && $parents) {
      $parents[] = $element_key;
      $selected_component = $form_state->getValue($parents, '');
    }
    return $selected_component;
  }

  private function persistSelectedComponentsAsHiddenInputs($element, $parameters, &$form) {
    $selected_tag = $parameters['value'] ?? '';
    if (!$selected_tag) {
      $selected_tag = $parameters['tag'] ?? '';
    }
    $element['#access'] = FALSE;
    $element['#type'] = 'container';
    if (!$selected_tag || !is_string($selected_tag) || !isset($this->getOptions('tag')[$selected_tag])) {
      return $element;
    }

    // With advanced search included into the form then we need to set the
    // search keys not required.
    if (!empty($form['keys']['#required'])) {
      $form['keys']['#required'] = FALSE;
    }
    // Let set the access on since there is at least selected tag.
    $element['#access'] = TRUE;
    // Setting hidden input for the tag components.
    $element['value']['#type'] = 'hidden';
    $element['value']['#value'] = $selected_tag;

    // Determining if we should also set the attribute.
    $selected_attribute = $parameters['attribute'] ?? '';
    if (!$selected_attribute || !is_string($selected_attribute) || !isset($this->getOptions('tag', $selected_tag)[$selected_attribute])) {
      return $element;
    }

    // Setting hidden input for the attribute component.
    $element['attribute']['#type'] = 'hidden';
    $element['attribute']['#value'] = $selected_attribute;

    // Determining if we should also set the attribute value.
    $selected_attribute_value = $parameters['attributeValue'] ?? '';
    if (!$selected_attribute_value || !is_string($selected_attribute_value) || !isset($this->getOptions('attribute', $selected_attribute)[$selected_attribute_value])) {
      return $element;
    }

    // Setting hidden input for the attribute value component.
    $element['attributeValue']['#type'] = 'hidden';
    $element['attributeValue']['#value'] = $selected_attribute_value;

    return $element;
  }

}
