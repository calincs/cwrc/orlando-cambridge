<?php

namespace Drupal\orlando_interface_as\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdvancedSearchStandaloneForm extends FormBase {

  /**
   * The entity storage for the facet mapping.
   *
   * @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage
   */
  protected $facetMappingStorage;

  /**
   * Constructs AdvancedSearchStandaloneForm instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->facetMappingStorage = $entity_type_manager->getStorage('basex_facet_mapping');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'orlando_interface_as__standalone_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $advanced_mapping */
    $advanced_mapping = $this->facetMappingStorage->load('advanced');
    /** @var \Drupal\orlando_interface_as\Plugin\orlando_interface_search\FacetWidget\SyntaxComponents $facet_widget */
    $facet_widget = $advanced_mapping->getFacetWidget();

    $form['keys'] = [
      '#type' => 'search',
      '#title' => $this->t('Search'),
      '#title_display' => 'invisible',
      '#field_prefix' => '<span class="tw-font-semibold tw-mr-2">' . $this->t('Search term') . '</span>',
      '#size' => 30,
      '#placeholder' => $this->t('search'),
      '#attributes' => [
        'class' => ['tw-max-w-md'],
      ],
      '#wrapper_attributes' => [
        'class' => [
          'tw-flex',
          'tw-justify-center',
          'tw-items-center',
        ]
      ],
    ];

    $form['#query_parameters'] = [];
    $form['#advanced_facet_mapping'] = $advanced_mapping;
    $form['#display_context'] = 'standalone';
    $form['advanced'] = $facet_widget->formElement($form, $form_state);

    $form['actions'] = [
      '#type' => 'actions',
      '#attributes' => [
        'class' => [
          'tw-flex',
          'tw-justify-center',
          'tw-mt-13',
        ],
      ],
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => ['class' => ['button--primary']],
    ];
    return $form;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $query = [];

    // Extract selected advanced values by skipping empty values.
    foreach ($values['advanced'] as $element_key => $value) {
      if (!$value) { continue; }
      $query['advanced'][$element_key] = $value;
    }

    // Only adding the keys to the query if a term was added.
    if (!empty($values['keys'])) {
      $query['keys'] = $values['keys'];
    }
    $form_state->setRedirect('orlando_interface_search.page', [], [
      'query' => $query,
    ]);
  }

}
