<?php

namespace Drupal\orlando_interface_as\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the syntax component entity edit forms.
 */
class SyntaxComponentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface $component */
    $component = $this->entity;

    if (empty($form['children']['widget']['#options']) || count($form['children']['widget']['#options']) < 2) {
      return $form;
    }

    $bundle = $component->bundle();
    $component_type_storage = $this->entityTypeManager->getStorage('oi_syntax_component_type');
    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentTypeInterface $bundle_entity */
    $bundle_entity = $component_type_storage->load($bundle);
    if ($bundle_entity->getChildrenMachineName() !== 'attributeValue') {
      return $form;
    }

    $component_storage = $this->entityTypeManager->getStorage($component->getEntityTypeId());
    $options = [];
    foreach ($form['children']['widget']['#options'] as $id => $label) {
      if ($id === '_none' || !is_int($id)) {
        $options[$id] = $label;
        continue;
      }
      /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface $attribute_value */
      $attribute_value = $component_storage->load($id);
      $options[$id] = $label . ' (' . $attribute_value->getMachineName() . ')';
    }
    $form['children']['widget']['#options'] = $options;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toString();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $link];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New syntax component %label has been created.', $message_arguments));
      $this->logger('orlando_interface_as')->notice('Created new syntax component %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The syntax component %label has been updated.', $message_arguments));
      $this->logger('orlando_interface_as')->notice('Updated new syntax component %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.oi_syntax_component.canonical', ['oi_syntax_component' => $entity->id()]);
  }

}
