<?php

namespace Drupal\orlando_interface_as\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for syntax component type forms.
 */
class SyntaxComponentTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentTypeInterface $syntax_component_type */
    $syntax_component_type = $this->entity;

    if ($this->operation === 'add') {
      $form['#title'] = $this->t('Add a Syntax component type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit %label syntax component type',
        ['%label' => $syntax_component_type->label()]
      );
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $syntax_component_type->label(),
      '#description' => $this->t('The human-readable name of this syntax component type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    // To allow machine name with uppercase letters.
    $replace_pattern = '[^a-zA-Z0-9_]+';
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $syntax_component_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\orlando_interface_as\Entity\SyntaxComponentType', 'load'],
        'source' => ['label'],
        'replace_pattern' => $replace_pattern,
      ],
      '#description' => $this->t('A unique machine-readable name for this syntax component type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['children_label'] = [
      '#title' => $this->t('Children Label'),
      '#type' => 'textfield',
      '#default_value' => $syntax_component_type->getChildrenLabel(),
      '#description' => $this->t('The human-readable name of this syntax component type children label. Enter ":none" when no children label.', [
        ':none' => '<none>',
      ]),
      '#required' => TRUE,
      '#size' => 30,
    ];
    $form['children_machine_name'] = [
      '#type' => 'machine_name',
      '#default_value' => $syntax_component_type->getChildrenMachineName(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
//        'exists' => ['Drupal\orlando_interface_as\Entity\SyntaxComponentType', 'load'],
        'source' => ['children_label'],
        'replace_pattern' => $replace_pattern,
      ],
      '#description' => $this->t('A unique machine-readable name for this syntax component type children machine name. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);
    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentTypeInterface $syntax_component_type */
    $syntax_component_type = $this->entity;

    $t_args = ['%name' => $syntax_component_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The syntax component type %name has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The syntax component type %name has been added.', $t_args);
    }
    $this->messenger()->addStatus($message);

    $form_state->setRedirectUrl($syntax_component_type->toUrl('collection'));
  }

}
