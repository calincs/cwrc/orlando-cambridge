<?php

namespace Drupal\orlando_interface_as\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Environment;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Controller\DbUpdateController;

class SyntaxComponentsBulkUploadForm extends FormBase {

  /**
   * Uploaded file entity.
   *
   * @var \Drupal\file\Entity\File
   */
  protected $file;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orlando_interface_as__syntax_component_bulk_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $validators = [
      'file_validate_extensions' => ['json'],
      'file_validate_size' => [Environment::getUploadMaxSize()],
    ];

    $form['json_format_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Expected Json format example'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['json_format_wrapper']['json_format'] = [
      '#type' => 'html_tag',
      '#tag' => 'pre',
      '#attributes' => ['style' => 'background-color: #f3f4f9;'],
      'code' => [
        '#type' => 'html_tag',
        '#tag' => 'code',
        '#value' => $this->jsonTextExample(),
      ],
    ];

    $form['json_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Upload file'),
      '#multiple' => FALSE,
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t('A JSON formatted file.'),
        '#upload_validators' => $validators,
      ],
      '#upload_validators' => $validators,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $all_files = $this->getRequest()->files->get('files', []);
    if (!empty($all_files['json_upload'])) {
      /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file_upload */
      $file_upload = $all_files['json_upload'];
      if ($file_upload->isValid()) {
        $form_state->setValue('json_upload', $file_upload->getRealPath());
      }
    }
    else {
      $form_state->setError($form['json_upload'], $this->t('The json file upload failed! Try again later.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (($json_upload = $form_state->getValue('json_upload', '')) && ($json = file_get_contents($json_upload))) {
      $data = Json::decode($json);
      $attributes = $data['attributes'] ?? [];
      $tags = $data['elements'] ?? [];
      $items_count = count($attributes) + count($tags);

      if ($items_count) {
        $batch_builder = (new BatchBuilder())
          ->setTitle($this->t('Syntax component bulk upload'))
          ->setInitMessage($this->t('Starting bulk uploads'))
          ->setProgressMessage($this->t('Processed @current out of @total.'))
          ->setErrorMessage($this->t('An unrecoverable error has occurred. You can find the error message below. It is advised to copy it to the clipboard for reference.'))
          ->setFinishCallback([SyntaxComponentsBulkUploadForm::class, 'batchFinished']);

        $callback = [SyntaxComponentsBulkUploadForm::class, 'batchProcess'];
        foreach ($attributes as $id => $item) {
          $item['id'] = Xss::filter($id);
          $item['label'] = Xss::filter($item['label']);
          $item['type'] = 'attribute';
          $batch_builder->addOperation($callback, [$item, $items_count]);
        }
        foreach ($tags as $id => $item) {
          $item['id'] = Xss::filter($id);
          $item['label'] = Xss::filter($item['label']);
          $item['type'] = 'tag';
          $batch_builder->addOperation($callback, [$item, $items_count]);
        }
        batch_set($batch_builder->toArray());
      }
    }
    $form_state->setRedirect('entity.oi_syntax_component.collection');
  }

  /**
   * Processes the batch operation.
   *
   * @param array $item
   *   The item to be processed.
   * @param $items_count
   *   The max item count.
   * @param $context
   *   The context.
   *
   * @return void
   */
  public static function batchProcess(array $item, $items_count, &$context) {
    // 1.a Initialize the sandbox if this the first batch.
    if (empty($context['sandbox'])) {
      $context['sandbox'] = [];
      $context['sandbox']['max'] = $items_count;
      $context['sandbox']['current'] = 1;
      // Total number processed for this migration.
      $context['sandbox']['progress'] = 0;
      $context['results']['failures'] = 0;
      $context['results']['successes'] = 0;
      // To track processed items with their drupal ids so that we don't have
      // to reprocessed things twice if we need to retrieve them.
      $context['results']['tag'] = [];
      $context['results']['attribute'] = [];
      $context['results']['attributeValue'] = [];
    }
    else {
      // 1.b Or increment the current sandbox tracker.
      $context['sandbox']['current']++;
    }

    // 2. Get the item type to know how to proceed. The type can only be "tag"
    // or "attribute". This is guaranteed by line 112 and 118.
    $syntax_component_type = $item['type'];

    // 3. Process the item children first and store their ids.
    // 3.a Get the children type.
    $item['children_ids'] = [];
    $is_tag = $syntax_component_type === 'tag';
    if ($is_tag) {
      $item_children_machine_name = 'attribute';
      $children = $item['attributes'] ?? [];
    }
    else {
      $item_children_machine_name = 'attributeValue';
      $children = $item['values'] ?? [];
    }
    // Track if items has children. We use this to determine if the children
    // were also successfully processed.
    $children_count = count($children);
    // 3.b Process the children.
    foreach ($children as $key => $child) {
      // For attributes child the $key is just an index while for values the
      // $key is the attributeValue mapped to its label.
      $child_id = $is_tag ? $child : $key;
      $child_item = [
        'id' => $child_id,
        'type' => $item_children_machine_name,
        // @todo Maybe if we were processing the tags first we should get the
        // labels for attributes in the json
        // data['attributes'][$child_id]['label'].
        'label' => !$is_tag ? $child : $key,
      ];
      if (isset($context['results'][$item_children_machine_name][$child_id])) {
        $item['children_ids'][$child_id] = $context['results'][$item_children_machine_name][$child_id];
        continue;
      }

      // Retrieve the item from drupal.
      $child_item_drupal_id = static::retrieveComponentIdByMachineName($child_item);
      $context['results'][$item_children_machine_name][$child_id] = $child_item_drupal_id;
      $item['children_ids'][$child_id] = $child_item_drupal_id;
    }

    // 4. Process the item.
    // 4.a Retrieve the item drupal id.
    $item_drupal_id = static::retrieveComponentIdByMachineName($item);
    // 4.b Store its drupal id in the result tracker.
    $context['results'][$item['type']][$item['id']] = $item_drupal_id;
    // When the item is processed and its children ids match the children count,
    // we can increment the succeeded results.
    if ($children_count === count($item['children_ids'])) {
      $context['results']['successes']++;
    }
    else {
      $context['results']['failures']++;
    }
    $context['sandbox']['progress']++;

    // 5. Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Finishes the bulk upload process and stores the results for eventual display.
   *
   * @param $success
   *   Indicate that the batch API tasks were all completed successfully.
   * @param array $results
   *   An array of all the results that were updated in update_do_one().
   * @param array $operations
   *   A list of all the operations that had not been completed by the batch API.
   */
  public static function batchFinished($success, array $results, array $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $failures_count = $results['failures'];
      $successes_count = $results['successes'];
      $tags = $results['tag'] ?? [];
      $attributes = $results['attribute'] ?? [];
      $attribute_values = $results['attributeValue'] ?? [];
      $messenger->addMessage(t('@count items succeeded.', [
        '@count' => $successes_count,
      ]));
      $messenger->addMessage(t('@tags were processed.<br/>@attr were processed.<br/>@val were processed', [
        '@tags' => count($tags),
        '@attr' => count($attributes),
        '@val' => count($attribute_values),
      ]));
      if ($failures_count) {
        $messenger->addWarning(t('At least @count items failed.', [
          '@count' => $failures_count,
        ]));
      }
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addError(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }

  }

  public static function retrieveComponentIdByMachineName(array $item): int {
    $component_storage = \Drupal::entityTypeManager()->getStorage('oi_syntax_component');
    $ids = $component_storage->getQuery()->accessCheck(FALSE)
      ->condition('type', $item['type'])
      ->condition('machine_name', $item['id'])
      ->range(0, 1)
      ->execute();
    if ($ids) {
      return reset($ids);
    }

    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface $component */
    $component = $component_storage->create([
      'type' => $item['type'],
      'title' => trim(Xss::filter($item['label'])),
      // @todo maybe validate the machine name?
      'machine_name' => trim(Xss::filter($item['id'])),
    ]);
    // Save children if they were provided.
    if (!empty($item['children_ids'])) {
      $children = $component->get('children');
      foreach ($item['children_ids'] as $id) {
        if (!is_integer($id)) {
          continue;
        }
        $children->appendItem(['target_id' => $id]);
      }
      $component->children = $children;
    }
    $component->save();
    return $component->id();
  }

  private function jsonTextExample(): string {
    return <<<JSON
{
  "elements": {
    "ADDRESS": {
      "label": "Address",
      "attributes": ["CURRENT","REG"]
    },
    "XREF": {
      "label": "Xref",
      "attributes": []
    },
    "YEAR": {
      "label": "Year",
      "attributes": ["REG", "VALUE"]
    }
  },
  "attributes": {
    "ACTIVISM": {
      "label": "Activism",
      "values": {
        "ACTIVISTYES": "yes",
        "ACTIVISTNO": "no"
      }
    },
    "CURRENT": {"label": "Current"},
    "INFLUENCETYPE": {
      "label": "Influence Type",
      "values": {
        "LITERARY": "literary",
        "INTELLECTUAL": "intellectual",
        "FAMILIAL": "familial"
      }
    },
  }
}
JSON;
  }

}
