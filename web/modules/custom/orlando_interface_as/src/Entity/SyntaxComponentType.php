<?php

namespace Drupal\orlando_interface_as\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Syntax component type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "oi_syntax_component_type",
 *   label = @Translation("Syntax component type"),
 *   label_collection = @Translation("Syntax component types"),
 *   label_singular = @Translation("Syntax component type"),
 *   label_plural = @Translation("Syntax component types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count syntax component type",
 *     plural = "@count syntax component types",
 *   ),
 *   handlers = {
 *     "access" = "\Drupal\entity\BundleEntityAccessControlHandler",
 *     "list_builder" = "Drupal\orlando_interface_as\SyntaxComponentTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\orlando_interface_as\Form\SyntaxComponentTypeForm",
 *       "edit" = "Drupal\orlando_interface_as\Form\SyntaxComponentTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer oi_syntax_component",
 *   bundle_of = "oi_syntax_component",
 *   config_prefix = "syntax_component_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/oi-syntax-component-types/add",
 *     "edit-form" = "/admin/structure/oi-syntax-component-types/manage/{oi_syntax_component_type}",
 *     "delete-form" = "/admin/structure/oi-syntax-component-types/manage/{oi_syntax_component_type}/delete",
 *     "collection" = "/admin/structure/oi-syntax-component-types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "children_label",
 *     "children_machine_name",
 *   }
 * )
 */
class SyntaxComponentType extends ConfigEntityBundleBase implements SyntaxComponentTypeInterface {

  /**
   * The machine name of this syntax component type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the syntax component type.
   *
   * @var string
   */
  protected $label;

  /**
   * The human-readable name of the children label of a syntax component type.
   *
   * @var string
   */
  protected $children_label;

  /**
   * The machine name of the children label of a syntax component type.
   *
   * @var string
   */
  protected $children_machine_name;

  /**
   * {@inheritdoc}
   */
  public function getChildrenLabel() {
    return $this->children_label;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildrenMachineName() {
    return $this->children_machine_name;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $children_machine_name = $this->getChildrenMachineName();

    if (!empty($children_machine_name) && $children_machine_name !== '_none_') {
      // Adding the children machine name as a dependency of this
      $prefix = $this->getEntityType()->get('config_prefix');
      $this->addDependency('config', 'orlando_interface_as.' . $prefix . '.' . $this->getChildrenMachineName());
    }
    return $this;
  }

}
