<?php

namespace Drupal\orlando_interface_as\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a syntax component entity type.
 */
interface SyntaxComponentInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the syntax component title.
   *
   * @return string
   *   Title of the syntax component.
   */
  public function getTitle();

  /**
   * Sets the syntax component title.
   *
   * @param string $title
   *   The syntax component title.
   *
   * @return \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface
   *   The called syntax component entity.
   */
  public function setTitle(string $title);

  /**
   * Gets the syntax component machine name.
   *
   * @return string
   *   Machine name of the syntax component.
   */
  public function getMachineName();

  /**
   * Sets the syntax component machine name.
   *
   * @param string $machine_name
   *   The syntax component tMachine name.
   *
   * @return \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface
   *   The called syntax component entity.
   */
  public function setMachineName(string $machine_name);

  /**
   * Gets the syntax component description.
   *
   * @return string
   *   The the syntax component description.
   */
  public function getDescription();

  /**
   * Sets the syntax component description.
   *
   * @param string $description
   *   The syntax component description.
   *
   * @return $this
   */
  public function setDescription(string $description);

  /**
   * Gets the text format name for the syntax component description.
   *
   * @return string
   *   The text format name.
   */
  public function getFormat();

  /**
   * Sets the text format name for the syntax component description.
   *
   * @param string $format
   *   The text format name.
   *
   * @return $this
   */
  public function setFormat(string $format);

  /**
   * Gets the syntax component children entities.
   *
   * @param string $array_key
   *   The array key to use when returning the children. valid value id or
   *   machine name.
   *
   * @return array|\Drupal\orlando_interface_as\Entity\SyntaxComponentInterface[]
   *   The array keyed by entity id or machine name.
   */
  public function getChildren(string $array_key = 'id'): array;

  /**
   * Gets the syntax component creation timestamp.
   *
   * @return int
   *   Creation timestamp of the syntax component.
   */
  public function getCreatedTime();

  /**
   * Sets the syntax component creation timestamp.
   *
   * @param int $timestamp
   *   The syntax component creation timestamp.
   *
   * @return \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface
   *   The called syntax component entity.
   */
  public function setCreatedTime(int $timestamp);

}
