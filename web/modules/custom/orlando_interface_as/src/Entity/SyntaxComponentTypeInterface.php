<?php

namespace Drupal\orlando_interface_as\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining a syntax component type entities.
 */
interface SyntaxComponentTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the children label of the syntax component.
   *
   * @return string
   *   The children label of the syntax component.
   */
  public function getChildrenLabel();

  /**
   * Gets the children machine name of the syntax component.
   *
   * @return string
   *   The children machine name of the syntax component.
   */
  public function getChildrenMachineName();

}
