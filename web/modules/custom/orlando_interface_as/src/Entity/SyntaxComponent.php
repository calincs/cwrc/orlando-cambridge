<?php

namespace Drupal\orlando_interface_as\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the syntax component entity class.
 *
 * @ContentEntityType(
 *   id = "oi_syntax_component",
 *   label = @Translation("Syntax component"),
 *   label_collection = @Translation("Syntax components"),
 *   label_singular = @Translation("syntax component"),
 *   label_plural = @Translation("syntax components"),
 *   label_count = @PluralTranslation(
 *     singular = "@count syntax component",
 *     plural = "@count syntax components",
 *   ),
 *   bundle_label = @Translation("Type"),
 *   handlers = {
 *     "storage" = "\Drupal\orlando_interface_as\SyntaxComponentStorage",
 *     "access" = "\Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "\Drupal\entity\EntityPermissionProvider",
 *     "form" = {
 *       "add" = "Drupal\orlando_interface_as\Form\SyntaxComponentForm",
 *       "edit" = "Drupal\orlando_interface_as\Form\SyntaxComponentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "\Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "list_builder" = "Drupal\orlando_interface_as\SyntaxComponentListBuilder",
 *     "views_data" = "\Drupal\entity\EntityViewsData",
 *   },
 *   base_table = "oi_syntax_component",
 *   data_table = "oi_syntax_component_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer oi_syntax_component",
 *   permission_granularity = "bundle",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-page" = "/oi-syntax-component/add",
 *     "add-form" = "/oi-syntax-component/add/{oi_syntax_component_type}",
 *     "edit-form" = "/oi-syntax-component/{oi_syntax_component}/edit",
 *     "canonical" = "/oi-syntax-component/{oi_syntax_component}",
 *     "collection" = "/admin/content/oi-syntax-component",
 *     "delete-form" = "/oi-syntax-component/{oi_syntax_component}/delete",
 *     "delete-multiple-form" = "/oi-syntax-component/delete",
 *   },
 *   bundle_entity_type = "oi_syntax_component_type",
 *   field_ui_base_route = "entity.oi_syntax_component_type.edit_form",
 * )
 */
class SyntaxComponent extends ContentEntityBase implements SyntaxComponentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMachineName() {
    return $this->get('machine_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMachineName(string $machine_name) {
    $this->set('machine_name', $machine_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormat() {
    return $this->get('description')->format;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormat(string $format) {
    $this->get('description')->format = $format;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChildren(string $array_key = 'id'): array {
    $children = $this->get('children');
    if ($children->isEmpty()) {
      return [];
    }
    $entities = [];
    $id_as_key = $array_key === 'id';
    foreach ($children as $child) {
      /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentInterface $entity */
      $entity = $child->entity;
      $key = $id_as_key ? $entity->id() : $entity->getMachineName();
      $entities[$key] = $entity;
    }
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The label of the syntax component entity.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['machine_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Machine name'))
      ->setDescription(t('The machine name of the syntax component entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->addConstraint('UniqueFieldByBundle', [])
      ->setDisplayOptions('form', [
        'type' => 'machine_name',
        'weight' => -5,
        'settings' => [
          'source_field' => 'title',
          'disable_on_edit' => TRUE,
          'standalone' => TRUE,
          'replace_pattern' => '[^a-zA-Z0-9_-]+',
          'replace' => '_',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['children'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Children/Sub components'))
      ->setDescription(t('The children/sub components of this component.'))
      ->setSetting('target_type', 'oi_syntax_component')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the syntax component.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'text_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the syntax component was created.'))
      ->setTranslatable(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the syntax component was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions */

    // Only syntax component in the children machine name bundle can be a child.
    $bundle_storage = \Drupal::entityTypeManager()->getStorage('oi_syntax_component_type');
    $fields['children'] = clone $base_field_definitions['children'];
    if (!$bundle) {
      return $fields;
    }

    /** @var \Drupal\orlando_interface_as\Entity\SyntaxComponentTypeInterface $syntax_component_type */
    $syntax_component_type = $bundle_storage->load($bundle);
    $child_label = $syntax_component_type->getChildrenLabel();
    $fields['children']->setLabel($child_label);
    $fields['children']->setDescription(t('@child_label of this component.',[
      '@child_label' => $child_label,
    ]));
    // @todo update the syntax component type form to enforce the selection of
    // the machine name to be an existing bundle.
    $children_machine_name = $syntax_component_type->getChildrenMachineName();
    $fields['children']->setSetting('handler_settings', [
      'target_bundles' => [$children_machine_name => $children_machine_name],
    ]);
    return $fields;
  }

}
