<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:output encoding="UTF-8" method="xml" indent="yes" omit-xml-declaration="no" />

    <xsl:strip-space elements="*"></xsl:strip-space>
    <xsl:variable name="source"><xsl:value-of select="ENTRY/@ID"/></xsl:variable>
    <xsl:variable name="VAR_FILENAME_PREFIX" select="'orlando_'" />
    <xsl:template match="/">
        <xsl:apply-templates select="//CHRONSTRUCT"/>
    </xsl:template>

  <!-- facets -->
  <xsl:template match="NATIONALITY">
    <xsl:copy-of select="self::node()/."/>
  </xsl:template>

  <xsl:template match="NATIONALHERITAGE">
    <xsl:copy-of select="self::node()/."/>
  </xsl:template>

  <xsl:template match="GENDER">
    <xsl:copy-of select="self::node()/."/>
  </xsl:template>

    <xsl:template match="CHRONSTRUCT">
        <xsl:variable name="filename" select="concat($VAR_FILENAME_PREFIX,@id)" />
<!--      <xsl:result-document href="{resolve-uri('output.xml', static-base-uri())}">-->
        <xsl:result-document indent="yes" href="{$filename}.xml" omit-xml-declaration="no" encoding="UTF-8" method="xml">
<!--        <xsl:result-document indent="yes" href="{resolve-uri('{$filename}.xml', static-base-uri())}" omit-xml-declaration="no" encoding="UTF-8" method="xml">-->
            <EMBEDDEDEVENT><xsl:attribute name="SOURCE"><xsl:value-of select="$source"/></xsl:attribute>
              <!--<xsl:value-of select="$Nationality"/>-->
              <NATIONALITY_FACET><xsl:apply-templates select="//NATIONALITY"/><xsl:apply-templates select="//NATIONALHERITAGE"/></NATIONALITY_FACET>
              <GENDER_FACET><xsl:apply-templates select="//GENDER"/></GENDER_FACET>
                <xsl:copy-of select="."/>
            </EMBEDDEDEVENT>
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>
