<?php

namespace Drupal\orlando_interface_ingestion;

use Drupal\Component\Datetime\DateTimePlus;

trait DateUtilitiesTrait {

  public function extractDateParts(string $date, bool $skip_extraction = FALSE): string {
    if ($skip_extraction && preg_match($this->getRegexPattern(), $date) === 1) {
      return $date;
    }
    [$year, $month, $day] = array_pad(explode('-', $date, 3), 3, '01');
    return "$year-$month-$day";
  }

  public function recoupFormatFromDate(string $date): string {
    $date_parts = explode('-', $date);
    $count = count($date_parts);
    if ($count === 1) {
      return static::YEAR_ONLY_FORMAT;
    }
    if ($count === 2) {
      return static::YEAR_MONTH_FORMAT;
    }
    return static::SCHEMA_DATE_FORMAT;
  }

  protected function extractStartEndDatesFromValue(string $value, string $type, bool $skip_extraction = FALSE): array {
    $dates = [
      'start' => '',
      'end' => '',
    ];
    if ($skip_extraction && preg_match($this->getRegexPattern(), $value) === 1) {
      $dates[$type ?: 'start'] = $value;
      return $dates;
    }

    $dates_data = array_combine(array_keys($dates), explode('-', $value, 2));
    foreach ($dates_data as $position => $item) {
      $item = trim($item);
      if (strlen($item) === 4 && is_numeric($item)) {
        $date = DateTimePlus::createFromArray(['year' => $item]);
      }
      else {
        $date = new DateTimePlus($item);
      }
      $dates[$position] = $date->format('Y-m-d');
    }
    return $dates;
  }

  protected function getRegexPattern(): string {
    return '/^(?:[\+-]?\d{4}(?!\d{2}\b))(?:(-?)(?:(?:0[1-9]|1[0-2])(?:\1(?:[12]\d|0[1-9]|3[01]))?|W(?:[0-4]\d|5[0-2])(?:-?[1-7])?|(?:00[1-9]|0[1-9]\d|[12]\d{2}|3(?:[0-5]\d|6[1-6])))(?:[T\s](?:(?:(?:[01]\d|2[0-3])(?:(:?)[0-5]\d)?|24\:?00)(?:[\.,]\d+(?!:))?)?(?:\2[0-5]\d(?:[\.,]\d+)?)?(?:[zZ]|(?:[\+-])(?:[01]\d|2[0-3]):?(?:[0-5]\d)?)?)?)?$/';
  }

}
