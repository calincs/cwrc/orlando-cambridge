<?php

namespace Drupal\orlando_interface_ingestion;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orlando_interface_ingestion\Entity\ProfileEvent;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A helper service which allow us to alter views query.
 */
class ViewsQueryAlter implements ContainerInjectionInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ViewsQueryAlter instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Implements a hook bridge for hook_views_query_alter().
   *
   * @see hook_views_query_alter()
   * @see orlando_interface_ingestion_views_query_alter()
   */
  public function alterQuery(ViewExecutable $view, QueryPluginBase $query) {
    if ($view->id() !== 'events' && $view->current_display !== 'page_timeline') {
      return;
    }

    // Getting the author profile argument if it was set as contextual filter.
    $profile = $view->argument['profile'] ?? NULL;
    // Initializing the author profile node which will then be used to extract
    // the person taxonomy term. In case the initialization fails we don't need
    // to do the rest of the work.
    if (!$profile || empty($profile->argument) || empty($profile->argument_validated)) {
      return;
    }

    // Extracting the person taxonomy term id in order to collect all the
    // connections with profile event as source.
    $author_profile = $this->entityTypeManager->getStorage('node')->load($profile->argument);
    $person_tid = $author_profile->field_person->target_id;
    $connection_storage = $this->entityTypeManager->getStorage('oi_connection');
    $connection_ids = $connection_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('source.target_type', 'profile_event')
      ->condition('subject.target_type', 'taxonomy_term')
      ->condition('subject.target_id', $person_tid)
      ->execute();

    if (!$connection_ids) {
      return;
    }

    $empty_group = $this->extractEmptyWhereGroup($query);
    $empty_group = $this->rearrangeWhereConditions($view, $query, $empty_group);

    // A flag to let us know if we should change the where grouping.
    $query_updated = FALSE;
    // All the regular contextual filters are grouped in index zero, so we are
    // going to use our event ids as contextual filters as well. This allows
    // the exposed filters to continue to work as expected.
    $where_group = $empty_group ?: 0;
    foreach ($connection_ids as $connection_id) {
      /** @var \Drupal\orlando_interface_pp\Entity\ConnectionInterface $connection */
      $connection = $connection_storage->load($connection_id);
      $event = $connection->getSource();
      // We are only interested on standalone events.
      if ($event instanceof ProfileEvent && !$event->isEmbedded()) {
        // Add a where query based on this event id.
        $query->addWhere($where_group, "$profile->tableAlias.id", $event->id(), '=');
        $query_updated = TRUE;
      }
    }
    if ($query_updated) {
      $query->setWhereGroup('OR', $where_group);
    }
  }

  /**
   * Re-arranges where conditions and returns the group where the contextual
   * filters were stored.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view executable.
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
   *   The view query.
   * @param $empty_group
   *   The empty group from extractEmptyWhereGroup().
   *
   * @return int
   *   The group id where the contextual filters were stored.
   */
  private function rearrangeWhereConditions(ViewExecutable $view, QueryPluginBase $query, $empty_group) {
    $contextual_filters_count = count($view->argument);
    if ($contextual_filters_count >= 1) {
      // Set an alias.
      $groups = &$query->where;
      // All the regular contextual filters are grouped in index zero.
      $conditions = $groups[0]['conditions'];
      // They are the last one to be added. So let set the offset to negative.
      $offset = -1 * abs($contextual_filters_count);
      // Extracting them from index zero conditions.
      $contextual_filters_where_conditions = array_splice($conditions, $offset, $contextual_filters_count);
      // Restore the rest of the conditions.
      $groups[0]['conditions'] = $conditions;
      // Then move the contextual filters into to their own grouping.
      if ($empty_group && isset($groups[$empty_group]['conditions'])) {
        $groups[$empty_group]['conditions'] = $contextual_filters_where_conditions;
      }
      else {
        $empty_group = max(array_keys($groups)) + 1;
        $type = $groups[0]['type'];
        $query->setWhereGroup($type, $empty_group);
        foreach ($contextual_filters_where_conditions as $condition) {
          $query->addWhere($empty_group, $condition['field'], $condition['value'], $condition['operator']);
        }
      }
    }
    return $empty_group;
  }

  /**
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
   *   The view query.
   *
   * @return int|null
   *   Null if there are no where conditions, an empty where group id otherwise.
   */
  private function extractEmptyWhereGroup(QueryPluginBase $query) {
    foreach ($query->where as $group => $value) {
      if (empty($value['conditions'])) {
        return $group;
      }
    }
    return NULL;
  }

}
