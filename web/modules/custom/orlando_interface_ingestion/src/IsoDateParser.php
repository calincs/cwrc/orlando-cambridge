<?php

namespace Drupal\orlando_interface_ingestion;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\BibciteReferenceRepository;

class IsoDateParser {

  use DateUtilitiesTrait;

  const SCHEMA_DATE_FORMAT = 'Y-m-d';
  const YEAR_ONLY_FORMAT = 'Y';
  const YEAR_MONTH_FORMAT = 'Y-m';

  public function xmlProcessor(array $data, bool $skip_extraction = FALSE, string &$open_ended_date_key = ''): array {
    $dates = [
      'start' => '',
      'end' => '',
      'open_ended_date_key' => '',
    ];

    $valid_value_types = ['start', 'end'];

    $items_count = count($data);
    if ($items_count === 0) {
      return $dates;
    }

    $value1 = (string) $data[0][0];
    $value1_data = (array) $data[0];
    $value1_type = $value1_data['@attributes']['type'] ?? '';
    if ($value1_type === 'end') {
      $dates['open_ended_date_key'] = 'published_to';
    }
    elseif ($value1_type === 'start') {
      $dates['open_ended_date_key'] = 'to_present';
    }

    if ($items_count === 1) {
      if ($dates['open_ended_date_key'] && $value1_type) {
        $dates[$value1_type] = $this->extractDateParts($value1, $skip_extraction);
        return $dates;
      }

      $dates_part = explode('-', $value1);
      if (count($dates_part) === 2) {
        return $this->extractStartEndDatesFromValue($value1, $value1_type, $skip_extraction) + $dates;
      }

      $dates['start'] = $this->extractDateParts($value1, $skip_extraction);
      return $dates;
    }

    $value2 = (string) ($data[1][0] ?? '');
    $value2_data = (array) $data[1];
    $value2_type = $value2_data['@attributes']['type'] ?? '';
    if (!$dates['open_ended_date_key'] && $value2_type === 'end') {
      $dates['open_ended_date_key'] = 'published_to';
    }
    elseif (!$dates['open_ended_date_key'] && $value2_type === 'start') {
      $dates['open_ended_date_key'] = 'to_present';
    }
    if ($items_count === 2) {
      if (in_array($value1_type, $valid_value_types, TRUE) && in_array($value2_type, $valid_value_types, TRUE)) {
        $dates[$value1_type] = $this->extractDateParts($value1, $skip_extraction);
        $dates[$value2_type] = $this->extractDateParts($value2, $skip_extraction);
        $dates['open_ended_date_key'] = '';
        return $dates;
      }
      if (!str_ends_with($value1, '-') && str_contains($value1, '-')) {
        $dates = $this->extractStartEndDatesFromValue($value1, $value1_type, $skip_extraction);
        if ($dates['start'] === NULL && $value2) {
          $dates['start'] = $this->extractDateParts($value2, $skip_extraction);
          $dates['end'] = '';
        }
        return $dates;
      }
      elseif (str_ends_with($value1, '-')) {
        $value1 = rtrim($value1, '-');
      }

      if (strlen($value2) === 4 && strlen($value1) > 4) {
        $value1 = trim($value1, '[]');
        $date = new DateTimePlus($value1);
        if (!$date->hasErrors()) {
          $dates['start'] = $date->format('Y-m-d');
        }
        else {
          unset($date);
          $dates['start'] = $this->extractDateParts($value2, $skip_extraction);
        }
        return $dates;
      }
      if (!$value2) {
        $dates['start'] = $this->extractDateParts($value1, $skip_extraction);
        return $dates;
      }

      $dates['start'] = $this->extractDateParts($value1, $skip_extraction);
      $dates['end'] = $this->extractDateParts($value2, $skip_extraction);
      return $dates;
    }

    if ($items_count === 3) {
      $value3 = (string) ($data[2][0] ?? '');
      if ((!$value2 || !$value3) && !str_ends_with($value1, '-') && str_contains($value1, '-')) {
        return $this->extractStartEndDatesFromValue($value1, $skip_extraction);
      }
      if ((!$value2 || !$value3) && str_ends_with($value1, '-')) {
        $value1 = rtrim($value1, '-');
        $date = new DateTimePlus($value1);
        $dates['start'] = $date->format('Y-m-d');
        return $dates;
      }

      $dates['start'] = $value2 ? $this->extractDateParts($value2, $skip_extraction) : '';
      $dates['end'] = $value3 ? $this->extractDateParts($value3, $skip_extraction) : '';
    }
    return $dates;
  }

  public function xmlProcessorUsingFileXpath($filename, $xpath, bool $skip_extraction = FALSE): array {
    $data = $this->extractDataFromFile($xpath, $filename);
    return $this->xmlProcessor($data, $skip_extraction);
  }

  private function extractDataFromFile(string $xpath, string $filename) {
    $xml = BibciteReferenceRepository::getSimpleXMLElement($filename, ['namespace' => 'http://www.loc.gov/mods/v3']);
    return $xml->xpath($xpath);
  }

}
