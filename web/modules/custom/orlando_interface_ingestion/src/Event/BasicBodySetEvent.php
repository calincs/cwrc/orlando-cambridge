<?php

namespace Drupal\orlando_interface_ingestion\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Contracts\EventDispatcher\Event;

class BasicBodySetEvent extends Event {

  protected $body;

  protected $source;

  protected $paragraph;

  protected $extracted_connections = [];

  public function __construct(string $body, EntityInterface $source, ParagraphInterface $paragraph) {
    $this->body = $body;
    $this->paragraph = $paragraph;
    $this->source = $source;
  }

  public function getParagraph(): ParagraphInterface {
    return $this->paragraph;
  }

  public function getBody(): string {
    return $this->body;
  }

  public function getSource(): EntityInterface {
    return $this->source;
  }

  public function setBody(string $body) {
    $this->body = $body;
    return $this;
  }

  public function getExtractedConnections() {
    return $this->extracted_connections;
  }

  public function setExtractedConnections(array $ids) {
    $this->extracted_connections = $ids;
    return $this;
  }

}
