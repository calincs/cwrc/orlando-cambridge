<?php

namespace Drupal\orlando_interface_ingestion\Event;

use Symfony\Contracts\EventDispatcher\Event;

abstract class IngestionEvent extends Event {

  protected $id;

  protected $type;

  protected $filepath;

  public function __construct(string $id, string $type, string $filepath) {
    $this->id = $id;
    $this->type = $type;
    $this->filepath = $filepath;
  }

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getFilepath(): string {
    return $this->filepath;
  }

}
