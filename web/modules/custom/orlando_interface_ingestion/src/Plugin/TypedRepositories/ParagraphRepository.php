<?php

namespace Drupal\orlando_interface_ingestion\Plugin\TypedRepositories;

use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * @TypedRepository(
 *   entity_type_id = "paragraph",
 *   wrappers = @ClassWithVariants(
 *     fallback = "Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\BaseParagraph",
 *     variants = {
 *       "Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Basic",
 *       "Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Heading",
 *       "Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Event",
 *     },
 *   ),
 *   description = @Translation("Repository that holds business logic applicable to all basic, heading and event paragraphs.")
 * )
 */
class ParagraphRepository extends TypedRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * The field that contains the external id of the entity.
   */
  const FIELD_ID = 'field_id';

  /**
   * Finds a taxonomy term by cwrc id.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Term|null
   *   The wrapped entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\typed_entity\InvalidValueException
   */
  public function findById(string $id) {
    $query = $this->getQuery();

    $entities = $query->condition(static::FIELD_ID, strtolower($id))
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function findBasicParagraphByBody(string $body) {
    $query = $this->getQuery();

    // @todo dispatch set event before searching by body.
    $entities = $query->condition('field_body', $body)
      ->accessCheck(FALSE)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createWrappedEntityFromObject(\stdClass $object, string $id_property = 'id') {
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'type' => $this->bundle,
        static::FIELD_ID => strtolower($object->{$id_property}),
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\BaseParagraph $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromObject($object);
    $wrapped_entity->save();
    return $wrapped_entity;
  }

}
