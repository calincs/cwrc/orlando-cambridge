<?php

namespace Drupal\orlando_interface_ingestion\Plugin\TypedRepositories;

use Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * @TypedRepository(
 *   entity_type_id = "media",
 *   bundle = "image",
 *   wrappers = @ClassWithVariants(
 *     fallback = "Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image",
 *   ),
 *   description = @Translation("Repository that holds business logic applicable to all image media."),
 * )
 */
class MediaRepository extends TypedRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * Finds a media entity by its attached filename.
   *
   * @param string $filename
   *   The filename to search for.
   * @param string $media_type
   *   The optional media type.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image|null
   *   The wrapped media entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function findMediaByFilename(string $filename, string $media_type = 'image'): ?Image {
    $field = $media_type === 'image' ? 'field_media_image.entity.uri' : '';
    if (!$field) {
      return NULL;
    }

    $entities = [];
    $ids = $this->getQuery()
      ->accessCheck(FALSE)
      ->condition($field, $filename)
      ->range(0, 1)
      ->execute();

    if ($ids) {
      $entities = $this->wrapMultipleById($ids);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createMediaImageFromData(string $file_uri, int $uid = 1): Image {
    $info = pathinfo($file_uri);
    $mod = str_replace($info['extension'], 'xml', $file_uri);
    $entity = $this->entityTypeManager->getStorage('media')
      ->create([
        'bundle' => 'image',
        'uid' => $uid,
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Media\Image $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setImageFile($file_uri);
    $wrapped_entity->setNameFromXml($mod);
    $wrapped_entity->save();
    return $wrapped_entity;
  }

  public function ingestMediaImagesByEntries(array $entries, bool $re_ingest = FALSE) {
    /** @var \Drupal\typed_entity\RepositoryManager $repository_manager */
    $repository_manager = \Drupal::service(RepositoryManager::class);
    assert($repository_manager instanceof RepositoryManager);

    $bundle = 'author_profile';
    /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\AuthorProfileRepository $repository */
    $repository = $repository_manager->repository('node', $bundle);
    if ($repository && !$repository->getBundle()) {
      $repository->setBundle($bundle);
    }
    foreach ($entries as $entry) {
      $wrapped_author_profile = $repository->findByCWRCId($entry);
      $field_images = $wrapped_author_profile ? $wrapped_author_profile->getFieldValue('field_images') : NULL;
      if ($field_images && ($field_images->isEmpty() || $re_ingest)) {
        if (!$field_images->isEmpty()) {
          $wrapped_author_profile->getEntity()->field_images = NULL;
        }
        $wrapped_author_profile->ingestMediaImages();
        $wrapped_author_profile->save();
      }
    }
  }

}
