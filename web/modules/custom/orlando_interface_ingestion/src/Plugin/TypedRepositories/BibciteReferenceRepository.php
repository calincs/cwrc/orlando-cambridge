<?php

namespace Drupal\orlando_interface_ingestion\Plugin\TypedRepositories;

use Drupal\file\Entity\File;
use Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences\BibCiteReference;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * @TypedRepository(
 *   entity_type_id = "bibcite_reference",
 *   wrappers = @ClassWithVariants(
 *     fallback = "Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences\BibCiteReference",
 *   ),
 *   description = @Translation("Repository that holds business logic applicable to all bibcite reference entities.")
 * )
 */
class BibciteReferenceRepository extends TypedRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * Finds a bibliography reference by cwrc id.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return mixed|null
   *   The wrapped entity or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function findByCWRCId(string $id): ?BibCiteReference {
    $extras = ['orlando:', 'orlando_', '.xml'];
    $id = str_replace($extras, '', $id);
    $query = $this->getQuery();

    $entities = $query->condition('cwrc_id', $id)
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createWrappedEntityFromXmlFile(string $external_id, string $filename, string $type = ''): BibCiteReference {
    $extras = ['orlando:', 'orlando_', '.xml'];
    $external_id = str_replace($extras, '', $external_id);
    $type = $type ?: static::extractTypeFromXml($filename);
    if (!$type || !is_string($type)) {
      throw new \Exception(sprintf('Couldn\'t determine or extract the bibliography type From %s file', $filename));
    }

    $file_uri = BibCiteReference::ITEMS_DIR . '/orlando_' . $external_id . '.xml';
    $file_storage = $this->entityTypeManager->getStorage('file');
    $files = $file_storage->loadByProperties(['uri' => $file_uri]);
    if ($files) {
      $file = reset($files);
    }
    else {
      $file = File::create([
        'uri' => $file_uri,
        'uid' => ['target_id' => 1],
        'status' => File::STATUS_PERMANENT,
      ]);
      $file->save();
    }
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'type' => $type,
        'cwrc_id' => $external_id,
        'uid' => ['target_id' => 1],
        'mod_file' => [
          'target_id' => $file->id(),
        ],
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences\BibCiteReference $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromXml($filename);
    $cup_product_id_mappings = $this->getCupProductIdMappings('bibcite_reference');
    if (($cup_product_id = $cup_product_id_mappings[$external_id] ?? '')) {
      $wrapped_entity->setCupProductId($cup_product_id);
    }
    $wrapped_entity->save();
    return $wrapped_entity;
  }

  public function getCupProductIdFilePrefix() {
    return 'bibliographic';
  }

  public static function getWrappedEntityByCWRCId(string $id): ?BibCiteReference {
    $wrapped_entity = NULL;
    $storage = \Drupal::entityTypeManager()->getStorage('bibcite_reference');
    $ids = $storage->getQuery()
      ->condition('cwrc_id', $id)
      ->range(0, 1)
      ->accessCheck(FALSE)
      ->execute();

    if ($ids && ($bibliography_reference = $storage->load(array_key_first($ids)))) {
      $wrapped_entity = \Drupal::service('Drupal\typed_entity\RepositoryManager')
        ->wrap($bibliography_reference);
    }
    return $wrapped_entity;
  }

  public static function extractTypeFromXml(string $filename): string {
    $bundle = '';
    if (!is_file($filename)) {
      return $bundle;
    }

    $type_xpath_mapping = [
      'audiovisual' => [
        '//*[local-name()="genre" and .="moving image"]',
        '//*[local-name()="genre" and .="videorecording"]',
        '//*[local-name()="genre" and .="video"]',
        '//*[local-name()="genre" and .="Video"]',
        '//*[local-name()="genre" and .="television"]',
        '//*[local-name()="genre" and .="television broadcast"]',
        '//*[local-name()="genre" and .="television broadcasting"]',
        '//*[local-name()="genre" and .="video recording"]',
        '//*[local-name()="genre" and .="sound recording"]',
        '//*[local-name()="genre" and .="voice recording"]',
        '//*[local-name()="genre" and .="sound"]',
        '//*[local-name()="genre" and .="Sound"]',
        '//*[local-name()="genre" and .="radio broadcast"]',
        '//*[local-name()="genre" and .="speech"]',
        '//*[local-name()="genre" and .="Speech"]',
        '//*[local-name()="typeOfResource" and .="moving image"]',
        '//*[local-name()="typeOfResource" and .="videorecording"]',
        '//*[local-name()="typeOfResource" and .="video"]',
        '//*[local-name()="typeOfResource" and .="Video"]',
        '//*[local-name()="typeOfResource" and .="television"]',
        '//*[local-name()="typeOfResource" and .="television broadcast"]',
        '//*[local-name()="typeOfResource" and .="television broadcasting"]',
        '//*[local-name()="typeOfResource" and .="video recording"]',
        '//*[local-name()="typeOfResource" and .="sound recording"]',
        '//*[local-name()="typeOfResource" and .="voice recording"]',
        '//*[local-name()="typeOfResource" and .="sound"]',
        '//*[local-name()="typeOfResource" and .="Sound"]',
        '//*[local-name()="typeOfResource" and .="radio broadcast"]',
        '//*[local-name()="typeOfResource" and .="speech"]',
        '//*[local-name()="typeOfResource" and .="Speech"]',
      ],
      'conference_paper' => [
        '//*[local-name()="genre" and .="Unpublished conference paper"]',
        '//*[local-name()="genre" and .="unpublished plenary lecture"]',
        '//*[local-name()="genre" and .="public speech"]',
      ],
      'correspondence' => ['//*[local-name()="genre" and text()="letter"]'],
      'journal' => [
        '//*[local-name()="genre" and text()="journal" and (following-sibling::*[local-name()="genre" and text()="periodical"] or preceding-sibling::*[local-name()="genre" and text() = "periodical"])]'
      ],
      'manuscript' => ['//*[local-name()="genre" and text()="script"]'],
      'book_chapter' => [
        '//*[local-name()="genre" and text()="article" and (following-sibling::*[local-name()="genre" and text()="book"] or preceding-sibling::*[local-name()="genre" and text()="book"])]',
        '//*[local-name()="genre" and .="Book Chapter"]',
        '//*[local-name()="genre" and .="book chapter"]',
        '//*[local-name()="genre" and .="Book chapter"]',
      ],
      'website' => [
        '//*[local-name()="genre" and .="Website"]',
        '//*[local-name()="genre" and .="Web site"]',
        '//*[local-name()="genre" and .="web-site"]',
        '//*[local-name()="genre" and .="Web Site"]',
        '//*[local-name()="genre" and .="website"]',
        '//*[local-name()="genre" and .="web site"]',
      ],
      'journal_article' => [
        '//*[local-name()="genre" and text()="article" and not(following-sibling::*[local-name()="genre" and text()="book"] or preceding-sibling::*[local-name()="genre" and text()="book"])]',
        '//*[local-name()="genre" and text()="Article" and not(following-sibling::*[local-name()="genre" and text()="book"] or preceding-sibling::*[local-name()="genre" and text()="book"])]',
      ],
      'book' => [
        '//*[local-name()="genre" and .="book" and not(following-sibling::*[local-name()="genre" and text()="article"])]',
        '//*[local-name()="genre" and .="Book" and not(following-sibling::*[local-name()="genre" and text()="article"])]',
      ],
    ];
    $xml_obj = new \SimpleXMLElement($filename, 0, TRUE);
    foreach ($type_xpath_mapping as $reference_type => $xpaths) {
      foreach ($xpaths as $xpath) {
        $xml_elt = $xml_obj->xpath($xpath);
        $data = (array) $xml_elt ?? [];
        if (!empty($data[0])) {
          $bundle = $reference_type;
          break;
        }
      }
      if ($bundle) {
        break;
      }
    }

    return $bundle;
  }

}
