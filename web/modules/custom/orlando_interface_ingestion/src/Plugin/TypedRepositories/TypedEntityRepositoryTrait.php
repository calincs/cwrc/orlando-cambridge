<?php

namespace Drupal\orlando_interface_ingestion\Plugin\TypedRepositories;

use Drupal\Component\Serialization\Json;
use Laminas\Xml2Json\Xml2Json;

trait TypedEntityRepositoryTrait {

  protected array $cupProductIdMappings = [];

  public function setBundle(string $bundle) {
    if (!$this->bundle) {
      $this->bundle = $bundle;
    }
    return $this;
  }

  /**
   * Wraps entity by id.
   *
   * @param int $id
   *   The entity id.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|null
   *   The wrapped entity or NULL if the id didn't match with any exiting
   *   entity.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function wrapById(int $id) {
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->load($id);
    return $entity ? $this->wrap($entity) : NULL;
  }

  public static function getValueFromXmlFile(string $filename, string $query, $register_xpath_ns = []) {
    return static::getSimpleXMLElement($filename, $register_xpath_ns)->xpath($query);
  }

  public static function getSimpleXMLElement(string $filename, $register_xpath_ns = []) {
    $xml = new \SimpleXMLElement($filename, 0, TRUE, 'd');
    if (!empty($register_xpath_ns['namespace'])) {
      $prefix = $register_xpath_ns['prefix'] ?? 'd';
      $xml->registerXPathNamespace($prefix, $register_xpath_ns['namespace']);
    }
    return $xml;
  }

  public static function transformXmlToArray(string $filename): array {
    if (!file_exists($filename) || !($file_content = file_get_contents($filename))) {
      return [];
    }

    $json = Xml2Json::fromXml($file_content, FALSE);
    $array = json_decode($json, 1);
    return isset($array['entity']) ? $array['entity'] : $array;
  }

  public function getStringHash(string $str) {
    $algo = 'sha512';
    return substr(hash($algo, $str), 0, 50);
  }

  public function getCupProductIdMappings(string $bundle): array {
    if (!empty($this->cupProductIdMappings[$bundle])) {
      return $this->cupProductIdMappings[$bundle];
    }
    $prefix = $this->getCupProductIdFilePrefix();
    $filename = "private://orlando-2-0-c-modelling/entities/{$prefix}_productIDs.json";
    if (!file_exists($filename)) {
      return [];
    }
    $json = file_get_contents($filename);
    $json = $json ? str_replace(['ORLANDO_', 'orlando_', 'ORLANDO:', 'orlando:'], '', $json) : '';
    $data =  $json ? Json::decode($json) : [];
    $this->cupProductIdMappings[$bundle] = array_flip(array_map('strtolower', $data));
    return $this->cupProductIdMappings[$bundle];
  }

}
