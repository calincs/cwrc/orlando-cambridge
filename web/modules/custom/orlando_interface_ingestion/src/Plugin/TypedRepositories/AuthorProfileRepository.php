<?php

namespace Drupal\orlando_interface_ingestion\Plugin\TypedRepositories;

use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for articles.
 *
 * @TypedRepository(
 *   entity_type_id = "node",
 *   bundle = "author_profile",
 *   wrappers = @ClassWithVariants(
 *     fallback = "Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile",
 *   ),
 *   description = @Translation("Repository that holds business logic applicable to all author profiles.")
 * )
 */
class AuthorProfileRepository extends TypedRepositoryBase {

  use TypedEntityRepositoryTrait;

  /**
   * The field that contains the external id of the entity.
   */
  const FIELD_ID = 'field_cwrc_id';

  /**
   * Finds a taxonomy term by cwrc id.
   *
   * @param string $id
   *   The external entity id.
   *
   * @return \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile|null
   *   The author profile if found.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function findByCWRCId(string $id): ?AuthorProfile {
    $query = $this->getQuery();

    $entities = $query->condition(static::FIELD_ID, $id)
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();

    if ($entities) {
      $entities = $this->wrapMultipleById($entities);
    }
    return $entities ? reset($entities) : NULL;
  }

  public function createWrappedEntityFromObject(\stdClass $data, int $uid = 1): AuthorProfile {
    $external_id = $data->entry->entry_identifiers->id;
    $entity = $this->entityTypeManager
      ->getStorage($this->entityType->id())
      ->create([
        'type' => $this->bundle,
        'title' => $data->entry->metadata->title,
        'uid' => $uid,
        static::FIELD_ID => $external_id,
      ]);
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile $wrapped_entity */
    $wrapped_entity = $this->wrap($entity);
    $wrapped_entity->setDataFromObject($data);

    $cup_product_id_mappings = $this->getCupProductIdMappings($this->bundle);
    if (($cup_product_id = $cup_product_id_mappings[$external_id] ?? '')) {
      $wrapped_entity->setCupProductId($cup_product_id);
    }
    $wrapped_entity->save();
    return $wrapped_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function wrapMultipleById(array $items): array {
    return parent::wrapMultipleById($items);
  }

  public function getCupProductIdFilePrefix() {
    return 'profiles';
  }

}
