<?php

namespace Drupal\orlando_interface_ingestion\Plugin\BibCiteProcessor;

use Drupal\bibcite\Plugin\BibCiteProcessor\CiteprocPhp as BaseCiteprocPhp;
use Seboettg\CiteProc\CiteProc;

/**
 * Defines a style provider based on citeproc-php library.
 *
 * @BibCiteProcessor(
 *   id = "orlando-citeproc-php",
 *   label = @Translation("Orlando Citeproc PHP"),
 * )
 */
class CiteprocPhp extends BaseCiteprocPhp {

  /**
   * {@inheritdoc}
   */
  public function render($data, $csl, $lang) {
    $cite_proc = new CiteProc($csl, $lang);

    if (!$data instanceof \stdClass) {
      $data = json_decode(json_encode($data));
    }

    if (!empty($data->author) && $data->author instanceof \stdClass) {
      $data->author = (array) $data->author;
    }

    return preg_replace('/(\\n|\r)( *)/', '', $cite_proc->render([$data]));
  }

}
