<?php

namespace Drupal\orlando_interface_ingestion\Plugin\views\sort;

use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\views\sort\Date as BaseDateTime;

/**
 * Sort handler for datetime field which introduces a month day granularity.
 *
 * @ViewsSort("oii_datetime")
 */
class Date extends BaseDateTime {

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    // Adding the month day options.
    $form['granularity']['#options'] = [
      'second' => $this->t('Second'),
      'minute' => $this->t('Minute'),
      'hour'   => $this->t('Hour'),
      'day'    => $this->t('Day'),
      'month'  => $this->t('Month'),
      'month_day'  => $this->t('Month Day'),
      'year'   => $this->t('Year'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // We are only interested in the month_day granularity.
    if ($this->options['granularity'] === 'month_day') {
      $this->ensureMyTable();
      $formula = $this->getDateFormat('md');
      // Add the field.
      $this->query->addOrderBy(NULL, $formula, $this->options['order'], $this->tableAlias . '_' . $this->field . '_' . $this->options['granularity']);
      return;
    }
    parent::query();
  }

}
