<?php

namespace Drupal\orlando_interface_ingestion\Plugin\views\argument;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\node\NodeInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Drupal\views\Plugin\views\argument\StringArgument;

/**
 * Argument handler to implement string arguments based on the status field and
 * the date value.
 *
 * @ViewsArgument("profile_event_status_date")
 */
class ProfileEventStatusDateValueArgument extends StringArgument {

  public function query($group_by = FALSE) {
    $arguments = $this->view->argument;
    $wrapped_author_profile = NULL;
    $profile_argument = $arguments['profile'] ?? NULL;
    if ($profile_argument instanceof NumericArgument && !empty($profile_argument->argument)) {
      $node = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->load($profile_argument->argument);
      if ($node && $node->bundle() === 'author_profile' && $node->hasField('field_person')) {
        /** @var \Drupal\typed_entity\RepositoryManager $repository_manager */
        $repository_manager = \Drupal::service(RepositoryManager::class);
        /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile $wrapped_author_profile */
        $wrapped_author_profile = $repository_manager->wrap($node);
      }
    }
    if (!$wrapped_author_profile) {
      return;
    }

    $argument = $this->argument;
    $wrapped_person = $wrapped_author_profile->getPerson();
    if (!empty($this->options['transform_dash'])) {
      $argument = strtr($argument, '-', ' ');
    }

    if (!empty($this->options['break_phrase'])) {
      $this->unpackArgumentValue();
    }
    else {
      $this->value = [$argument];
      $this->operator = 'or';
    }

    $formula = FALSE;
    $this->ensureMyTable();
    if (empty($this->options['glossary'])) {
      $field = "$this->tableAlias.$this->realField";
    }
    else {
      $formula = TRUE;
      $field = $this->getFormula();
    }

    if (count($this->value) > 1) {
      $operator = 'IN';
      $argument = $this->value;
    }
    else {
      $operator = '=';
    }

    $placeholder = $this->placeholder();
    if ($formula) {
      if ($operator == 'IN') {
        $field .= " IN($placeholder)";
      }
      else {
        $field .= ' = ' . $placeholder;
      }
    }
    else {
      $field .= ' ' . $operator . ' ' . $placeholder;
    }
    $placeholders = [$placeholder => $argument];

    if ($lifespan = $wrapped_person->getLifespan()) {
      // Using date of birth as min.
      $min = $lifespan->value;
      // Using date of $death as max.
      $max = $lifespan->end_value ?? '';
      if (!$max) {
        // Since end value is optional then let add 100 years to dob.
        $date = new DateTimePlus($min);
        $date->format('Y-m-d');
        $interval = \DateInterval::createFromDateString('100 years');
        $date->add($interval);
        $max = $date->format('Y-m-d');
      }
      $date_field = 'date_value';
      $placeholders += [
        ":{$this->tableAlias}_{$date_field}_min" => $min,
        ":{$this->tableAlias}_{$date_field}_max" => $max,
      ];
    }

    $this->query->addWhereExpression(0, $this->expression($field), $placeholders);
  }

  /**
   * Builds the query expression.
   *
   * @param string $field
   *   The field query.
   *
   * @return string
   *   The expression.
   */
  public function expression(string $field) {
    $table = $this->tableAlias;
    $date_field = 'date_value';
    return "$field AND (DATE_FORMAT($table.$date_field, '%Y-%m-%d') BETWEEN DATE_FORMAT(:{$table}_{$date_field}_min, '%Y-%m-%d') AND DATE_FORMAT(:{$table}_{$date_field}_max, '%Y-%m-%d'))";
  }

}
