<?php

namespace Drupal\orlando_interface_ingestion\Plugin\Block;

use Drupal\Component\Plugin\Context\ContextInterface as ComponentContextInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Heading;
use Drupal\typed_entity\RepositoryManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a TableOfContent block.
 *
 * @Block(
 *   id = "oii_table_of_content",
 *   admin_label = @Translation("Author Profile Table Of Content"),
 * )
 */
class TableOfContent extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The typed entity repository manager.
   *
   * @var \Drupal\typed_entity\RepositoryManager
   */
  protected $repositoryManager;

  /**
   * The current node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $currentNode;

  const CLASS_PREFIX = 'oii-toc';

  /**
   * TableOfContent constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param $plugin_id
   *   The plugin_id for the plugin instance.
   * @param $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   * @param \Drupal\typed_entity\RepositoryManager $repository_manager
   *   The typed data repository manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $current_route_match, RepositoryManager $repository_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
    $this->repositoryManager = $repository_manager;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get(RepositoryManager::class)
    );
  }

  /**
   * @inheritDoc
   */
  public function blockAccess(AccountInterface $account) {
    $node = $this->getCurrentNode();
    $bundle = $node instanceof NodeInterface ? $node->bundle() : '';
    $access = $bundle === 'author_profile' ? AccessResult::allowed() : AccessResult::forbidden('Only works for author profiles node type');
    return $access->addCacheableDependency($node);
  }

  /**
   * @inheritDoc
   */
  public function build() {
    $node = $this->getCurrentNode();
    $build = [
      '#type' => 'container',
      '#attached' => ['library' => [
        'orlando_interface/smooth-scroll',
        'orlando_interface/toc',
      ]],
      '#attributes' => ['class' => [static::CLASS_PREFIX]],
      'highlights' => $this->getUrlRenderArray('Highlights', static::getHighlightCssIdentifier($node), [
        'tw-font-bold',
        'tw-text-red-200',
        'hover:tw-text-gray-500',
      ]),
    ];

    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile $author_profile */
    $author_profile = $this->repositoryManager->wrap($node);
    $items = $author_profile->getHeadingParagraphs();

    foreach ($items as $index => $item) {
      /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Heading $heading */
      $heading = $this->repositoryManager->wrap($item->entity);
      $key = str_replace('-', '_', $heading->getExternalId());
      $level = $heading->getLevel();
      if ($level !== 'h2') {
        continue;
      }

      $section_heading = $key;
      $build[$section_heading] = $this->buildLevel($heading, $items, $index);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'route';
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();
    $node = $this->getCurrentNode();
    $tags[] = 'node:' . $node->id();
    return $tags;
  }

  public static function getHighlightCssIdentifier(NodeInterface $node): string {
    return Html::cleanCssIdentifier($node->bundle()) . '--highlights';
  }

  private function getUrlRenderArray(string $title, string $fragment, array $classes) {
    $classes = array_merge(['tw-block'], $classes);
    return [
      '#type' => 'link',
      '#title' => $this->t('@title', ['@title' => $title]),
      '#url' => Url::fromRoute('<none>', [], [
        'fragment' => $fragment,
      ]),
      '#attributes' => ['class' => $classes],
    ];
  }

  private function buildLevel(Heading $heading, $items, int $start) {
    $level = $heading->getLevel();
    $id = Html::cleanCssIdentifier($heading->getExternalId());
    $length = $this->getLevelLength($level, $items, $start + 1);
    $children = array_slice($items, $start + 1, $length);
    $is_h2 = $level === 'h2';
    $classes = $is_h2 ? 'tw-font-bold hover:tw-text-red-200' : 'hover:tw-text-red-200';
    $wrapper_mt = $is_h2 ? 'tw-mt-4' : 'tw-mt-2.5';
    $children_id = static::CLASS_PREFIX . '--' . $id . '--' . $level . '--children';
    $prefix = static::CLASS_PREFIX . '--' . $level;
    $build = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [$prefix . '--wrapper', $wrapper_mt],
      ],
      'link' => $this->getUrlRenderArray($heading->getText(), $id, [$classes]),
    ];

    if ($level === 'h2' && $children) {
      $build['#attributes']['class'][] = 'tw-relative';
      $build['button'] = [
        '#theme' => 'oi_toc_button',
        '#value' => $this->t('Expand'),
        '#attributes' => new Attribute([
          'class' => [
            static::CLASS_PREFIX . '--button',
            'tw-block',
            'tw-p-2',
            'tw-absolute',
            'tw-top-0',
            'tw--right-7.5',
          ],
          'aria-controls' => $children_id,
        ]),
      ];
    }

    if ($children) {
      $build['children'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [$prefix . '--children'],
          'id' => $children_id,
          'aria-expanded' => 'true',
        ],
      ];
      if ($level === 'h3') {
        $build['children']['#attributes']['class'][] = 'tw-pl-5';
      }

      foreach ($children as $index => $child) {
        /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Heading $head */
        $head = $this->repositoryManager->wrap($child->entity);
        $key = str_replace('-', '_', $head->getExternalId());
        $id = Html::cleanCssIdentifier($head->getExternalId());
        if ($head->getLevel() === 'h3') {
          $build['children'][$key] = $this->buildLevel($head, $children, $index);
        }
        elseif ($level === 'h3') {
          $build['children'][$key] = $this->getUrlRenderArray($head->getText(), $id, ['hover:tw-text-red-200', 'tw-mt-1']);
        }
      }
    }

    return $build;
  }

  private function getLevelLength(string $level, $items, int $start) {
    $children = array_slice($items, $start);
    $length = 0;
    foreach ($children as $child) {
      /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs\Heading $heading */
      $heading = $this->repositoryManager->wrap($child->entity);
      if ($level === $heading->getLevel()) {
        break;
      }
      $length++;
    }
    return $length;
  }

  /**
   * Gets the current node.
   *
   * @return \Drupal\node\NodeInterface|null
   *   The current node or null.
   */
  protected function getCurrentNode() {
    if (empty($this->currentNode)) {
      $this->currentNode = $this->currentRouteMatch->getParameter('node');
    }
    return $this->currentNode;
  }

}
