<?php

namespace Drupal\orlando_interface_ingestion\EventSubscriber;

use Drupal\cambridge_core_api\Event\StatisticsApiEvents;
use Drupal\cambridge_core_api\Event\StatisticsApiMetadataCollectorEvent;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CambridgeCoreApiStatisticsSubscriber implements EventSubscriberInterface {

  const AUTHOR_PROFILE_ENTITY_PAGES_EVENT_CODE = 'RV-DI';
  const AUTHOR_PROFILE_SUB_PAGE_EVENT_CODE = 'RV-DI';

  protected EntityTypeManagerInterface $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      StatisticsApiEvents::METADATA_COLLECTOR => ['onMetadataCollection'],
    ];
  }

  public function onMetadataCollection(StatisticsApiMetadataCollectorEvent $event) {
    $route_match = $event->getRouteMatch();
    $route_name = $route_match->getRouteName();
    if (strpos($route_name, '.canonical') !== FALSE) {
      $this->setCanonicalPagesMetadataProperties($route_match, $event);
      return;
    }

    $author_tabs_route_names = [
      'view.bibliographies.page_author_profile',
      'view.connections.page_author_connections',
      'view.events.page_timeline',
    ];
    if (!in_array($route_name, $author_tabs_route_names)) {
      return;
    }
    $node = $this->getEntityFromRouteMatchParameter($route_match, 'node');
    if (!($node instanceof NodeInterface) || $node->bundle() !== 'author_profile') {
      return;
    }

    $event->setProperty('event_code', static::AUTHOR_PROFILE_SUB_PAGE_EVENT_CODE);
    $event->setProperty('product_id', $node->field_stats_cup_product_id->value ?? '');
  }

  protected function setCanonicalPagesMetadataProperties(RouteMatchInterface $route_match, StatisticsApiMetadataCollectorEvent $event) {
    $supported_entity_configs = [
      'node' => [
        'bundles' => ['author_profile'],
        'field_id' => 'field_stats_cup_product_id',
      ],
      'profile_event' => [
        'bundles' => ['profile_event'],
        'field_id' => 'field_stats_cup_product_id',
      ],
      'taxonomy_term' => [
        'bundles' => ['person', 'organization'],
        'field_id' => 'field_stats_cup_product_id',
      ],
      'bibcite_reference' => [
        'bundles' => ['_all_'],
        'field_id' => 'stats_cup_product_id',
      ],
    ];
    $route_name = $route_match->getRouteName();
    $supported_entities = array_keys($supported_entity_configs);
    [, $entity_type_id, ] = explode('.', $route_name);

    // Ensure that we are dealing with a supported entity.
    if (!$entity_type_id || !in_array($entity_type_id, $supported_entities)) {
      return;
    }
    // Get the entity or its id from the parameter.
    if (!($entity = $this->getEntityFromRouteMatchParameter($route_match, $entity_type_id))) {
      return;
    }

    // Checking bundles.
    $bundle = $entity->bundle();
    $supported_bundles = $supported_entity_configs[$entity_type_id]['bundles'];
    if (
      ($entity_type_id !== 'bibcite_reference' && !in_array($bundle, $supported_bundles)) ||
      !($product_id = $entity->get($supported_entity_configs[$entity_type_id]['field_id'])->value ?? '')
    ) {
      return;
    }

    $event->setProperty('event_code', static::AUTHOR_PROFILE_ENTITY_PAGES_EVENT_CODE);
    $event->setProperty('product_id', $product_id);
  }

  protected function getEntityFromRouteMatchParameter(RouteMatchInterface $route_match, $entity_type_id): ?ContentEntityInterface {
    $entity = $route_match->getParameter($entity_type_id);
    if (!($entity instanceof ContentEntityInterface) && !is_numeric($entity)) {
      return NULL;
    }

    if (is_numeric($entity)) {
      try {
        $storage = $this->entityTypeManager->getStorage($entity_type_id);
        $entity = $storage->load($entity);
      }
      catch (InvalidPluginDefinitionException $e) {
        return NULL;
      }
      catch (PluginNotFoundException $e) {
        return NULL;
      }
    }
    return $entity;
  }

}
