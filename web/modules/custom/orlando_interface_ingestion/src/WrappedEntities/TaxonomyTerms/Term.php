<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms;

use Drupal\Component\Utility\Xss;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

/**
 * The wrapped entity for orlando related taxonomy terms.
 */
abstract class Term extends WrappedEntity {

  /**
   * The field that contains the external changed date of the entity.
   */
  const FIELD_CHANGED = 'field_cwrc_record_changed_date';

  /**
   * The field that store the name variants.
   */
  const FIELD_NAME_VARIANTS = 'field_name_variants';
  const FIELD_PREFERRED_NAME = 'field_preferred_name';

  /**
   * The date format to use when comparing two dates.
   */
  const DATE_FORMAT = 'Y m d';

  public function getChangedDate(): string {
    $time = $this->getEntity()->{static::FIELD_CHANGED}->value;
    return $this->getDateFromTime($time, static::DATE_FORMAT);
  }

  public function setChangedDate(string $formatted_date): Term {
    $time = strtotime($formatted_date);
    $this->getEntity()->set(static::FIELD_CHANGED, $time);
    return $this;
  }

  public function getNameVariants() {
    return $this->getEntity()->get(static::FIELD_NAME_VARIANTS);
  }

  public function setNameVariants(array $variants): Term {
    if ($variants) {
      $this->getEntity()->set(static::FIELD_NAME_VARIANTS, $variants);
    }
    return $this;
  }

  public function setNameVariantsFromItemArray(array $item_array): Term {
    $data = [];
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = $this->getEntity();
    $variants = $item_array[$term->bundle()]['identity']['variantForms']['variant'] ?? [];
    $types = static::validNameVariantTypes();
    $has_field = $term->hasField('field_orlando_standard_name');
    $standard_name_set = FALSE;
    if (!is_numeric(array_key_first($variants))) {
      $variants = [$variants];
    }

    foreach ($variants as $variant) {
      if (is_array($variant['namePart'])) {
        $name = $variant['namePart']['@text'] ?? '';
      }
      else {
        $name = $variant['namePart'] ?? '';
      }
      $name = $name ? Xss::filter(strip_tags($name)) : '';
      $type = $variant['variantType'] ?? '';
      $type = $type ? strip_tags($variant['variantType']) : '';
      if (!$name) {
        continue;
      }

      if (!in_array($type, $types)) {
        $type = 'unknown';
      }

      // Setting the orlando standard name field separately since it's also used
      // to extract alphabetical index.
      if ($type === 'orlandoStandardName' && $has_field && !$standard_name_set) {
        $this->getEntity()->set('field_orlando_standard_name', $name);
        $standard_name_set = TRUE;
      }
      $data[] = ['first' => $name, 'second' => $type];
    }

    return $this->setNameVariants($data);
  }

  abstract public function setDataFromXml(string $filename, array $item_array);

  /**
   * Updates term based on it's xml file.
   *
   * @return bool|int
   *   If the record update failed, returns FALSE. If it succeeded,
   *   returns SAVED_UPDATED (2).
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateUsingXmlFile() {
    $is_new = $this->getEntity()->isNew();
    $filename = !$is_new ? $this->getXmlFilename() : '';
    if (is_file($filename)) {
      $item_array = TaxonomyTermRepository::transformXmlToArray($filename);
      $this->setDataFromXml($filename, $item_array);
      return $this->save();
    }
    return FALSE;
  }

  /**
   * Checks if the stored changed date is different.
   *
   * @param int $time
   *   The date to use for comparison.
   *
   * @return bool
   *   TRUE if the date has changed, FALSE otherwise.
   */
  public function hasChanged(int $time): bool {
    $current_time = $this->getEntity()->{static::FIELD_CHANGED}->value;
    return $this->getDateFromTime($current_time, static::DATE_FORMAT) !== $this->getDateFromTime($time, static::DATE_FORMAT);
  }

  public static function validNameVariantTypes(): array {
    return [
      'birthName',
      'indexedName',
      'marriedName',
      'nickname',
      'orlandoStandardName',
      'pseudonym',
      'religiousName',
      'selfConstructedName',
      'styledName',
      'titledName',
      'usedForm',
      'royalName',
    ];
  }

  private function getXmlFilename() {
    $external_id = $this->getExternalId();
    $bundle = $this->getEntity()->bundle();
    $prefix = 'private://orlando-2-0-c-modelling/entities';
    $dir = $prefix . '/' . $bundle . 's';
    return $dir . '/orlando_' . $external_id . '.xml';
  }

}
