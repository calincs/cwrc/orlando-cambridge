<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms;

use Drupal\Component\Utility\Xss;
use Drupal\name\Plugin\Field\FieldType\NameItem;
use Drupal\orlando_interface_ingestion\DateUtilitiesTrait;
use Drupal\taxonomy\TermInterface;
use Drupal\typed_entity\TypedEntityContext;

/**
 * The wrapped entity for the person taxonomy vocabulary.
 */
final class Person extends Term {

  use DateUtilitiesTrait;

  const FIELD_LIFESPAN = 'field_lifespan';

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->getRepository()->getPersonFormattedPrefferedName($this);
  }

  public function getPreferredName(): ?NameItem {
    return parent::getFieldValue(static::FIELD_PREFERRED_NAME)->first();
  }

  public function getLifespan() {
    $field = parent::getFieldValue(static::FIELD_LIFESPAN);
    return !$field->isEmpty() ? $field : NULL;
  }

  public function setLifespan(array $date_ranges): Person {
    if ($date_ranges) {
      parent::setFieldValue(static::FIELD_LIFESPAN, $date_ranges);
    }
    return $this;
  }

  public function setLifespanFromItemArray(array $item_array): Person {
    $date_single = $item_array['person']['description']['existDates']['dateSingle'] ?? [];
    $date_range = [];
    if ($date_single && !is_numeric(array_key_first($date_single))) {
      $date_single = [$date_single];
    }

    foreach ($date_single as $date_item) {
      if (
        empty($date_item['dateType']) ||
        empty($date_item['standardDate']) ||
        ($date_item['dateType'] !== 'birth' && $date_item['dateType'] !== 'death')
      ) {
        continue;
      }
      $date_range_property = $date_item['dateType'] === 'birth' ? 'value' : 'end_value';
      $date_range[$date_range_property] = $this->extractDateParts($date_item['standardDate']);
    }
    return $this->setLifespan($date_range);
  }

  public function setPreferredNameFromItemArray(array $item_array): Person {
    $name_parts = [];
    $name_part_data = $item_array['person']['identity']['preferredForm']['namePart'] ?? [];
    if (is_array($name_part_data) && is_numeric(array_key_first($name_part_data))) {
      foreach ($name_part_data as $name_part_datum) {
        $part_type = $name_part_datum['@attributes']['partType'] ?? '';
        if (
          ($part_type !== 'given' && $part_type !== 'family') ||
          empty($name_part_datum['@text'])
        ) {
          continue;
        }
        $name_parts[$part_type] = trim(Xss::filter(strip_tags($name_part_datum['@text'])));
      }
    }
    elseif (is_array($name_part_data) && !is_numeric(array_key_first($name_part_data)) && !empty($name_part_data['@text'])) {
      $part_type = $name_part_data['@attributes']['partType'] ?? '';
      if ($part_type === 'fullNameInvertedOrder') {
        [$given, $family] = array_map('trim', explode(',', $name_part_data['@text'], 2)) + [
          0 => '',
          1 => '',
        ];
      }
      else {
        [$family, $given] = array_map('trim', explode(',', $name_part_data['@text'], 2)) + [
          0 => '',
          1 => '',
        ];
      }
      $name_parts['given'] = $given;
      $name_parts['family'] = $family;
    }
    elseif (is_string($name_part_data)) {
      [$family, $given] = array_map('trim', explode(',', $name_part_data, 2)) + [
        0 => '',
        1 => '',
      ];

      // When only one is provided ensure that it's set to family name instead.
      if ($given && !$family) {
        $family = $given;
        $given = '';
      }
      $name_parts['given'] = $given;
      $name_parts['family'] = $family;
    }
    return parent::setFieldValue(static::FIELD_PREFERRED_NAME, $name_parts);
  }

  public function setDataFromXml(string $filename, array $item_array): Person {
    $this->setLifespanFromItemArray($item_array);
    $this->setPreferredNameFromItemArray($item_array);
    $this->setNameVariantsFromItemArray($item_array);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {
    $entity = $context->offsetGet('entity');
    if (!($entity instanceof TermInterface)) {
      return FALSE;
    }

    return $entity->bundle() === 'person';
  }

}
