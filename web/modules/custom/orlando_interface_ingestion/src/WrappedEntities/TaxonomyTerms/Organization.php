<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms;

use Drupal\Component\Utility\Xss;
use Drupal\taxonomy\TermInterface;
use Drupal\typed_entity\TypedEntityContext;

final class Organization extends Term {

  const FIELD_PREFERRED_NAME = 'name';

  public function getPreferredName(): string {
    return parent::getFieldValue(static::FIELD_PREFERRED_NAME)->value;
  }

  public function setPreferredNameFromItemArray(array $item_array): Organization {
    $name_data = $item_array['organization']['identity']['preferredForm']['namePart'] ?? '';
    $name = '';
    if (is_array($name_data) && !empty($name_data['@text'])) {
      $name = $name_data['@text'];
    }
    elseif (is_string($name_data)) {
      $name = $name_data;
    }
    $name = $name ? Xss::filter(strip_tags($name)) : '';
    return parent::setFieldValue(static::FIELD_PREFERRED_NAME, $name);
  }

  public function setDataFromXml(string $filename, array $item_array): Organization {
    return $this->setPreferredNameFromItemArray($item_array);
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {
    $entity = $context->offsetGet('entity');
    if (!($entity instanceof TermInterface)) {
      return FALSE;
    }

    return $entity->bundle() === 'organization';
  }

}
