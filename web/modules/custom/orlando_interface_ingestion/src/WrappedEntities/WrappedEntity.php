<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\FileInterface;
use Drupal\orlando_interface_ingestion\Event\BasicBodySetEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryInterface;
use Drupal\typed_entity\WrappedEntities\WrappedEntityBase;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class WrappedEntity extends WrappedEntityBase {

  const SCHEMA_DATE_FORMAT = 'Y-m-d';
  const YEAR_ONLY_FORMAT = 'Y';
  const YEAR_MONTH_FORMAT = 'Y-m';

  const CUP_PRODUCT_ID_FIELD = 'field_stats_cup_product_id';

  const CWRC_ID_FIELD = 'field_cwrc_id';

  protected ?TypedRepositoryInterface $repository;

  public function getFieldValue(string $field_name): FieldItemListInterface {
    return $this->getEntity()->get($field_name);
  }

  public function setFieldValue(string $field_name, $value) {
    if ($value) {
      $this->getEntity()->set($field_name, $value);
    }
    return $this;
  }

  /**
   * @return bool|int
   *   If the record insert or update failed, returns FALSE. If it succeeded,
   *   returns SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save() {
    return $this->getEntity()->save();
  }

  public function getCupProductId(): string {
    return $this->getFieldValue(static::CUP_PRODUCT_ID_FIELD)->value ?? '';
  }

  public function setCupProductId(string $id) {
    if (!$this->getEntity()->hasField(static::CUP_PRODUCT_ID_FIELD)) {
      return $this;
    }
    return $this->setFieldValue(static::CUP_PRODUCT_ID_FIELD, $id);
  }

  public function getExternalId(): string {
    return $this->getFieldValue(static::CWRC_ID_FIELD)->value;
  }

  public function setExternalId(string $id): WrappedEntity {
    return $this->setFieldValue(static::CWRC_ID_FIELD, $id);
  }

  protected function getRepository() {
    if (empty($this->repository)) {
      $this->repository = $this->repositoryManager()->repositoryFromEntity($this->getEntity());
    }
    return $this->repository;
  }

  protected function getDateFromTime(int $time, string $format): string {
    $date = DateTimePlus::createFromTimestamp($time);
    // Ensure that this date has the default time.
    $date->setDefaultDateTime();
    return $date->format($format);
  }

  protected function createFileEntity(string $file_uri): FileInterface {
    if (!is_file($file_uri)) {
      throw new FileNotFoundException(sprintf('File "%s" could not be found or is not valid!', $file_uri));
    }
    $info = pathinfo($file_uri);
    /** @var \Drupal\file\FileStorageInterface $file_storage */
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    $files = $file_storage->loadByProperties(['uri' => $file_uri]);

    if ($files) {
      /** @var \Drupal\file\FileInterface $file */
      $file = reset($files);
    }
    else {
      $file = $file_storage->create([
        'uri' => $file_uri,
        'filename' => $info['basename'],
        'statue' => FileInterface::STATUS_PERMANENT,
      ]);
      $file->save();
    }
    return $file;
  }

  protected function setParagraphItemsParent(FieldItemListInterface $paragraph_items, EntityInterface $connection_source, $parent_field_name) {
    foreach ($paragraph_items as $item) {
      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      $paragraph = $item->entity;

      // Ensure that the parent on the paragraph is set.
      if ($paragraph && !$paragraph->getParentEntity()) {
        $paragraph->setParentEntity($this->getEntity(), $parent_field_name);
        $paragraph->save();
        $paragraph = NULL;
      }

      // Dispatch an event to process the body field.
      $processed = $paragraph && $paragraph->bundle() === 'basic' && (bool) ($paragraph->get('field_body_processed')->value ?? FALSE);
      if (!$processed && $paragraph->hasField('field_body') && !$paragraph->get('field_body')->isEmpty()) {
        /** @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher */
        $event_dispatcher = \Drupal::service('event_dispatcher');
        $body = $paragraph->get('field_body')->value;

        $event = new BasicBodySetEvent($body, $connection_source, $paragraph);
        $event_dispatcher->dispatch($event, IngestionEvents::BEFORE_BODY_SET);

        $extracted_connection_ids = $event->getExtractedConnections();
        if ($extracted_connection_ids) {
          $targets = [];
          foreach ($extracted_connection_ids as $connection_id) {
            $targets[] = ['target_id' => $connection_id];
          }
          $paragraph->set('field_connections', $targets);
        }

        $paragraph->set('field_body', [
          'value' => $event->getBody(),
          'format' => 'full_html',
        ]);
        $paragraph->set('field_body_processed', TRUE);
        $paragraph->save();
        $paragraph = NULL;
      }
    }
  }

}
