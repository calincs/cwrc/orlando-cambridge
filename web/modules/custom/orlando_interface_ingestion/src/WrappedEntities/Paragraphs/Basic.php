<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\typed_entity\TypedEntityContext;

final class Basic extends BaseParagraph {

  public function getBody() {
    return $this->getFieldValue('field_body');
  }

  public function setBody(string $body) {
    $data = [
      'value' => $body,
      'format' => 'full_html',
    ];
    return $this->setFieldValue('field_body', $data);
  }

  public function setDataFromObject(\stdClass $object) {
    $body = '';
    if (isset($object->content) && is_string($object->content)) {
      $body = $object->content;
    }
    elseif (isset($object->chronprose_content) && is_string($object->chronprose_content)) {
      $body = $object->chronprose_content;
    }

    if ($body) {
      $this->setBody('<div class="paragraph-wrapper">' . $body . '</div>');
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {
    $entity = $context->offsetGet('entity');
    if (!($entity instanceof ParagraphInterface)) {
      return FALSE;
    }

    return $entity->bundle() === 'basic';
  }

}
