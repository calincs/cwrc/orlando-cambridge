<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\typed_entity\TypedEntityContext;

final class Heading extends BaseParagraph {

  public function getLevel(): string {
    return $this->getFieldValue('field_level')->value;
  }

  public function setLevel(string $level): Heading {
    return $this->setFieldValue('field_level', $level);
  }

  public function getText(): string {
    return $this->getFieldValue('field_text')->value;
  }

  public function setText(string $text): Heading {
    return $this->setFieldValue('field_text', $text);
  }

  public function setDataFromObject(\stdClass $object): Heading {
    $this->setLevel($object->level);
    $this->setText($object->content);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function applies(TypedEntityContext $context): bool {
    $entity = $context->offsetGet('entity');
    if (!($entity instanceof ParagraphInterface)) {
      return FALSE;
    }

    return $entity->bundle() === 'heading';
  }

}
