<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Paragraphs;

use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

abstract class BaseParagraph extends WrappedEntity {

  const CWRC_ID_FIELD = 'field_id';

  abstract public function setDataFromObject(\stdClass $object);

}
