<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Media;

use Drupal\Component\Utility\Xss;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\MediaRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;

final class Image extends WrappedEntity {

  protected $title;

  protected $mod;

  public function setImageFile(string $filename): Image {
    $info = pathinfo($filename);
    $file = $this->createFileEntity($filename);
    $mod_xml = str_replace($info['extension'], 'xml', $filename);
    $this->mod = MediaRepository::transformXmlToArray($mod_xml)['mods'];
    $abstract = '';
    if (is_array($this->mod['abstract'])) {
      foreach ($this->mod['abstract'] as $item) {
        if (is_string($item)) {
          $abstract = $item;
          break;
        }
      }
    }
    else {
      $abstract = $this->mod['abstract'];
    }
    $title = $this->processTitleInfo($this->mod['titleInfo']);
    $title = $title ? substr(Xss::filter(strip_tags($title)), 0, 1024) : $title;
    $values = [
      'target_id' => $file->id(),
      'alt' => substr(Xss::filter(strip_tags($abstract)), 0, 512),
      'title' => $title,
    ];

    parent::setFieldValue('field_media_image', $values);
    // Making sure that we removed the path scheme wrapper.
    parent::setFieldValue('field_path', str_replace('private://', '', $info['dirname']));
    $this->setImageFileModsFile($mod_xml);
    return $this;
  }

  public function setImageFileMods(array $data) {
    return $this->setFieldValue('field_image_mods', $data);
  }

  public function setNameFromXml(string $filename): Image {
    return parent::setFieldValue('name', $this->getImageTitleFromXml($filename));
  }

  private function setImageFileModsFile(string $mod_uri) {
    $mod = $this->createFileEntity($mod_uri);
    return $this->setImageFileMods(['target_id' => $mod->id()]);
  }

  private function getImageTitleFromXml(string $filename): string {
    if (empty($this->title)) {
      if (empty($this->mod)) {
        $this->mod = MediaRepository::transformXmlToArray($filename)['mods'];
      }
      $title = $this->processTitleInfo($this->mod['titleInfo']);
      $this->title = $title ? Xss::filter(strip_tags($title)) : '';
    }
    return $this->title;
  }

  private function processTitleInfo($title_info): string {
    $title = $title_info['title'] ?? '';
    if (!$title && is_array($title_info)) {
      foreach ($title_info as $info) {
        if (empty($info['@attributes']['type']) || $info['@attributes']['type'] !== 'alternative') {
          $title = $info['title'];
          break;
        }
      }
    }
    return $title;
  }

}
