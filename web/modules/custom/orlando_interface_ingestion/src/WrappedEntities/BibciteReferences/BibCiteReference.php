<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\orlando_interface_ingestion\Commands\IngestItemsCommands;
use Drupal\orlando_interface_ingestion\IsoDateParser;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\BibciteReferenceRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Person;
use Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Term;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;
use Laminas\Xml2Json\Xml2Json;
use Symfony\Component\DomCrawler\Crawler;

class BibCiteReference extends WrappedEntity {

  const ITEMS_DIR = 'private://orlando-2-0-c-modelling/entities/bibls';

  const BIBCITE_DATE_XPATH = '/*/*[local-name()="originInfo"]/*[local-name()="dateIssued" and @encoding]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="originInfo"]/*[local-name()="dateIssued" and @encoding]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="part"]/*[local-name()="date" and @encoding]';

  const CWRC_ID_FIELD = 'cwrc_id';

  const CUP_PRODUCT_ID_FIELD = 'stats_cup_product_id';

  protected $namespaceOptions = ['namespace' => 'http://www.loc.gov/mods/v3'];

  /**
   * @var \SimpleXMLElement
   */
  protected $xml;

  protected $isoDateParser;

  public function getModFileUri() {
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->getFieldValue('mod_file')->entity;
    return $file ? $file->getFileUri() : '';
  }

  public function getSimpleXMLElement(string $filename = '') {
    if ($this->xml === NULL) {
      $filename = $filename ?: $this->getModFileUri();
      $this->xml = BibciteReferenceRepository::getSimpleXMLElement($filename, $this->namespaceOptions);
    }
    return $this->xml;
  }

  public function setFieldsFromXML(string $filename = '') {
    $xml = $this->getSimpleXMLElement($filename);
    $fields_xpath_mapping = $this->getFieldsXpathMapping();
    foreach ($fields_xpath_mapping as $field_name => $query) {
      if (!empty($query['skip'])) {
        continue;
      }

      $cast_to_string = TRUE;
      $data = $xml->xpath($query['xpath']);
      $value = NULL;
      $is_publisher = !empty($query['is_publisher']);
      $target_attribute = $query['attribute'] ?? '';
      $open_ended_date_key = '';
      if (!$is_publisher && $target_attribute && (isset($data[0][$target_attribute]) || isset($data[1][$target_attribute]))) {
        $value = $data[0][$target_attribute] ?? $data[1][$target_attribute];
      }
      elseif ($is_publisher && !empty($data[0])) {
        if (isset($data[0][$target_attribute])) {
          $field_name = 'oi_publisher';
          $value = $this->processPublisher((string) $data[0][$target_attribute]);
          $cast_to_string = FALSE;
        }
        else {
          $value = $data[0][0];
        }
      }
      elseif (!empty($query['is_author'])) {
        $cast_to_string = FALSE;
        $value = $this->processAuthors($data);
      }
      elseif (!empty($query['is_title']) && !empty($data[0])) {
        $value = $this->processTitle($data[0]);
      }
      elseif (!empty($query['is_date'])) {
        $value = $data ? $this->processExtractedIsoDate($data, !empty($query['skip_extraction']), $open_ended_date_key) : '';
      }
      elseif (!empty($query['children_tags']) && !empty($data[0])) {
        $value = $this->processResultChildrenTags($data[0], $query['children_tags']);
      }
      elseif (isset($data[0][0])) {
        $value = $data[0][0];
      }
      $value = $cast_to_string && $value ? (string) $value : $value;

      if ($value) {
        $this->setFieldValue($field_name, $value);
      }
      if ($open_ended_date_key) {
        $this->setFieldValue('bibcite_custom2', $open_ended_date_key);
      }
    }
    return $this;
  }

  /**
   * Sets data from XML filename.
   *
   * @param string $filename
   *   The file name.
   *
   * @return $this
   *   The wrapped entity.
   */
  public function setDataFromXml(string $filename) {
    $this->setFieldsFromXML($filename);
    return $this;
  }

  /**
   * Updates Bibcite reference based on its XML file.
   *
   * @return bool|int
   *   If the record update failed, returns FALSE. If it succeeded,
   *   returns SAVED_UPDATED (2).
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateUsingXmlFile(bool $author_contributor_only = TRUE) {
    $reference = $this->getEntity();
    $is_new = $reference->isNew();
    if ($is_new && $author_contributor_only) {
      // No updating authors only on new bibliographic reference.
      return FALSE;
    }

    if ($author_contributor_only && $reference->get('author')->isEmpty()) {
      return FALSE;
    }

    if ($author_contributor_only) {
      $author_query = $this->getFieldsXpathMapping()['author'];
      $xml = $this->getSimpleXMLElement();
      if (!($data = $xml->xpath($author_query['xpath']))) {
        return FALSE;
      }

      $index = 0;
      $save_status = TRUE;
      foreach ($reference->get('author') as $author_item) {
        /** @var \Drupal\bibcite_entity\Entity\ContributorInterface $contributor */
        $contributor = $author_item->entity;
        $result_item = Json::decode(Xml2Json::fromXml($data[$index]->asXML(), FALSE));
        $name_parts = $this->extractNameParts($result_item);
        $name_display_form = $this->extractNameDisplayForm($result_item);
        $contributor->setFirstName($name_parts['first_name']);
        $contributor->setLastName($name_parts['last_name']);
        $contributor->setNickName($name_display_form);
        $save_status = $contributor->save();
        $index++;
      }
      return $save_status;
    }
    $filename = !$is_new ? $this->getModFileUri() : '';
    if (is_file($filename)) {
      $this->setDataFromXml($filename);
      return $this->save();
    }
    return FALSE;
  }

  /**
   * Processes the result with children tags.
   *
   * @param \SimpleXMLElement $result
   *   The result.
   * @param array $tags
   *   The children tag.
   *
   * @return string
   *   The value.
   */
  private function processResultChildrenTags(\SimpleXMLElement $result, array $tags): string {
    $children = [];
    foreach ($tags as $tag) {
      $data = $result->{$tag} ?? NULL;
      if($data instanceof \SimpleXMLElement || is_string($data)){
        $children[] = (string)$data;
      }
    }

    return $children ? implode(' - ', $children) : '';
  }

  private function processExtractedIsoDate(array $dates, bool $skip_extraction = FALSE, string &$open_ended_date_key = ''): string {
    $iso_date_parser = $this->isoDateParser();
    $dates_data = $iso_date_parser->xmlProcessor($dates, $skip_extraction);
    $open_ended_date_key = $dates_data['open_ended_date_key'] ?? '';

    $separator = $dates_data['start'] && $dates_data['end'] ? '-' : '';
    $value = $dates_data['start'];
    if ($separator) {
      $value .= $separator . $dates_data['end'];
    }
    return $value;
  }

  private function processTitle(\SimpleXMLElement $result) {
    $xml = str_replace(['tei:title', 'tei:orgname'], ['tei_title', 'tei_orgname'], $result->asXML());
    // Converts all special characters to utf-8.
    $xml = mb_convert_encoding($xml, 'HTML-ENTITIES', 'UTF-8');
    // Creating new document.
    $doc = new \DOMDocument('1.0', 'utf-8');
    // Turning off some errors.
    libxml_use_internal_errors(true);
    // Loads the content without adding enclosing html/body tags and also the
    // doctype declaration.
    $doc->loadHTML($xml, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

    $crawler = new Crawler($doc);
    $crawler->filter('extension')
      ->each(function(Crawler $extension) use ($doc) {
        $tag_name = '';
        $tei_title_level = '';
        $extension->filter('tei_title')->each(function(Crawler $title) use (&$tag_name, &$tei_title_level) {
          $tag_name = 'tei_title';
          $tei_title_level = $title->attr('level');
        });
        $extension->filter('tei_orgname')->each(function() use (&$tag_name) {
          $tag_name = 'tei_orgname';
        });
        foreach($extension as $node) {
          /** @var \DOMNode $node */
          $span = $doc->createElement('span', trim($node->textContent));
          $span->setAttribute('data-tei-ns-tag', $tag_name);
          if ($tei_title_level) {
            $span->setAttribute('data-tei-title-lvl', $tei_title_level);
          }
          $node->parentNode->replaceChild($span, $node);
        }
      });
    return trim(preg_replace('/\s+/', ' ', $crawler->html()));
  }

  private function processPublisher(string $uri) {
    $wrapped_term = $this->getWrappedTermFromUri($uri);
    $value = [];
    if ($wrapped_term) {
      $this->setFieldValue('bibcite_publisher', $wrapped_term->label());
      $value = ['target_id' => $wrapped_term->getEntity()->id()];
    }
    return $value;
  }

  private function processAuthors($results) {
    $data = [];
    foreach ($results as $index => $result) {
      $result_item = Json::decode(Xml2Json::fromXml($result->asXML(), FALSE));
      $wrapped_term = $this->extractWrappedTerm($result_item);
      $name_display_form = $this->extractNameDisplayForm($result_item);
      $name_parts = $this->extractNameParts($result_item);

      if (!$wrapped_term && !$name_parts) {
        continue;
      }

      $data[$index] = [
        'target_id' => $this->getContributorTargetId($wrapped_term, $name_display_form, $name_parts),
      ];
      $data[$index]['category'] = match($index) {
        0 => 'primary',
        1 => 'secondary',
        2 => 'tertiary',
        default => 'subsidiary',
      };
      if ($role_id = $this->extractRoleId($result_item)) {
        $data[$index]['role'] = $role_id;
      }
    }
    return $data;
  }

  private function getWrappedTermFromUri(string $uri, $vid = '') {
    $prefixes = [
      'http://',
      'https://',
      'commons.cwrc.ca/',
      'cwrc.ca/islandora/object/',
      'dev-02.cwrc.ca/islandora/object/',
      'orlando:',
    ];
    $id = str_replace($prefixes, '', $uri);
    if ($vid) {
      $publisher_dir = IngestItemsCommands::FILES_ENTITIES_DIR . '/' . $vid . 's';
      $filename = $publisher_dir . '/orlando_' . $id . '.xml';
    }
    else {
      foreach (['persons', 'organizations'] as $folder) {
        $publisher_dir = IngestItemsCommands::FILES_ENTITIES_DIR . '/' . $folder;
        $filename = $publisher_dir . '/orlando_' . $id . '.xml';
        if (is_file($filename)) {
          $vid = rtrim($folder, 's');
          break;
        }
      }
    }

    $wrapped_term = NULL;
    if ($vid) {
      /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository $term_repository */
      $term_repository = $this->repositoryManager()->repository('taxonomy_term', $vid);
      if ($term_repository && !$term_repository->getBundle()) {
        $term_repository->setBundle($vid);
      }
      $wrapped_term = $term_repository->findByCWRCId($id);
      if (!$wrapped_term) {
        $wrapped_term = $term_repository->createWrappedEntityFromXmlFile($id, $filename);
      }
    }
    return $wrapped_term;
  }

  private function getContributorTargetId(Person $person = NULL, string $name_display_form = '', $name_parts = []) {
    $contributor_storage = \Drupal::entityTypeManager()->getStorage('bibcite_contributor');
    $query = $contributor_storage->getQuery()->accessCheck(FALSE);

    $group = $query->orConditionGroup();
    $id = $person ? $person->getEntity()->id() : 0;
    $group->condition('field_person.target_id', $id);

    $and = $query->andConditionGroup();
    $and->condition('last_name', $name_parts['last_name'] ?? '');
    $and->condition('first_name', $name_parts['first_name'] ?? '');
    $group->condition($and);

    $ids = $query->condition($group)
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    if ($ids) {
      return array_key_first($ids);
    }

    $values = [];
    if ($person) {
      $values['field_person'] = ['target_id' => $id];
    }

    $values += [
      'first_name' => $name_parts['first_name'] ?? '',
      'last_name' => $name_parts['last_name'] ?? '',
    ];
    if ($name_display_form) {
      $values['nick'] = trim(Xss::filter($name_display_form));
    }
    $contributor = $contributor_storage->create($values);
    $contributor->save();
    return $contributor->id();
  }

  private function getFieldsXpathMapping() {
    return [
      'title' => [
        'xpath' => '/*[local-name()="mods"]/*[local-name()="titleInfo" and @usage="primary"]/*[local-name()="title"]|/*[local-name()="mods"]/*[local-name()="titleInfo" and not(@usage) and not(@type)]/*[local-name()="title"]',
        'is_title' => TRUE,
      ],
      'author' => [
        'xpath' => '//d:mods/d:name|//d:mods/d:relatedItem[@type="host"]/d:name',
        'is_author' => TRUE,
      ],
      'bibcite_publisher' => [
        'xpath' => '/*/*[local-name()="originInfo"]/*[local-name()="publisher"]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="originInfo"]/*[local-name()="publisher"]',
        'attribute' => 'valueURI',
        'is_publisher' => TRUE,
      ],
      'bibcite_alternate_title' => [
        'xpath' => '/*/*[local-name()="titleInfo" and @type="alternative"]/*[local-name()="title"]',
        'is_title' => TRUE,
        'skip' => TRUE,
      ],
      'bibcite_secondary_title' => [
        'xpath' => '/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="titleInfo" and @usage="primary"]/*[local-name()="title"]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="titleInfo"]/*[local-name()="title"]',
        'is_title' => TRUE,
      ],
      'bibcite_tertiary_title' => [
        'xpath' => '/*/*[local-name()="titleInfo" and @type="uniform" and @usage="primary"]/*[local-name()="title"]|/*/*[local-name()="titleInfo" and @type="uniform"]/*[local-name()="title"]|/*/*[local-name()="titleInfo" and not(@usage) and not(@type)]/*[local-name()="title"]',
        'is_title' => TRUE,
        'skip' => TRUE,
      ],
      'bibcite_custom1' => [
        'xpath' => '/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="titleInfo" and @type="alternative"]/*[local-name()="title"]',
        'is_title' => TRUE,
      ],
      'bibcite_number_of_volumes' => [
        'xpath' => '/*/*[local-name()="physicalDescription"]/*[local-name()="extent"]',
      ],
      'bibcite_place_published' => [
        'xpath' => '/*/*[local-name()="originInfo"]/*[local-name()="place"]/*[local-name()="placeTerm" and @type="text"]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="originInfo"]/*[local-name()="place"]/*[local-name()="placeTerm" and @type="text"]',
      ],
      'bibcite_access_date' => [
        'xpath' => '/*/*[local-name()="location"]/*[local-name()="url" and @dateLastAccessed !=""]/@dateLastAccessed|/*/*[local-name()="originInfo"]/*[local-name()="dateOther" and @type="dateAccessed" and @encoding="iso8601"]/text()',
        'attribute' => 'dateLastAccessed',
      ],
      'bibcite_url' => [
        'xpath' => '/*/*[local-name()="location"]/*[local-name()="url"]/text()',
      ],
      'bibcite_notes' => [
        'xpath' => '/*/*[local-name()="note" and @type="public_note"]',
      ],
      'bibcite_lang' => [
        'xpath' => '/*/*[local-name()="language"]/*[local-name()="languageTerm" and @type="text"]',
      ],
      'bibcite_date' => [
        'xpath' => static::BIBCITE_DATE_XPATH,
        'is_date' => TRUE,
        'skip_extraction' => TRUE,
      ],
      'bibcite_volume' => [
        'xpath' => '/*/*[local-name()="part"]/*[local-name()="detail" and @type="volume"]/*[local-name()="number"]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="part"]/*[local-name()="detail" and @type="volume"]/*[local-name()="number"]',
      ],
      'bibcite_pages' => [
        'xpath' => '/*/*[local-name()="part"]/*[local-name()="extent" and contains(@unit, "page")]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="part"]/*[local-name()="extent" and contains(@unit, "page")]',
        'children_tags' => ['start', 'end', 'list'],
      ],
      'bibcite_edition' => [
        'xpath' => '/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="originInfo"]/*[local-name()="edition"]|/*/*[local-name()="originInfo"]/*[local-name()="edition"]',
      ],
      'bibcite_original_publication' => [
        'xpath' => '/*/*[local-name()="originInfo"]/*[local-name()="dateOther" and @type="original"]',
        'is_date' => TRUE,
        'attribute_name' => 'point',
      ],
      'bibcite_number' => [
        'xpath' => '/*/*[local-name()="part"]/*[local-name()="detail" and @type="issue"]/*[local-name()="number"]|/*/*[local-name()="relatedItem" and @type="host"]/*[local-name()="part"]/*[local-name()="detail" and @type="issue"]/*[local-name()="number"]',
      ],
    ];
  }

  private function isoDateParser(): IsoDateParser {
    if (!$this->isoDateParser) {
      $this->isoDateParser = \Drupal::service('orlando_interface_ingestion.iso_date_parser');
    }
    return $this->isoDateParser;
  }

  private function extractNameParts(array $result_item): array {
    $data = $result_item['name']['namePart'] ?? [];
    $name_parts = [
      'first_name' => '',
      'last_name' => '',
    ];

    if (is_array($data)) {
      foreach ($data as $index => $datum) {
        $name_type = $datum['@attributes']['type'] ?? '';
        $name_type = $name_type ?? 'family';
        if ($name_type === 'family' && !$name_parts['last_name']) {
          $name_parts['last_name'] = trim($datum['@text'] ?? '');
        }
        else {
          $name_parts['first_name'] = trim($datum['@text'] ?? '');
        }
      }
    }
    elseif (is_string($data)) {
      [$family, $given] = array_map('trim', explode(',', $data, 2)) + [
        0 => '',
        1 => '',
      ];

      // When only one is provided ensure that it's set to family name instead.
      if ($given && !$family) {
        $family = $given;
        $given = '';
      }
      $name_parts['first_name'] = $given;
      $name_parts['last_name'] = $family;
    }
    return $name_parts;
  }

  private function extractRoleId(array $result_item): string {
    $role_id = $result_item['name']['role']['roleTerm']['@text'] ?? '';
    $role_id = is_string($role_id) ? strtolower($role_id) : $role_id;
    // When the role tag is not present, then we are dealing with an author.
    if (!$role_id) {
      return 'author';
    }

    // @todo check if we should be getting this value from the drupal stored
    // configurations.
    $expected_configured_roles = [
      'author',
      'editor',
      'illustrator',
      'transcriber',
      'compiler',
      'adaptor',
      'recipient',
      'contributor',
      'translator',
    ];
    if (in_array($role_id, $expected_configured_roles, TRUE)) {
      return $role_id;
    }
    if ($role_id === 'rcp' || (is_array($role_id) && in_array('rcp', $role_id))) {
      return 'recipient';
    }
    if ($role_id === 'edt' || (is_array($role_id) && in_array('edt', $role_id))) {
      return 'editor';
    }
    if ($role_id === 'ctb' || (is_array($role_id) && in_array('ctb', $role_id))) {
      return 'contributor';
    }
    return '';
  }

  private function extractWrappedTerm(array $result_item): ?Term {
    if (!($cwrc_id = $result_item['name']['@attributes']['valueURI'] ?? '')) {
      return NULL;
    }
    return $this->getWrappedTermFromUri($cwrc_id, 'person');
  }

  private function extractNameDisplayForm(array $result_item): string {
    return $result_item['name']['displayForm'] ?? '';
  }

}
