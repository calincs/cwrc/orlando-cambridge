<?php

namespace Drupal\orlando_interface_ingestion\WrappedEntities\Nodes;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\file\FileInterface;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\ParagraphRepository;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Person;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;
use Drupal\paragraphs\ParagraphInterface;

final class AuthorProfile extends WrappedEntity {

  const ENTRIES_DIR = 'private://orlando-2-0-c-modelling/entities/entries';

  public function getPerson(): Person {
    $term = $this->getFieldValue('field_person')->entity;
    return $this->repositoryManager()->wrap($term);
  }

  public function setPerson(array $data): AuthorProfile {
    return $this->setFieldValue('field_person', $data);
  }

  public function setPersonFromExternalUri(string $uri): AuthorProfile {
    $person_id = static::cleanEntryIdentifierEntityUri($uri);
    $vid = 'person';

    /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository $term_repository */
    $term_repository = $this->repositoryManager()->repository('taxonomy_term', $vid);
    if ($term_repository && !$term_repository->getBundle()) {
      $term_repository->setBundle($vid);
    }
    assert($term_repository instanceof TaxonomyTermRepository);
    // Getting the connection filename so that we can use it later if needed.
    $filename_id = $person_id;
    $person_id = str_replace(['orlando:', 'orlando_', '.xml'], '', $person_id);
    $wrapped_term_entity = $term_repository->findByCWRCId($person_id);
    if (!$wrapped_term_entity) {
      $connections_dir = 'private://orlando-2-0-c-modelling/entities/persons';
      $filename = "$connections_dir/$filename_id.xml";
      // Creating the taxonomy term.
      $wrapped_term_entity = $term_repository->createWrappedEntityFromXmlFile($person_id, $filename);
    }
    return $this->setPerson(['target_id' => $wrapped_term_entity->getEntity()->id()]);
  }

  public function getSummary() {
    return $this->getFieldValue('field_summary');
  }

  public function setSummary(array $data) {
    return $this->setFieldValue('field_summary', $data);
  }

  public function getNameDiscussion() {
    return $this->getFieldValue('field_name_discussion');
  }

  public function setNameDiscussion(array $data) {
    return $this->setFieldValue('field_name_discussion', $data);
  }

  public function setSummaryFromObject(\stdClass $object): AuthorProfile {
    $paragraph = $this->setBasicParagraphFieldFromObject($object);
    return $this->setSummary([
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ]);
  }

  public function setNameDiscussionFromObject(\stdClass $object): AuthorProfile {
    $paragraph = $this->setBasicParagraphFieldFromObject($object);
    return $this->setNameDiscussion([
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ]);
  }

  public function getXmlEntryFile(): FileInterface {
    return $this->getFieldValue('field_xml_entry')->entity;
  }

  public function setXmlEntry(array $data) {
    return $this->setFieldValue('field_xml_entry', $data);
  }

  public function setXmlEntryFile(string $entry_id) {
    $filename =  $entry_id . '.xml';
    $file_uri = static::getAuthorProfileXmlUri($entry_id) . '/' . $filename;
    $file = $this->createFileEntity($file_uri);
    return $this->setXmlEntry(['target_id' => $file->id()]);
  }

  public function getParagraphs() {
    return $this->getFieldValue('field_paragraphs');
  }

  public function getHeadingParagraphs() {
    $headings = $this->getParagraphs()->filter(function(TypedDataInterface $item) {
      return $item->entity->bundle() === 'heading';
    });
    return iterator_to_array($headings);
  }

  public function setParagraphsFromSectionObject(\stdClass $section) {
    $paragraphs = $this->getFieldValue('field_paragraphs');
    $section->section_heading->level = 'h2';
    $paragraphs = $this->addToParagraphsFromObject($section->section_heading, $paragraphs, 'heading');
    $paragraphs = $this->addContentItemsToParagraphs($section->section_content, $paragraphs, 3, FALSE);
    $this->getEntity()->field_paragraphs = $paragraphs;
    return $this;
  }

  public function setDataFromObject(\stdClass $object): AuthorProfile {
    $external_id = $object->entry->entry_identifiers->id;
    $entry = $object->entry;
    $this->setPersonFromExternalUri($entry->entry_identifiers->entity_uri);
    $this->setXmlEntryFile($external_id);

    if (isset($entry->authorsummary->content[0]->paragraph)) {
      $this->setSummaryFromObject($entry->authorsummary->content[0]->paragraph);
    }
    if (isset($entry->name_discussion->content[0]->content[0]->paragraph)) {
      $this->setNameDiscussionFromObject($entry->name_discussion->content[0]->content[0]->paragraph);
    }

    // Set sections.
    foreach (['biography', 'writing'] as $section_id) {
      $section = $entry->{$section_id} ?? NULL;
      if ($section && is_array($entry->{$section_id}->section_content)) {
        // Instantiate section heading if not set.
        if (empty($section->section_heading) || $section->section_heading === 'null') {
          $heading_content = ucfirst($section_id);
          $section->section_heading = (object) [
            'id' => $external_id . '_section_' . $heading_content,
            'content' => $heading_content,
          ];
        }
        $this->setParagraphsFromSectionObject($section);
      }
    }

    $this->ingestMediaImages();

    return $this;
  }

  public function setParagraphsParent() {
    $connection_source = $this->getPerson()->getEntity();
    // Summary field.
    $this->setParagraphItemsParent($this->getSummary(), $connection_source, 'field_summary');
    // Name Discussion.
    $this->setParagraphItemsParent($this->getNameDiscussion(), $connection_source, 'field_name_discussion');
    // Main paragraph field.
    $this->setParagraphItemsParent($this->getParagraphs(), $connection_source, 'field_paragraphs');
  }

  public function getImages() {
    return $this->getFieldValue('field_images');
  }

  public function setImages(array $values): AuthorProfile {
    return $this->setFieldValue('field_images', $values);
  }

  public function ingestMediaImages(): AuthorProfile {
    $values = [];
    /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\MediaRepository $repository */
    $repository = $this->repositoryManager()->repository('media', 'image');
    if ($repository && !$repository->getBundle()) {
      $repository->setBundle('image');
    }
    $file_system = \Drupal::service('file_system');
    $entry_dir = static::getAuthorProfileXmlUri($this->getExternalId());
    // Realpath is needed cause glob doesn't work with system wrapper
    // stream.
    $entry_dir_realpath = $file_system->realpath($entry_dir);
    $pattern = $entry_dir_realpath . '/images/*.{jpg,png,jpeg}';
    $images = glob($pattern, GLOB_BRACE);
    foreach ($images as $image_realpath) {
      $info = pathinfo($image_realpath);
      $mod = str_replace($info['extension'], 'xml', $image_realpath);
      if (is_file($mod)) {
        // Using system wrapper stream instead of the realpath coming from glob.
        $filename = $entry_dir . '/images/' . $info['basename'];
        $wrapped_media = $repository->findMediaByFilename($filename);
        if (!$wrapped_media) {
          $wrapped_media = $repository->createMediaImageFromData($filename);
        }
        $values[] = ['target_id' => $wrapped_media->getEntity()->id()];
      }
    }

    if ($values) {
      $this->setImages($values);
    }
    return $this;
  }

  public static function getAuthorProfileXmlUri(string $entry_id): string {
    return static::ENTRIES_DIR . '/' . $entry_id;
  }

  public static function cleanEntryIdentifierEntityUri(string $uri) {
    if (!$uri) {
      return $uri;
    }

    $uri_prefixes = [
      'http://',
      'https://',
      'commons.cwrc.ca/',
      'cwrc.ca/islandora/object/',
      'dev-02.cwrc.ca/islandora/object/'
    ];
    $person_id = str_replace($uri_prefixes, '', $uri);
    return rtrim(str_replace([':', '%3A0', '%3A'], '_', $person_id), '/');
  }

  private function addContentItemsToParagraphs($items, FieldItemListInterface $paragraphs, int $heading_lvl, $has_level_3) {
    foreach ($items as $item) {
      if (isset($item->heading) && isset($item->heading->id)) {
        $item->heading->level = 'h' . $heading_lvl;
        // Update that we have build a level 3 heading.
        $has_level_3 = $heading_lvl === 3;
        $paragraphs = $this->addToParagraphsFromObject($item->heading, $paragraphs, 'heading');
      }

      if (isset($item->content) && is_array($item->content)) {
        $level = $has_level_3 ? $heading_lvl + 1 : $heading_lvl;
        $this->addContentItemsToParagraphs($item->content, $paragraphs, $level, $has_level_3);
      }
      elseif (isset($item->event) && isset($item->event->time->value)) {
        $paragraphs = $this->addToParagraphsFromObject($item->event, $paragraphs, 'event');
      }
      elseif (isset($item->paragraph) && isset($item->paragraph->id) && isset($item->paragraph->content)) {
        $paragraphs = $this->addToParagraphsFromObject($item->paragraph, $paragraphs, 'basic');
      }
    }
    return $paragraphs;
  }

  private function addToParagraphsFromObject(\stdClass $object, FieldItemListInterface $paragraphs, string $paragraph_type) {
    $repository = $this->repositoryManager()->repository('paragraph', $paragraph_type);
    if ($repository && !$repository->getBundle()) {
      $repository->setBundle($paragraph_type);
    }
    assert($repository instanceof ParagraphRepository);
    $wrapped_entity = $repository->createWrappedEntityFromObject($object);
    $wrapped_entity->setDataFromObject($object);
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $wrapped_entity->getEntity();
    $paragraphs->appendItem([
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ]);
    return $paragraphs;
  }

  private function setBasicParagraphFieldFromObject(\stdClass $object): ParagraphInterface {
    $repository = $this->repositoryManager()->repository('paragraph', 'basic');
    if ($repository && !$repository->getBundle()) {
      $repository->setBundle('basic');
    }
    assert($repository instanceof ParagraphRepository);
    $wrapped_paragraph_entity = $repository->findBasicParagraphByBody($object->content);
    if (!$wrapped_paragraph_entity) {
      $wrapped_paragraph_entity = $repository->createWrappedEntityFromObject($object);
      $wrapped_paragraph_entity->setDataFromObject($object);
      $wrapped_paragraph_entity->save();
    }
    return $wrapped_paragraph_entity->getEntity();
  }

}
