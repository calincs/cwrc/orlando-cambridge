<?php

namespace Drupal\orlando_interface_ingestion\Commands;

use Drupal\Component\Utility\Xss;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\orlando_interface_ingestion\Event\ConnectionIngestionEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_ingestion\Event\IngestionStartEvent;
use Drupal\orlando_interface_ingestion\Event\ItemIngestionEvent;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\BibciteReferenceRepository;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\MediaRepository;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryInterface;
use Drush\Commands\DrushCommands;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class IngestItemsCommands extends DrushCommands {

  const FILES_ENTRIES_DIR = 'private://orlando-2-0-c-modelling/entities/entries';
  const FILES_ENTITIES_DIR = 'private://orlando-2-0-c-modelling/entities';
  const FILENAME_ID_SEPARATOR = '_';

  /**
   * The file system helper service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The typed entity repository manager.
   *
   * @var \Drupal\typed_entity\RepositoryManager
   */
  protected RepositoryManager $repositoryManager;

  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Construct a new IngestCommands object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system helper service.
   * @param \Drupal\typed_entity\RepositoryManager $repository_manager
   *   The typed entity repository manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service
   */
  public function __construct(FileSystemInterface $file_system, RepositoryManager $repository_manager, EventDispatcherInterface $event_dispatcher) {
    parent::__construct();

    $this->fileSystem = $file_system;
    $this->repositoryManager = $repository_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Ingests a list of author profiles entries.
   *
   * @command oi_ingest:entries
   *
   * @param string $entry_type
   *   The entry type/ Currently supporting author_profile and event for
   *   standalone events.
   * @param array $opts
   *   A list of options where ids of entries to ingest can be passed using the
   *   entries a flag or the all flags to ingest all available entries.
   *
   * @throws \Exception
   *
   * @usage drush oi_ingest:entries author_profile -e akhman -e atwoma
   *   Ingest author profiles for akhman and atwoma.
   * @usage drush oi_ingest:entries author_profile -e akhman
   *   Ingest author profiles for akhman only.
   * @usage drush oi_ingest:entries author_profile --all
   *   Ingest all entries.
   * @usage drush oi_ingest:entries author_profile --all --basex-only
   *   Ingest all author profile entries but in basex only.
   * @usage drush oi_ingest:entries event --all --basex-only
   *    Ingest all event entries but in basex only.
   * @usage drush oi_ingest:entries author_profile --all --limit 10
   *   Ingest all entries but limited to 10.
   * @usage drush oi_ingest:entries author_profile --all --offset 400 --limit 10
   *   Ingest entries where the sequence will start at 309 offset in the list
   *   but limited to 10.
   *
   * @aliases oii_entries
   */
  public function ingestItems(string $entry_type, array $opts = ['entries|e' => [], 'all|a' => FALSE, 'limit' => 0, 'offset|o' => 0, 'basex-only' => FALSE, 'sleep-seconds|s' => 20, 'reverse' => FALSE]) {
    $items = [];
    $is_author_profile = $entry_type === 'author_profile';
    if ($is_author_profile) {
      $entries_dir_realpath = $this->fileSystem->realpath(AuthorProfile::ENTRIES_DIR) . '/';
    }
    elseif ($entry_type === 'event') {
      $entries_dir_realpath = $this->fileSystem->realpath(Event::ITEMS_DIR) . '/';
    }
    else {
      throw new \Exception(dt('Invalid entry type (@type) provided!', [
        '@type' => $entry_type,
      ]));
    }

    if ($opts['all']) {
      $items = $this->getAllItems($entries_dir_realpath);
    }
    elseif (!empty($opts['entries'])) {
      $items = $opts['entries'];
    }

    if (!$items) {
      throw new \Exception(dt('No entries was provided or @dir is empty!', [
        '@dir' => $entries_dir_realpath,
      ]));
    }

    if (!empty($opts['reverse'])) {
      $items = array_reverse($items);
    }

    if ($opts['all'] && $opts['offset'] > 0) {
      // Since element 1 is at 0 index we ensure that when offset is 1 we
      // pass 0. This also allow to start at the 400 elements instead of 401.
      $items = array_slice($items, $opts['offset'] - 1);
    }

    // Let other modules know that we have started the ingestion, and they can
    // do stuff.
    $event = new IngestionStartEvent($items, $entry_type);
    $this->eventDispatcher->dispatch($event, IngestionEvents::INGESTION_STARTED);

    $limit = (int) $opts['limit'];
    $counter = 1;
    $sleep_counter = 100;
    $sleep_seconds = $opts['sleep-seconds'] ?? 20;
    $total = count($items);
    // In case we have a limit, and it's less than the total count of all the
    // elements, then the limit becomes the total number of items to be
    // processed.
    $total = ($limit > 0 && $limit < $total) ? $limit : $total;
    $json_key = $is_author_profile ? 'entry' : 'standalone_event';
    $basex_only = !empty($opts['basex-only']);
    foreach ($items as $id) {
      if (!($data = $this->getItemData($is_author_profile, $id))) {
        $this->io()->warning(dt('Skipping "@id" item. No data could be retrieved!', ['@id' => $id]));
        $counter++;
        continue;
      }

      $connections = $data->{$json_key}->connections ?? NULL;
      if (!$connections) {
        $this->io()->warning(dt("We couldn't extract any connections from json for @id", [
          '@id' => $id,
        ]));
        $counter++;
        continue;
      }

      $this->ingestItemConnections($id, $connections, $basex_only, [
        'counter' => $counter,
        'total' => $total,
      ]);
      $type = $is_author_profile ? 'author profile' : 'standalone event';
      if (!$basex_only) {
        $this->ingestItem($type, $data, $id);
      }
      else {
        $this->io()->caution(dt('The @type with "@id" is being only ingested in basex!', [
          '@id' => $id,
          '@type' => $type,
        ]));
      }
      $event = new ItemIngestionEvent($id, $json_key, $data->xmlFile);
      $this->eventDispatcher->dispatch($event, IngestionEvents::ITEM_INGESTION);

      if ($limit > 0) {
        if ($counter === $limit) {
          break;
        }
      }
      if ($counter === $sleep_counter) {
        // Sleeping after every 100 processed elements to avoid running out of
        // memory.
        $sleep_counter += 100;
        sleep($sleep_seconds);
      }
      $counter++;
    }
    $event = new \Symfony\Contracts\EventDispatcher\Event();
    $this->eventDispatcher->dispatch($event, IngestionEvents::INGESTION_FINISHED);
  }

  /**
   * Ingests a list of author profiles entry images.
   *
   * @command oi_ingest:entries-images
   *
   * @param array $opts
   *   Comma-separated list of author profiles ids to ingest. Pass "all" to
   *   ingest all entries.
   *
   * @throws \Exception
   *
   * @usage drush oi_ingest:entries-images -e akhman -e atwoma
   *   Ingest author profiles for akhman and atwoma.
   * @usage drush oi_ingest:entries-images -e akhman
   *   Ingest author profiles for akhman only.
   * @usage drush oi_ingest:entries-images all
   *   Ingest all entries. Not currently supported.
   *
   * @aliases oii_entries_images
   */
  public function ingestEntriesImages(array $opts = ['entries|e' => [], 'all|a' => FALSE, 'reingest|i' => FALSE]) {
    if ($opts['all']) {
      $entries_dir_realpath = $this->fileSystem->realpath(AuthorProfile::ENTRIES_DIR) . '/';
      $entries = $this->getAllItems($entries_dir_realpath);
    }
    else {
      $entries = $opts['entries'];
    }
    $reingest = $opts['reingest'];
    /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\MediaRepository $repository */
    $repository = $this->getWrappedEntityRepository('media', 'image');
    $repository->ingestMediaImagesByEntries($entries, $reingest);
  }

  /**
   * Fixes replaced media image files.
   *
   * @command oi_ingest:entries-images-fix
   *
   * @usage drush oi_ingest:entries-images-fix
   *
   * @aliases oii_entries_images_fix
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function fixReplacedMediaImages() {
    $media_storage = \Drupal::entityTypeManager()->getStorage('media');
    $ids = $media_storage->getQuery()->accessCheck(FALSE)
      ->condition('bundle', 'image')
      ->exists('field_image_mods')
      ->condition('field_path', 'orlando-2-0-c-modelling/entities/entries/', 'STARTS_WITH')
      ->execute();

    $total = count($ids);
    $counter = 0;
    foreach($ids as $id) {
      /** @var \Drupal\media\MediaInterface $media */
      $media = $media_storage->load($id);
      /** @var \Drupal\file\FileInterface $image */
      $image = $media->get('field_media_image')->entity;
      $image_file_uri = $image->getFileUri();
      if (!file_exists($image_file_uri)) {
        $counter++;
        $this->io()->text(dt('The "@uri" image file uri is missing from the system.', [
          '@uri' => $image_file_uri,
        ]));
        $images_path = $media->get('field_path')->value;
        $author_profile_id = str_replace(['orlando-2-0-c-modelling/entities/entries/', '/images'], '', $images_path);
        if ($author_profile_id) {
          $entry_dir = AuthorProfile::getAuthorProfileXmlUri($author_profile_id);
          $entry_dir_realpath = $this->fileSystem->realpath($entry_dir);
          $pattern = $entry_dir_realpath . '/images/*.{jpg,png,jpeg}';
          $images = glob($pattern, GLOB_BRACE);

          // We only want to do this on images folder where there is only one
          // image file.
          if (count($images) > 1) {
            // Checking if only the file extension have been changed to png.
            $new_image_uri = str_replace('.jpg', '.png', $image_file_uri);
            // No image with png extension exists let try the .jpeg.
            $new_image_uri = file_exists($new_image_uri) ? $new_image_uri : str_replace('.jpg', '.jpeg', $image_file_uri);
            if (file_exists($new_image_uri)) {
              $info = pathinfo($this->fileSystem->realpath($new_image_uri));
              $new_image_mime_type = 'image/' . $info['extension'];
              $new_mod_uri = str_replace($info['extension'], 'xml', $new_image_uri);
              $this->updateReplacedImageFile($image, $new_image_uri, $new_image_mime_type, $info['basename']);
              $this->updateReplacedMediaImageMetadata($media, $image, $new_mod_uri);
              continue;
            }
            $this->io()->warning(dt('Processing of "@uri" have been skipped because there is more than one image file.', [
              '@uri' => $image_file_uri,
            ]));
            continue;
          }
          foreach ($images as $image_realpath) {
            $info = pathinfo($image_realpath);
            $this->io()->text(dt('---- The image "@uri".', [
              '@uri' => $image_realpath,
            ]));
            $mod = str_replace($info['extension'], 'xml', $image_realpath);
            if (is_file($mod)) {
              $this->io()->text(dt('---- The mod "@uri".', [
                '@uri' => $mod,
              ]));
              $full_path_prefix = '/mnt/files/private/';
              $stream_path_prefix = 'private://';
              $new_image_uri = str_replace($full_path_prefix, $stream_path_prefix, $image_realpath);
              $new_mod_uri = str_replace($full_path_prefix, $stream_path_prefix, $mod);
              $new_image_mime_type = 'image/' . $info['extension'];

              // Updating the image.
              $this->updateReplacedImageFile($image, $new_image_uri, $new_image_mime_type, $info['basename']);
              image_path_flush($image->getFileUri());

              // Updating the mod file if the image id changed.
              /** @var \Drupal\file\FileInterface $mod_file */
              $mod_file = $media->get('field_image_mods')->entity;
              if ($mod_file->getFileUri() !== $new_mod_uri) {
                $mod_file->setFileUri($new_mod_uri);
                $mod_file->save();
              }

              // Updating the media metadata.
              $this->updateReplacedMediaImageMetadata($media, $image, $new_mod_uri);
            }
          }
        }
      }
    }

    $this->io()->title(dt('@counter/@total images was processed.', [
      '@counter' => $counter,
      '@total' => $total,
    ]));
  }

  /**
   * Re-ingests embedded events based on a list of author profiles entries.
   *
   * @command oi_ingest:entries-events
   *
   * @param array $opts
   *   Comma-separated list of author profiles ids to ingest. Pass "all" to
   *   ingest all entries.
   *
   * @throws \Exception
   *
   * @usage drush oi_ingest:entries-events -e akhman -e atwoma
   *   Ingest author profiles for akhman and atwoma.
   * @usage drush oi_ingest:entries-events -e akhman
   *   Ingest author profiles for akhman only.
   * @usage drush oi_ingest:entries-events all
   *   Ingest all entries. Not currently supported.
   *
   * @aliases oii_entries_events
   */
  public function reingestEntriesEmbeddedEvents($opts = ['entries|e' => []]) {
    $entries = $opts['entries'];
    foreach ($entries as $entry) {
      if (!($data = $this->getItemData(TRUE, $entry))) {
        $this->io()->warning(dt('Skipping "@id" item. No data could be retrieved!', ['@id' => $entry]));
        continue;
      }

      /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\ProfileEventRepository $repository */
      $repository = $this->getWrappedEntityRepository('profile_event', 'profile_event');
      $this->say(dt('Processing @id', ['@id' => $entry]));
      $result = $repository->updateEmbeddedEventFromEntry($data);
      if (!is_array($result)) {
        $this->io()->error(dt('The @id wasn\'t processed', [
          '@id' => $entry,
        ]));
      }
      else {
        $this->io()->success(dt('Successfully re-ingested @count events embedded in @entry', [
          '@count' => count($result),
          '@entry' => $entry,
        ]));
      }
    }
  }

  /**
   * Helper command to test an xpath for a particular connection.
   *
   * @command oi_ingest:test-xpath
   *
   * @param string $connection_id
   * @param string $vid
   * @param string $xpath
   *
   * @usage drush oi_ingest:test-xpath 'orlando:0f34ea95-cc5f-4ec5-bd0f-e6ab2dbdc69b' person '/entity/person[1]/identity[1]/preferredForm[1]/namePart[@partType='family"]'
   *   Test the provided xpath for a person with provided id.
   *
   * @aliases oii_test_xpath
   */
  public function testXpathSelection(string $connection_id, string $vid, string $xpath) {
    $connection = $vid . 's';
    $connections_dir = static::FILES_ENTITIES_DIR . '/' . $connection;
    // Getting the connection filename.
    $filename_id = rtrim(str_replace([':', '%3A0', '%3A'], self::FILENAME_ID_SEPARATOR, $connection_id), '/');
    $filename = "$connections_dir/$filename_id.xml";
    if (!is_file($filename)) {
      $this->io()->warning(dt('@filename does\'t exists!', [
        '@filename' => $filename,
      ]));
      return;
    }

    $value = TaxonomyTermRepository::getValueFromXmlFile($filename, $xpath);
    $this->io()->success(dt('The queried values for @id with @xpath is<br> @value', [
      '@id' => $connection_id,
      '@xpath' => $xpath,
      '@value' => json_encode($value),
    ]));
  }

  /**
   * Helper command to update xml uri attached to the node author profiles.
   *
   * @command oi_ingest:update-xml-uris
   **
   * @usage drush oi_ingest:update-xml-uris
   *   To update the paths.
   *
   * @aliases oii_update_xml_uris
   */
  public function updateXmlEntryFilesUris() {
    /** @var \Drupal\file\FileStorageInterface $file_storage */
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    $fids = $file_storage->getQuery()
      ->condition('uri', 'transformed', 'CONTAINS')
      ->accessCheck(FALSE)
      ->execute();

    if ($fids) {
      foreach ($fids as $fid) {
        /** @var \Drupal\file\FileInterface $file */
        $file = $file_storage->load($fid);
        $old_uri = $file->getFileUri();
        $entry = str_replace('.xml', '', $file->getFilename());
        $uri = AuthorProfile::getAuthorProfileXmlUri($entry) . "/$entry.xml";
        $file->setFileUri($uri);
        $file->save();

        if ($old_uri !== $file->getFileUri()) {
          $this->io()->success(dt('The @name uri have been successful updated!', ['@name' => $file->getFilename()]));
        }
        else {
          $this->io()->warning(dt('Failed to update the uri for @name', ['@name' => $file->getFilename()]));
        }
      }
    }
    else {
      $this->io()->warning(dt('No files contains the word transformed!'));
    }
  }

  /**
   * Helper command to check a bibliography xml mapping type.
   *
   * @command oi_ingest:check-bibliographies-type
   *
   * @usage drush oi_ingest:check-bibliographies-type
   *   To check the type.
   *
   * @aliases oii_check_bibliographies_type
   */
  public function checkBibliographyXmlTypes() {
    $bibliography_dir = static::FILES_ENTITIES_DIR . '/bibls';
    $real_path = $this->fileSystem->realpath($bibliography_dir);
    $type_xpath_mapping = [
      'audiovisual' => 0,
      'conference_paper' => 0,
      'correspondence' => 0,
      'journal' => 0,
      'manuscript' => 0,
      'book_chapter' => 0,
      'website' => 0,
      'journal_article' => 0,
      'book' => 0,
    ];
    foreach(glob($real_path . '/*.xml') as $filename){
      if($type = BibciteReferenceRepository::extractTypeFromXml($filename)) {
        $type_xpath_mapping[$type]++;
      }
      else {
        $this->io()->error("$filename\t\n");
      }
    }
    $this->io()->note(json_encode($type_xpath_mapping));
  }

  /**
   * Helper command to ingest all bibliographies.
   *
   * @command oi_ingest:bibliographies
   *
   * @usage drush oi_ingest:bibliographies
   *   To ingest all bibliographies.
   * @usage drush oi_ingest:bibliographies --all --limit 10
   *    Ingest all bibliographies but limited to 10.
   * @usage drush oi_ingest:bibliographies --all --basex-only
   *   Ingest all bibliographies in basex only.
 * @usage drush oi_ingest:bibliographies -b ffffbf9b-13ef-44af-9d5c-3890dec195e0
   *    Ingest author profiles for ffffbf9b-13ef-44af-9d5c-3890dec195e0 only.
   * @usage drush oi_ingest:bibliographies --all --offset 400 --limit 10
   *    Ingest items where the sequence will start at 309 offset in the list
   *    but limited to 10.
   * @usage drush oi_ingest:bibliographies --all --reverse
   *   The reverse flag allows starting with the items that are last on the list.
   * @usage drush oi_ingest:bibliographies --all --reprocess --skip-event-dispatch
   *  To skip event dispatch and resave existing content.
   *
   * @aliases oii_bibliographies
   */
  public function ingestAllBibliographies(array $opts = ['bibliographies|b' => [], 'all|a' => FALSE, 'limit' => 0, 'offset|o' => 0, 'reverse' => FALSE, 'basex-only' => FALSE, 'skip-event-dispatch' => FALSE, 'reprocess' => FALSE]) {
    $bibliography_dir = static::FILES_ENTITIES_DIR . '/bibls';
    $real_path = $this->fileSystem->realpath($bibliography_dir);

    $items = [];
    if ($opts['all']) {
      $items = glob($real_path . '/*.xml');
      if ($opts['offset'] > 0) {
        // Since element 1 is at 0 index we ensure that when offset is 1 we
        // pass 0. This also allow to start at the 400 elements instead of 401.
        $items = array_slice($items, $opts['offset'] - 1);
      }
    }
    elseif (!empty($opts['bibliographies'])) {
      $items = $opts['bibliographies'];
    }

    if (!$items) {
      throw new \Exception(dt('No entries was provided or @dir is empty!', [
        '@dir' => $real_path,
      ]));
    }

    if (!empty($opts['reverse'])) {
      $items = array_reverse($items);
    }

    $bibliography_repositories = [];
    $limit = (int) $opts['limit'];
    $counter = 1;
    $total = count($items);
    $total = ($limit > 0 && $limit < $total) ? $limit : $total;
    $basex_only = !empty($opts['basex-only']);
    $skip_event_dispatch = !empty($opts['skip-event-dispatch']);
    $reprocess = !empty($opts['reprocess']);
    foreach($items as $filename) {
      if (!str_contains($filename, 'orlando_')) {
        $filename = 'orlando_' . $filename;
      }
      if (!str_starts_with($filename, $real_path)) {
        $filename = $real_path . '/' . $filename;
      }
      if (!str_ends_with($filename, '.xml')) {
        $filename .= '.xml';
      }

      $t_args = [
        '@filename' => $filename,
        '@current' => $counter,
        '@total' => $total,
      ];
      // Let the user know which connection id we are working with.
      $this->io()->text(dt('(@current/@total) Ingestion process for @filename', $t_args));
      if(!($bundle = BibciteReferenceRepository::extractTypeFromXml($filename))) {
        $this->io()->caution(dt('(@current/@total) We could not extract the bundle for @filename!', $t_args));
        continue;
      }

      if (!isset($bibliography_repositories[$bundle])) {
        $bibliography_repositories[$bundle] = $this->getWrappedEntityRepository('bibcite_reference', $bundle);
      }
      /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\BibciteReferenceRepository $repository */
      $repository = $bibliography_repositories[$bundle];
      $path_info = pathinfo($filename);
      $connection_id = str_replace('orlando_', '', $path_info['filename']);
      if (!$basex_only) {
        /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\BibciteReferences\BibCiteReference $wrapped_entity */
        if ($wrapped_entity = $repository->findByCWRCId($connection_id)) {
          if ($reprocess) {
            $wrapped_entity->updateUsingXmlFile();
            $this->io()
              ->caution(dt('(@current/@total) The @connection with "@label" was reprocess! Authors only update.', [
                  '@connection' => $bundle,
                  '@label' => $wrapped_entity->getEntity()->label(),
                ] + $t_args));
          }
          else {
            $this->io()
              ->caution(dt('(@current/@total) The @connection with "@label" was already saved!', [
                  '@connection' => $bundle,
                  '@label' => $wrapped_entity->getEntity()->label(),
                ] + $t_args));
          }
        }
        else {
          $wrapped_entity = $repository->createWrappedEntityFromXmlFile($connection_id, $filename);
          $this->io()
            ->success(dt('(@current/@total) The @connection with label: "@label" successfully created.', [
                '@connection' => $bundle,
                '@label' => $wrapped_entity->getEntity()->label(),
              ] + $t_args));
        }
      }
      else {
        $this->io()->caution(dt('The @type with "@id" is being only ingested in basex!', [
          '@id' => $connection_id,
          '@type' => $bundle,
        ]));
      }
      if (!$skip_event_dispatch) {
        $event = new ConnectionIngestionEvent($connection_id, 'bibls', $filename);
        $this->eventDispatcher->dispatch($event, IngestionEvents::CONNECTION_INGESTION);
      }

      if ($limit > 0) {
        if ($counter === $limit) {
          break;
        }
      }
      $counter++;
    }

    if (!$skip_event_dispatch) {
      $event = new \Symfony\Contracts\EventDispatcher\Event();
      $this->eventDispatcher->dispatch($event, IngestionEvents::INGESTION_FINISHED);
    }
  }

  private function ingestItemConnections(string $entry_id, $connections, bool $basex_only, $position) {
    $ext_uri_prefixes = [
      'commons.cwrc.ca/',
      'cwrc.ca/islandora/object/',
      'dev-02.cwrc.ca/islandora/object/',
      'http://',
      'https://',
    ];
    $bundles = [
      'person' => 'taxonomy_term',
      'organization' => 'taxonomy_term',
      'bibl' => 'bibcite_reference',
    ];
    $bibliography_repositories = [];

    foreach ($bundles as $bundle => $entity_type_id) {
      $connection = $bundle . 's';
      $connection_ids = $connections->{$connection} ?? NULL;
      if (is_array($connection_ids) && !empty($connection_ids)) {
        $connection_ids = array_unique($connection_ids);
        $connections_dir = static::FILES_ENTITIES_DIR . '/' . $connection;
        // Getting the current vocabulary-wrapped entity repository.
        /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository $repository */
        $repository = $this->getWrappedEntityRepository($entity_type_id, $bundle);
        $processed_ids = [];
        $is_bibliography = $bundle === 'bibl';
        foreach ($connection_ids as $connection_ext_uri) {
          if (isset($processed_ids[$connection_ext_uri])) {
            continue;
          }
          $processed_ids[$connection_ext_uri] = $connection_ext_uri;

          // Extracting the connection id from the external uri.
          $connection_id = rtrim(str_replace($ext_uri_prefixes, '', $connection_ext_uri), '/');
          // Getting the connection filename.
          $filename_id = rtrim(str_replace([':', '%3A0', '%3A'], self::FILENAME_ID_SEPARATOR, $connection_id), '/');
          $filename = "$connections_dir/$filename_id.xml";
          // Let the user know which connection id we are working with.
          $this->io()->text(dt('Ingestion process for: Entry id: @entry_id (@counter/@total) | File: @filename | Basex Only: @basex', [
            '@entry_id' => $entry_id,
            '@counter' => $position['counter'],
            '@total' => $position['total'],
            '@filename' => $filename,
            '@basex' => $basex_only ? 'True' : 'False',
          ]));

          if (!is_file($filename)) {
            $this->io()->warning(dt('@filename does\'t exists!', [
              '@filename' => $filename,
            ]));
            continue;
          }

          $connection_id = rtrim(str_replace('orlando:', '', $connection_id), '/');

          // Using the basex only flag to skip any drupal related processing.
          if (!$basex_only) {
            // When dealing with bibliographies, we need to get bundles from
            // xml.
            if ($is_bibliography) {
              $bundle = BibciteReferenceRepository::extractTypeFromXml($filename);
              if (!$bundle) {
                $this->io()
                  ->warning(dt('We couldn\'t determine the bibliography type for @filename. Skipping!', [
                    '@filename' => $filename,
                  ]));
                continue;
              }
              if (!isset($bibliography_repositories[$bundle])) {
                $bibliography_repositories[$bundle] = $this->getWrappedEntityRepository($entity_type_id, $bundle);
              }
              $repository = $bibliography_repositories[$bundle];
            }

            $wrapped_entity = $repository->findByCWRCId($connection_id);
            if (!$wrapped_entity) {
              // Creating the taxonomy term.
              $wrapped_entity = $repository->createWrappedEntityFromXmlFile($connection_id, $filename);
              if ($wrapped_entity) {
                $this->io()
                  ->success(dt('The @connection with label: "@label" successfully created.', [
                    '@connection' => $bundle,
                    '@label' => $wrapped_entity->getEntity()->label(),
                  ]));
              }
            }
            else {
              $this->io()
                ->caution(dt('The @connection with "@label" was already saved!', [
                  '@connection' => $bundle,
                  '@label' => $wrapped_entity->getEntity()->label(),
                ]));
            }
          }

          $event = new ConnectionIngestionEvent($connection_id, $connection, $filename);
          $this->eventDispatcher->dispatch($event, IngestionEvents::CONNECTION_INGESTION);
        }
      }
      else {
        $this->io()->warning(dt('@id entry didn\'t contain any @connection!', [
          '@id' => $entry_id,
          '@connection' => $connection,
        ]));
      }
    }
  }

  private function ingestItem(string $type, \stdClass $object, string $id) {
    $is_author_profile = $type === 'author profile';
    if ($is_author_profile) {
      /** @var \Drupal\orlando_interface_ingestion\\Plugin\TypedRepositories\AuthorProfileRepository $repository */
      $repository = $this->getWrappedEntityRepository('node', 'author_profile');
      $id = $object->entry->entry_identifiers->id;
      $wrapped_entity = $repository->findByCWRCId($id);
    }
    else {
      /** @var \Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\ProfileEventRepository $repository */
      $repository = $this->getWrappedEntityRepository('profile_event', 'profile_event');
      $wrapped_entity = $repository->findById($id);
      $object->id = $id;
    }

    if (!$wrapped_entity) {
      $wrapped_entity = $is_author_profile
      ? $repository->createWrappedEntityFromObject($object)
      : $repository->createEmbeddedEventFromObject($object);

      if ($wrapped_entity) {
        $this->io()->success(dt('The @type "@item (@id)" was successfully saved!', [
          '@item' => $wrapped_entity->label(),
          '@id' => $wrapped_entity->getEntity()->id(),
          '@type' => $type,
        ]));
      }
    }
    else {
      $this->io()->caution(dt('The @type "@entry (@id)" was already saved!', [
        '@entry' => $wrapped_entity->label(),
        '@id' => $wrapped_entity->getEntity()->id(),
        '@type' => $type,
      ]));
    }
  }

  private function getItemData(bool $is_author_profile, string $id): ?\stdClass {
    $events_dir = Event::ITEMS_DIR;
    $dir = $is_author_profile ? static::FILES_ENTRIES_DIR : $events_dir;
    $filename = $is_author_profile
      ? AuthorProfile::getAuthorProfileXmlUri($id) . '/' . $id . '.json'
      : "$events_dir/$id/$id.json";
    if (!is_file($filename)) {
      $this->io()->warning(dt('@file file for @id wasn\'t found in @dir!', [
        '@id' => $id,
        '@file' => $filename,
        '@dir' => $dir,
      ]));
      return NULL;
    }

    $json = file_get_contents($this->fileSystem->realpath($filename));
    if ($data = json_decode($json)) {
      $data->xmlFile = str_replace('.json', '.xml', $filename);
    }
    return $data;
  }

  /**
   * Gets the typed entity repository.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\typed_entity\TypedRepositories\TypedRepositoryInterface|null
   *   The typed entity repository.
   */
  private function getWrappedEntityRepository(string $entity_type_id, string $bundle): ?TypedRepositoryInterface {
    if($bundle === 'bibl') {
      return NULL;
    }
    assert($this->repositoryManager instanceof RepositoryManager);
    $repository = $this->repositoryManager->repository($entity_type_id, $bundle);
    if ($repository && !$repository->getBundle()) {
      $repository->setBundle($bundle);
    }
    return $repository;
  }

  private function getAllItems(string $entries_dir_realpath) {
    $pattern = $entries_dir_realpath . '*';
    $entries_dir = glob($pattern, GLOB_ONLYDIR);
    return array_map(function($dir) use ($entries_dir_realpath) {
      return str_replace($entries_dir_realpath, '', $dir);
    }, $entries_dir);
  }

  private function updateReplacedImageFile(FileInterface $file, string $uri, string $mime_type, string $filename) {
    $file->setFileUri($uri);
    $file->setMimeType($mime_type);
    $file->setFilename($filename);
    $file->save();
    image_path_flush($file->getFileUri());
  }

  private function updateReplacedMediaImageMetadata(MediaInterface $media, FileInterface $image, string $mod_xml) {
    $mod = MediaRepository::transformXmlToArray($mod_xml);
    $title = substr(Xss::filter(strip_tags($mod['titleInfo']['title'])), 0, 1024);
    $media->get('field_media_image')->first()->setValue([
      'target_id' => $image->id(),
      'alt' => substr(Xss::filter(strip_tags($mod['abstract'])), 0, 512),
      'title' => $title,
    ]);
    $media->set('name', $title);
    $media->save();
  }

}
