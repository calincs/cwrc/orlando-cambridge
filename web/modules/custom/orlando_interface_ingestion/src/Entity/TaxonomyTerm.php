<?php

namespace Drupal\orlando_interface_ingestion\Entity;

use Drupal\taxonomy\Entity\Term;

class TaxonomyTerm extends Term {

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($this->bundle() === 'person') {
      /** @var \Drupal\typed_entity\RepositoryManager $repository_manager */
      $repository_manager = \Drupal::service('Drupal\typed_entity\RepositoryManager');
      /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\TaxonomyTerms\Person $wrapped_entity */
      $wrapped_entity = $repository_manager->wrap($this);
      return $wrapped_entity->label();
    }
    return parent::label();
  }

}
