<?php

namespace Drupal\orlando_interface_ingestion;

/**
 * Helper class to parse XML to string.
 */
interface XmlParserInterface {

  /**
   * Gets the processor version.
   *
   * @return string
   *   The version.
   */
  public function getProcessorVersion(): string;

  /**
   * Transforms xml to string using xslt.
   *
   * @param string $filename
   *   The full file path as name to transform.
   * @param string $extension
   *   Used to determine which xslt file to use.
   *
   * @return string
   *   The results.
   */
  public function transform(string $filename, string $extension): ?string;

  /**
   * Gets json string for given author profile/entry id.
   *
   * @param string $id
   *   The id.
   *
   * @return string
   *   The json content.
   */
  public function getEntryJson(string $id): string;

}
