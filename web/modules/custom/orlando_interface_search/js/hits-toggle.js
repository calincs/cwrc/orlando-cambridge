(function ($, window, Drupal) {

  Drupal.oiSearch = Drupal.oiSearch || {};

  Drupal.oiSearch.btnToggleClick = function () {
    const $btnConfig = JSON.parse(this.dataset.oiSearchResultItemHitToggleConfig);
    const wrapper = document.getElementById($btnConfig.item_hits_wrapper_id);
    wrapper.classList.toggle('tw-hidden');
    const isClosed = wrapper.classList.contains('tw-hidden');
    this.setAttribute('aria-expanded', isClosed ? 'false' : 'true');
    this.querySelectorAll('svg, span').forEach((elt) => {
      elt.classList.toggle('tw-hidden');
    });
  };

  Drupal.oiSearch.ajaxComplete = function (context, selector) {
    const wrapper = context.querySelector(selector);
    if (!wrapper || wrapper.classList.contains('ajax-render-completed')) { return; }

    wrapper.classList.add('ajax-render-completed');
    const btnToggle = wrapper.closest('div.oi-search--results-content-item')
      .querySelector('[data-oi-search-result-item-hit-toggle-config]');

    // Let remove all the ajax stuff.
    const btnElement = $(btnToggle);
    btnElement.unbind();
    // Add a toggle click back.
    btnElement.on('click', Drupal.oiSearch.btnToggleClick);
    // Triggering the click here to expand/show the results directly.
    btnElement.trigger('click');
  };

  Drupal.oiSearch.bindAjaxButtonToggle = (element) => {
    // Bind Ajax behaviors to all button items having the
    // data-oi-search-result-item-hit-toggle-config attribute.
    $(element)
      .find('[data-oi-search-result-item-hit-toggle-config]')
      .once('ajax')
      .each((i, btnToggle) => {
        const $btnConfig = JSON.parse(btnToggle.dataset.oiSearchResultItemHitToggleConfig);
        const $btnElement = $(btnToggle);
        const url = `/interface-search/item-hits/${$btnConfig.item_entity_type_id}/${$btnConfig.item_entity_id}/${$btnConfig.item_id}/${$btnConfig.item_hit_count}/${$btnConfig.item_search_results_index}/nojs`;

        const elementSettings = {
          progress: { type: 'throbber' },
          dialogType: '',
          dialog: undefined,
          dialogRenderer: undefined,
          base: $btnElement.attr('id'),
          element: btnToggle,
          url: `${url}${window.location.search}`,
          event: 'click',
        };
        Drupal.ajax(elementSettings);
      });
  };

  Drupal.behaviors.oiSearchItemHitsToggle = {
    attach(context) {
      Drupal.oiSearch.bindAjaxButtonToggle(document.body);
      $(document).ajaxComplete(function(event, response) {
        if (response.hasOwnProperty('responseJSON') && response.responseJSON) {
          for (let data of response.responseJSON) {
            if (data.hasOwnProperty('settings') && data.settings.hasOwnProperty('oi_search_item_hits')) {
              Drupal.oiSearch.ajaxComplete(context, data.selector);
              break;
            }
          }
        }
      });
    }
  };
})(jQuery, window, Drupal);
