<?php

namespace Drupal\orlando_interface_search\Event;

final class SearchEvents {

  /**
   * The name of the event triggered when adding the xquery search function to
   * the search xquery.
   *
   * @see \Drupal\orlando_interface_search\Event\AddingXquerySearchFunctionEvent
   * @see \Drupal\orlando_interface_search\Driver\Database\basex\Connection::addReturnXquerySearchFunction()
   *
   * @Event
   *
   * @var string
   */
  const ADDING_XQUERY_SEARCH_FN = 'oi_search.adding_xquery_search_function';

  /**
   * The event dispatched when setting up the search results summary.
   *
   * @see \Drupal\orlando_interface_search\Event\ResultsSummaryEvent
   * @see \Drupal\orlando_interface_search\Controller\ResultsController::getResultsSummary()
   *
   * @Event
   *
   * @var string
   */
  const RESULT_SUMMARY = 'oi_search.result_summary';

}
