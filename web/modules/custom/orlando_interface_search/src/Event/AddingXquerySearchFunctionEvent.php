<?php

namespace Drupal\orlando_interface_search\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event dispatched when adding the xquery search function to the search
 * xquery.
 */
class AddingXquerySearchFunctionEvent extends Event {

  protected $fn;

  protected $appliedVariables;

  public function __construct(string $fn, array $applied_variables) {
    $this->fn = $fn;
    $this->appliedVariables = $applied_variables;
  }

  public function setFunction(string $fn) {
    $this->fn = $fn;
  }

  public function getFunction(): string {
    return $this->fn;
  }

  public function getAppliedVariables(): array {
    return $this->appliedVariables;
  }

}
