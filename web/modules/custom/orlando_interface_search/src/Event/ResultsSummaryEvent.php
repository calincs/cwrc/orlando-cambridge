<?php

namespace Drupal\orlando_interface_search\Event;

use Drupal\Component\Utility\Xss;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event dispatched when setting up the search results summary.
 */
class ResultsSummaryEvent extends Event {

  protected $message;

  protected $args;

  protected $queryParameters;

  protected $htmlTags;

  /**
   * Constructs the ResultsSummaryEvent instance.
   *
   * @param string $message
   *   The message containing placeholder(s).
   * @param array $args
   *   The arguments for the message.
   * @param array $query_parameters
   *   The requests query parameters.
   */
  public function __construct(string $message, array $args, array $query_parameters) {
    $this->htmlTags = Xss::getHtmlTagList();
    $this->htmlTags[] = 'span';
    $this->message = Xss::filter($message, $this->htmlTags);
    $this->args = $args;
    $this->queryParameters = $query_parameters;
  }

  /**
   * @return string
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * @param string $message
   */
  public function setMessage(string $message): void {
    $this->message = Xss::filter($message, $this->htmlTags);
  }

  /**
   * @return array
   */
  public function getArgs(): array {
    return $this->args;
  }

  /**
   * @param array $args
   */
  public function setArgs(array $args): void {
    $this->args = $args;
  }

  /**
   * @return array
   */
  public function getQueryParameters(): array {
    return $this->queryParameters;
  }

}
