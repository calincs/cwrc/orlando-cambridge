<?php

namespace Drupal\orlando_interface_search\Item;

class Hit {

  public const XPATH_SEPARATOR = '/';

  protected $xpath;

  protected $id;

  protected $value;

  public function __construct(string $xpath, string $id, $has_value = FALSE) {
    if (!$has_value) {
      $this->id = $id;
    }
    else {
      $this->value = $id;
    }
    $this->xpath = $xpath;
  }

  /**
   * @return string
   */
  public function getXpath(): string {
    return $this->xpath;
  }

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  public function getValue() {
    return $this->value;
  }

}
