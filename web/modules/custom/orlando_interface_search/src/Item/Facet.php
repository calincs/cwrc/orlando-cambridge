<?php

namespace Drupal\orlando_interface_search\Item;

use Drupal\Core\StringTranslation\TranslatableMarkup;

class Facet {

  protected string $type;

  protected string $name;

  protected int $count;

  protected array $subgroup;

  /**
   * FacetItem constructor.
   *
   * @param string $type
   * @param string $name
   * @param int $count
   */
  public function __construct(string $type, string $name, int $count) {
    $this->type = $type;
    $this->name = $name;
    $this->count = $count;
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return int
   */
  public function getCount() {
    return $this->count;
  }

}
