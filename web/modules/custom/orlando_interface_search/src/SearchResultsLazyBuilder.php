<?php


namespace Drupal\orlando_interface_search;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\orlando_interface_ingestion\Entity\ProfileEvent;
use Drupal\orlando_interface_ingestion\WrappedEntities\ProfileEvents\Event;
use Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity;
use Drupal\orlando_interface_search\Utility\QueryHelper;
use Drupal\orlando_interface_support\Plugin\Field\FieldFormatter\IconDefaultFormatter;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchResultsLazyBuilder implements TrustedCallbackInterface {

  use StringTranslationTrait;
  use ItemHitsBuilderTrait;

  /**
   * @var \Drupal\orlando_interface_search\Utility\QueryHelper
   */
  protected $queryHelper;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var array
   */
  protected $searchKeys;

  public function __construct(RequestStack $request_stack, QueryHelper $query_helper, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, RendererInterface $renderer) {
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->queryHelper = $query_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['renderFilterItemsClear', 'renderResultItem'];
  }

  public function renderResultItem($index, $total_items) {
    $error_output = [
      '#markup' => '<p class="tw-text-red-200">' . $this->t('An error has occurred. Refresh the page, and if the problem persists, click “Collaborate” in the footer to report the error.') . '</p>',
    ];
    if (!($search_result = $this->queryHelper->viewResult[$index] ?? [])) {
      return $error_output;
    }

    $wrapped_entity = $search_result['_wrapped_entity'] ?? NULL;
    /** @var \Drupal\orlando_interface_search\Item\Document $item */
    if (!($item = $search_result['_item'] ?? NULL)) {
      return $error_output;
    }

    $icon_identifier = $item->getType();
    $url = '';
    $entity_type_id = '';
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = NULL;
    $linked_titles = [
      'node',
      'taxonomy_term',
    ];

    if ($wrapped_entity && $wrapped_entity instanceof WrappedEntity) {
      $entity = $wrapped_entity->getEntity();
      $title = $entity->label();
      $entity_type_id = $entity->getEntityTypeId();
      if (in_array($entity_type_id, $linked_titles) || $this->isStandaloneEvent($wrapped_entity)) {
        $url = $entity->toUrl()->toString();
      }
      if ($entity instanceof ProfileEvent && !$entity->isEmbedded()) {
        $date_label = $entity->getDateLabel();
        $title = (string) $this->t('<span class="tw-font-bold">@date</span><br/><span>@label</span>', [
          '@date' => $date_label,
          '@label' => trim(str_replace([$date_label . ':', $date_label], '', $title)),
        ]);
      }
      elseif ($entity_type_id === 'paragraph' && $entity->bundle() === 'event') {
        /** @var \Drupal\paragraphs\ParagraphInterface $entity */
        $icon_identifier = $entity->get('field_event')->entity->type->value;
        $parent = $entity->getParentEntity();
        $title = (string) $this->t('Author event in <a href="@url" class="tw-font-bold">@parent</a>', [
          '@parent' => $parent->label(),
          '@url' => $parent->toUrl()->toString(),
        ]);
      }
    }
    else {
      $title = (string) $this->t('@name', [
        '@name' => $item->getName(),
      ]);
    }

    $is_anonymous = $this->isAnonymousUser();
    $hits_content_entry = $this->buildDocumentHitItems($search_result);
    $hits_content = [];
    $clean_text = NULL;
    if (count($hits_content_entry) === 1 && !empty($hits_content_entry[0]['#text'])) {
      $clean_text = trim(str_replace([' ', '\n\t', '\n'], '', strip_tags($hits_content_entry[0]['#text'])));
    }
    if ($clean_text === NULL || $clean_text !== '') {
      $hits_content = [
        '#type' => 'container',
        '#attributes' => ['class' => ['tw-mt-3']],
        '#cache' => ['contexts' => ['user.roles']],
        'content' => $hits_content_entry,
      ];
    }

    $build = [
      '#theme' => 'orlando_interface_search__results__item',
      '#wrapped_entity' => $wrapped_entity,
      '#item' => $item,
      '#entity_type_id' => $entity_type_id,
      '#title' => $title && is_string($title) ? new FormattableMarkup($title, []) : '',
      '#url' => $url,
      '#icon' => $this->getIconTheme($icon_identifier),
      '#is_first' => $search_result['index'] == 0,
      '#is_last' => ($search_result['index'] + 1) == $total_items,
      '#hits' => $hits_content,
      '#is_anonymous_user' => $is_anonymous,
      // Only node author profile and standalone event can expand their results.
      '#can_toggle' => ($entity_type_id == 'node' || $entity_type_id == 'profile_event') && !$is_anonymous,
      '#hit_toggle_attributes' => new Attribute(['class' => [
        'tw-block',
        'tw-my-9',
        'tw-min-w-6',
        'tw-w-9',
        'tw-relative',
        'disabled:tw-cursor-not-allowed',
      ]]),
    ];
    if ($build['#can_toggle'] && $item->getHitCount() > 1) {
      $entity_id = $entity->id();
      $build['#hit_toggle_attributes']->setAttribute('aria-expanded', 'false');
      $build['#hit_toggle_attributes']->setAttribute('data-oi-search-result-item-hit-toggle-config', Json::encode([
        'item_id' => $item->getId(),
        'item_search_results_index' => $index,
        'item_entity_type_id' => $entity_type_id,
        'item_entity_id' => $entity_id,
        'item_hit_count' => $item->getHitCount(),
        'item_hits_wrapper_id' => self::getItemHitsWrapperId($entity_type_id, $entity_id),
      ]));
      $build['#attached']['library'][] = 'orlando_interface_search/item-hits-toggle';
    }
    if ($entity) {
      $cache = new CacheableMetadata();
      $cache->addCacheableDependency($entity);
      $cache->applyTo($build);
    }
    return $build;
  }

  public function renderFilterItemsClear() {
    $parameters = $this->currentRequest->query->all();
    $facet_results = $this->queryHelper->getResultSets()->getFacets();
    $items = [];
    if ($facet_results) {
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $storage */
      $storage = $this->entityTypeManager->getStorage('basex_facet_mapping');
      $facet_mappings = $storage->loadEnabledMappings();
      $query_keys = [];
      foreach ($facet_mappings as $facet_mapping) {
        foreach ($facet_mapping->getFacetWidget()->getQueryKeys() as $query_key) {
          $query_keys[$query_key] = $facet_mapping;
        }
      }

      // Extracting applied mapping from parameters.
      $applied_mappings = array_intersect_key($query_keys, $parameters);
      // Keeping track of applied filters so that the clear all filters link
      // knows which ones to remove.
      $applied_filters = array_keys($applied_mappings);
      foreach ($applied_mappings as $type => $mapping) {
        /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface $mapping */
        // Cloning the current parameters to allow its alteration without
        // affecting its usage after.
        $link_query = $parameters;
        // Removing the current mapping from the query so that the cancel link
        // doesn't contain it.
        unset($link_query[$type]);
        // Also removing page query cause the current page might not be present
        // anymore once we remove this filter.
        if (isset($link_query['page'])) {
          unset($link_query['page']);
        }

        $facet_widget = $mapping->getFacetWidget();
        $label = $facet_widget->buildLabelForSelectedValuesString($type);
        $is_included = strpos($type, 'Included') !== FALSE;
        if ($is_included || strpos($type, 'Excluded') !== false) {
          $label = $is_included ?
            $this->t('@label, Include', ['@label' => $label]) :
            $this->t('@label, Exclude', ['@label' => $label]);
        }

        $selected = $facet_widget->generateSelectedValuesString($parameters[$type], $type);
        if (!$selected) {
          continue;
        }
        $items[] = [
          '#type' => 'html_tag',
          '#tag' => 'a',
          '#attributes' => [
            'href' => Url::fromRoute('orlando_interface_search.page', [], ['query' => $link_query])->toString(),
            'title' => $this->t('Clear @label filter(s)', [
              '@label' => $label,
            ]),
            'class' => [
              'tw-flex',
              'tw-px-1',
              'tw-items-center',
              'tw-bg-gray-100',
              'hover:tw-bg-gray-200',
              'tw-rounded-full',
              'tw-no-underline',
              'tw-space-x-1.5',
            ],
          ],
          'label' => [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $this->t('@label: ', ['@label' => $label]),
          ],
          'selected' => [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#attributes' => [
              'class' => [
                'tw-font-bold',
              ],
            ],
            '#value' => $this->t('@selected', [
              '@selected' => ucwords($selected),
            ]),
            '#suffix' => '<span class="tw-font-bold tw-p-0.5">X</span>',
          ],
        ];
      }
      if ($applied_filters) {
        $query = array_diff_key($parameters, array_flip($applied_filters));
        if (isset($query['page'])) {
          unset($query['page']);
        }
        $items[] = [
          '#type' => 'link',
          '#title' => $this->t('Clear all filters'),
          '#url' => Url::fromRoute('orlando_interface_search.page', [], ['query' => $query]),
          '#attributes' => [
            'class' => [
              'tw-flex',
              'tw-underline',
              'hover:tw-no-underline',
              'focus:tw-no-underline',
            ],
          ],
        ];
      }
    }

    $build = [];
    if ($items) {
      $build = [
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => [
          'class' => [
            'tw-flex',
            'tw-flex-wrap',
            'tw-items-center',
            'tw-text-sm',
            'tw-space-x-3',
          ],
        ],
      ];
    }
    return $build;
  }

  private function getIconTheme(string $identifier) {
    return [
      '#theme' => IconDefaultFormatter::getIconThemeName($identifier),
      '#attributes' => new Attribute([
        'class' => [
          'tw-w-6',
          'tw-h-6',
          'tw-flex',
          'tw-items-center',
          'tw-justify-center',
          'tw-rounded-full',
          'tw-bg-blue-100',
          'tw-text-white',
        ],
        'aria-hidden' => 'true',
      ]),
      '#svg_attributes' => new Attribute([
        'fill' => 'currentColor',
        'width' => '14px',
        'height' => '14px',
        'aria-hidden' => 'true',
      ]),
      '#color_primary' => 'currentColor',
      '#color_secondary' => '#000000',
      '#color_stroke' => 'transparent',
    ];
  }

  private function isAnonymousUser() {
    return $this->currentUser->isAnonymous();
  }

  public static function getItemHitsWrapperId($entity_type_id, $entity_id) {
    return 'oi-search--results-content--item-entity-' . $entity_type_id . '-' . $entity_id . '--hits-wrapper';
  }

  protected function isStandaloneEvent($wrapped_entity) {
    // Always return false if user is anonymous.
    if ($this->isAnonymousUser() || !($wrapped_entity instanceof Event) || $wrapped_entity->isEmbedded()) {
      return FALSE;
    }
    return TRUE;
  }

}
