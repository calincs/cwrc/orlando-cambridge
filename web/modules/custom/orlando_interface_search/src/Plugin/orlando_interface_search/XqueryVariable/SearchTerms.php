<?php

namespace Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Drupal\orlando_interface_search\ExtractSearchTermsTrait;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "searchTerms",
 *   label = @Translation("Search terms"),
 *   reserved = TRUE,
 *   allow_empty = TRUE,
 *   weight = 2,
 * )
 */
class SearchTerms extends BaseXXqueryVariablePluginBase {

  use ExtractSearchTermsTrait;

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $search_terms = 'let $' . $this->getPluginId() . ' := (';
    if (!empty($parameters['keys'])) {
      $search_terms .= '"' . $this->getSearchKeys($parameters['keys'], '","') . '"';
    }
    $search_terms .= ') ';
    return $search_terms;
  }

}
