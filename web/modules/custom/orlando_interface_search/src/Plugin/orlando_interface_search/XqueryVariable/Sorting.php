<?php

namespace Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "sorting",
 *   label = @Translation("Sorting"),
 *   reserved = TRUE,
 *   allow_empty = TRUE,
 *   weight = 4,
 * )
 */
class Sorting extends BaseXXqueryVariablePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $mapping = $this->getMapping(FALSE);
    $sorting_key = $this->getSortingKey($mapping, $parameters);
    return 'let $' . $this->getPluginId() . ' := "' . $mapping[$sorting_key]['sorting'] . '" ';
  }

  public function getSortingKey($mapping, array $parameters) {
    $sorting_key = $parameters['sort'] ?? 'relevance';
    // Ensure that we only allow supported sorting values.
    if ($sorting_key !== 'relevance' && (!is_string($sorting_key) || !isset($mapping[$sorting_key]))) {
      return 'relevance';
    }
    return $sorting_key;
  }

  public function getMapping($as_options = TRUE) {
    $query_parameter_keys = ['relevance', 'az', 'za', 'old', 'new'];
    if ($as_options) {
      return array_combine($query_parameter_keys, [
        new TranslatableMarkup('Relevance'),
        new TranslatableMarkup('A-Z'),
        new TranslatableMarkup('Z-A'),
        new TranslatableMarkup('Oldest first'),
        new TranslatableMarkup('Newest first'),
      ]);
    }
    return array_combine($query_parameter_keys, [
      ['sorting' => 'relevance', 'direction' => 'descending'],
      ['sorting' => 'alphabetical', 'direction' => 'ascending'],
      ['sorting' => 'alphabetical', 'direction' => 'descending'],
      ['sorting' => 'chronological', 'direction' => 'ascending'],
      ['sorting' => 'chronological', 'direction' => 'descending'],
    ]);
  }

}
