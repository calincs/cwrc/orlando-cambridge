<?php

namespace Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "stemming",
 *   label = @Translation("Stemming"),
 *   reserved = TRUE,
 *   allow_empty = TRUE,
 *   weight = 6,
 * )
 */
class Stemming extends BaseXXqueryVariablePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $value = !static::processStemmingParameterValue($parameters) ? 'true' : 'false';
    return 'let $' . $this->getPluginId() . ' := "' . $value . '" ';
  }

  public static function processStemmingParameterValue(array $parameters, bool $default_value = FALSE): bool {
    if (!isset($parameters['exact-match'])) {
      return $default_value;
    }
    if ($parameters['exact-match'] == 0 || $parameters['exact-match'] == 'false' || $parameters['exact-match'] == 'no') {
      return FALSE;
    }
    if ($parameters['exact-match'] == 1 || $parameters['exact-match'] == 'true' || $parameters['exact-match'] == 'yes') {
      return TRUE;
    }
    return $default_value;
  }

}
