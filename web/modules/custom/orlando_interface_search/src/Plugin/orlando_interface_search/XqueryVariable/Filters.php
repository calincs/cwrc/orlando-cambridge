<?php

namespace Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "filters",
 *   label = @Translation("Filters"),
 *   reserved = FALSE,
 *   allow_empty = TRUE,
 *   weight = 3,
 * )
 */
class Filters extends BaseXXqueryVariablePluginBase {

  /**
   * The basex facet mapping storage.
   *
   * @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage
   */
  protected $facetMappingStorage;

  /**
   * Constructs a Filters object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->facetMappingStorage = $entity_type_manager->getStorage('basex_facet_mapping');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $plugin_id = $this->getPluginId();
    $xquery = 'let $' . $this->getPluginId() . ' := map{';
    // Track if the variable had any value.
    $applied = FALSE;
    foreach ($this->facetMappingStorage->loadEnabledMappingsByXqueryVariables($plugin_id) as $mapping) {
      $widget = $mapping->getFacetWidget();
      $query_keys = $widget->getQueryKeys();
      // Extract parameters related to this mapping widget.
      $widget_values = array_intersect_key($parameters, array_flip($query_keys));
      if (!$applied && $widget_values) {
        $applied = TRUE;
      }
      $xquery .= $widget_values ? $widget->buildXQueryString($widget_values) : '';
    }

    if ($applied || $this->allowEmpty()) {
      $xquery = rtrim($xquery, ', ');
      $xquery .= '} ';
      return $xquery;
    }

    // This allows us to return empty string in case this plugin doesn't allow
    // to send an empty variable. For example, we can return
    // `let $filters := map{}`.
    return '';
  }

}
