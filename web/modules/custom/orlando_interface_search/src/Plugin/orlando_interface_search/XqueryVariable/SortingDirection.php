<?php

namespace Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "direction",
 *   label = @Translation("Sorting direction"),
 *   reserved = TRUE,
 *   allow_empty = TRUE,
 *   weight = 5,
 * )
 */
class SortingDirection extends Sorting {

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $mapping = $this->getMapping(FALSE);
    $sorting_key = $this->getSortingKey($mapping, $parameters);
    return 'let $' . $this->getPluginId() . ' := "' . $mapping[$sorting_key]['direction'] . '" ';
  }

}
