<?php

namespace Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable;

use Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXXqueryVariable;
use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;

/**
 * @OrlandoInterfaceSearchBaseXXqueryVariable(
 *   id = "database",
 *   label = @Translation("Database"),
 *   reserved = TRUE,
 *   allow_empty = TRUE,
 *   weight = 1,
 * )
 */
class Database extends BaseXXqueryVariablePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string {
    $connection_options = $connection->getConnectionOptions();
    return sprintf('let $%s := "%s" ', $this->getPluginId(), $connection_options['database']);
  }

}
