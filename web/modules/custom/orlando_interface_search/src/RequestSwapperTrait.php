<?php

namespace Drupal\orlando_interface_search;

use Symfony\Component\HttpFoundation\Request;

trait RequestSwapperTrait {

  /**
   * The query helper service.
   *
   * @var \Drupal\orlando_interface_search\Utility\QueryHelper
   */
  protected $queryHelper;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathStack;

  /**
   * The current router.
   *
   * @var \Drupal\Core\Routing\AccessAwareRouterInterface
   */
  protected $router;

  /**
   * The processor manager.
   *
   * @var \Drupal\Core\PathProcessor\InboundPathProcessorInterface
   */
  protected $pathProcessor;

  /**
   * Array of request.
   *
   * @var array
   */
  protected $storedRequests = [];

  protected function requestSwapWithPath(Request $request, string $path) {
    $new_request = $this->createRequestFromPath($path);
    $request->attributes->add($this->router()->matchRequest($new_request));
    $this->overwriteRequestStack($new_request);
  }

  /**
   * Creates a new request object from a path.
   *
   * @param string $path
   *   A path with facet arguments.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   A new request object.
   */
  protected function createRequestFromPath($path) {
    $new_request = Request::create($path);
    if ($session = $this->requestStack()->getCurrentRequest()->getSession()) {
      $new_request->setSession($session);
    }
    $processed = $this->pathProcessor()->processInbound($path, $new_request);
    $this->currentPathStack()->setPath($processed);

    return $new_request;
  }

  /**
   * Resets the request stack and adds one request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The one and only request.
   */
  protected function overwriteRequestStack(Request $request) {
    while ($this->requestStack()->getCurrentRequest()) {
      $this->storedRequests[] = $this->requestStack()->pop();
    }
    $this->requestStack()->push($request);
  }

  /**
   * Restore all saved requests on the stack.
   */
  protected function restoreRequestStack() {
    $this->requestStack()->pop();
    foreach ($this->storedRequests as $request) {
      $this->requestStack()->push($request);
    }
  }

  protected function requestStack() {
    if (!$this->requestStack) {
      $this->requestStack = \Drupal::service('request_stack');
    }
    return $this->requestStack;
  }

  protected function currentPathStack() {
    if (!$this->currentPathStack) {
      $this->currentPathStack = \Drupal::service('path.current');
    }
    return $this->currentPathStack;
  }

  protected function pathProcessor() {
    if (!$this->pathProcessor) {
      $this->pathProcessor = \Drupal::service('path_processor_manager');
    }
    return $this->pathProcessor;
  }

  protected function router() {
    if (!$this->router) {
      $this->router = \Drupal::service('router');
    }
    return $this->router;
  }
}
