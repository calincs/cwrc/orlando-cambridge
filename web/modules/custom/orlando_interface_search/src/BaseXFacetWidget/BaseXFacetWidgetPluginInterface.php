<?php

namespace Drupal\orlando_interface_search\BaseXFacetWidget;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;

interface BaseXFacetWidgetPluginInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the label for use on the administration pages.
   *
   * @return string
   *   The administration label.
   */
  public function label();

  /**
   * Gets the facet mapping for this widget.
   *
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface
   *   The facet mapping entity.
   */
  public function getFacetMapping(): FacetMappingInterface;

  /**
   * Sets the facet mapping for this widget.
   *
   * @param \Drupal\orlando_interface_search\Entity\FacetMappingInterface $facet_mapping
   *   The facet mapping entity.
   *
   * @return $this;
   */
  public function setFacetMapping(FacetMappingInterface $facet_mapping);

  /**
   * Determine if the widget is valid.
   *
   * @return bool
   *   TRUE when valid FALSE otherwise.
   */
  public function isValid(): bool;

  public function alwaysDisplayed(): bool;

  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array;

  public function getQueryKeys(): array;

  public function getResultKeys(): array;

  public function extractSelectedValues($data);

  public function buildLabelForSelectedValuesString(string $query_key): string;

  public function generateSelectedValuesString($values, string $query_key): string;

  public function buildXQueryString(array $widget_values): string;

  public function validQueryParameters(array $values, array $allowed_values = []): bool;

  public function getFileUris(): array;

}
