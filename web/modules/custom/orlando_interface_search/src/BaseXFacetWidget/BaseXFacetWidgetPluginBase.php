<?php

namespace Drupal\orlando_interface_search\BaseXFacetWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginDependencyTrait;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseXFacetWidgetPluginBase
 *
 * @see plugin_api
 */
abstract class BaseXFacetWidgetPluginBase extends PluginBase implements BaseXFacetWidgetPluginInterface {

  use PluginDependencyTrait;

  /**
   * The facet mapping for this widget.
   *
   * @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface
   */
  protected $facetMapping;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    if (($configuration['#facet_mapping'] ?? NULL) instanceof FacetMappingInterface) {
      $this->setFacetMapping($configuration['#facet_mapping']);
      unset($configuration['#facet_mapping']);
    }
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'is_collapsible' => TRUE,
      'is_bordered' => FALSE,
      'always_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return $this->dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFacetMapping(): FacetMappingInterface {
    return $this->facetMapping;
  }

  /**
   * {@inheritdoc}
   */
  public function setFacetMapping(FacetMappingInterface $facet_mapping) {
    $this->facetMapping = $facet_mapping;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function alwaysDisplayed(): bool {
    return !empty($this->configuration['always_display']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['is_collapsible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is the widget collapsible?'),
      '#default_value' => !empty($this->configuration['is_collapsible']),
    ];
    $form['is_bordered'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is the widget bordered?'),
      '#default_value' => !empty($this->configuration['is_bordered']),
    ];
    $form['always_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always display?'),
      '#default_value' => !empty($this->configuration['always_display']),
      '#description' => $this->t('Whether this display should be always displayed regardless of the basex facets results.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(array &$form = [], FormStateInterface $form_state = NULL, array $results = []): array {
    return $this->formElementInit();
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryKeys(): array {
    return ['value' => $this->getFacetMapping()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultKeys(): array {
    return [$this->getFacetMapping()->id()];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function extractSelectedValues($data);

  /**
   * {@inheritdoc}
   */
  public function buildLabelForSelectedValuesString(string $query_key): string {
    return $this->facetMapping->label();
  }

  /**
   * {@inheritdoc}
   */
  abstract public function generateSelectedValuesString($values, string $query_key): string;

  /**
   * {@inheritdoc}
   */
  public function getFileUris(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function processedMappings();

  private function formElementInit() {
    $title = $this->t('@title', [
      '@title' => $this->facetMapping->label(),
    ]);
    $element = ['#tree' => TRUE];
    if (!empty($this->configuration['is_collapsible'])) {
      $element['#type'] = 'fieldset';
      $element['#title'] = $title;
      $element['#collapsible'] = TRUE;
      $element['value']['#title_display'] = 'invisible';
    }
    else {
      $element['#type'] = 'container';
    }

    $element['#attributes']['class'][] = 'oi-search-basex-facet-widget';
    $element['#attributes']['class'][] = 'oi-search-basex-facet-widget--' . Html::cleanCssIdentifier($this->getPluginId());

    $element['value']['#title'] = $title;
    $element['value']['#bordered'] = !empty($this->configuration['is_bordered']);
    return $element;
  }

}
