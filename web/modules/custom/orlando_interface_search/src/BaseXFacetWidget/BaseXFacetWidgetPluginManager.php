<?php

namespace Drupal\orlando_interface_search\BaseXFacetWidget;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXFacetWidget;

/**
 * Manages Basex facet plugins.
 *
 * @see \Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXFacetWidget
 * @see \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginInterface
 * @see \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginBase
 * @see plugin_api
 */
class BaseXFacetWidgetPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/orlando_interface_search/FacetWidget', $namespaces, $module_handler, BaseXFacetWidgetPluginInterface::class, OrlandoInterfaceSearchBaseXFacetWidget::class);

    $this->alterInfo('orlando_interface_search_basex_facet_widget_info');
    $this->setCacheBackend($cache_backend, 'orlando_interface_search_basex_facet_widgets');
  }

}
