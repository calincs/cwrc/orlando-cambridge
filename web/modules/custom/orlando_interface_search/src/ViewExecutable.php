<?php

namespace Drupal\orlando_interface_search;

use Drupal\orlando_interface_search\Cache\BasexResultsCache;

class ViewExecutable {

  public $executed = FALSE;

  public function execute() {
    if (!empty($this->executed)) {
      return TRUE;
    }

    $this->executed = TRUE;
  }

}
