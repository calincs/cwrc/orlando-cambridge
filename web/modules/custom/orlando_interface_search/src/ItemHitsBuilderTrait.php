<?php

namespace Drupal\orlando_interface_search;

use Drupal\bibcite_entity\Entity\ReferenceInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\RendererInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\taxonomy\TermInterface;

trait ItemHitsBuilderTrait {

  protected $paragraphStorage;

  /**
   * @var \Drupal\paragraphs\ParagraphViewBuilder
   */
  protected $paragraphViewBuilder;

  protected $bibciteReferenceViewBuilder;

  /**
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  protected function buildDocumentHitItems(array $value, $is_item_hits_build = FALSE): array {
    $hit_paragraphs = [];
    /** @var \Drupal\orlando_interface_ingestion\WrappedEntities\WrappedEntity $wrapped_entity */
    $wrapped_entity = $value['_wrapped_entity'];
    $entity = $wrapped_entity ? $wrapped_entity->getEntity() : NULL;
    $entity_type_id = $entity ? $entity->getEntityTypeId() : '';
    $entity_id = $entity ? $entity->id() : NULL;
    /** @var \Drupal\orlando_interface_search\Item\Document $item */
    $item = $value['_item'];
    if ($entity_type_id == 'node' || $entity_type_id == 'profile_event') {
      // Helper var to allow us to skip the initial item during item hits build
      // ajax callback.
      $i = 0;
      // Variable to track processed paragraphs.
      $processed_paragraph_ids = [];
      foreach ($item->getHits() as $hit) {
        $hit_id = $hit->getId();
        if (!$hit_id) {
          continue;
        }

        /** @var \Drupal\paragraphs\ParagraphInterface[] $paragraphs */
        $paragraphs = $this->paragraphStorage()->loadByProperties([
          'field_id' => $hit_id,
        ]);
        if (!$paragraphs) {
          continue;
        }

        $i++;
        if ($is_item_hits_build && $i === 1) {
          // Let skip the first hit because it was already rendered.
          continue;
        }
        $paragraph = reset($paragraphs);
        $paragraph_id = $paragraph->id();
        if (in_array($paragraph_id, $processed_paragraph_ids, TRUE)) {
          // Skip paragraphs which have been processed.
          continue;
        }

        $processed_paragraph_ids[] = $paragraph_id;
        $build = $this->paragraphViewBuilder()->view($paragraph, 'search_result');
        if ($is_item_hits_build) {
          $build['#attributes']['class'][] = 'tw-mt-3 tw-pt-3 tw-border-t tw-border-dashed tw-border-gray-200';
        }
        $hit_paragraphs[] = $this->highlightBuildView($build, $paragraph);
        if (!$is_item_hits_build) {
          // Only rendering one hit. The other ones are going to be rendered
          // using ajax by the user.
          break;
        }
      }
      if (!$is_item_hits_build && !$this->isAnonymousUser() && $item->getHitCount() > 1) {
        $hit_paragraphs[] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => self::getItemHitsWrapperId($entity_type_id, $entity_id),
            'class' => ['tw-hidden'],
          ],
        ];
      }
    }
    elseif ($entity instanceof TermInterface) {
      $field_items = $entity->get('field_name_variants');
      $hit_names = $item->getHitValues();
      if ($hit_names) {
        $hit_values = [];
        foreach ($field_items as $field_item_index => $field_item) {
          /** @var \Drupal\double_field\Plugin\Field\FieldType\DoubleField $field_item */
          $name = $field_item->getValue()['first'];
          if (in_array($name, $hit_names)) {
            $hit_values[] = $field_item->getValue();
          }
        }
        if ($hit_values) {
          // Override field values.
          $field_items->setValue($hit_values, FALSE);
          $hit_paragraphs[] = $this->highlightBuildView(
            $field_items->view('explore'),
            $field_items->getFieldDefinition()
          );
        }
      }
    }
    elseif ($entity instanceof ReferenceInterface) {
      $hit_paragraphs[] = $this->highlightBuildView(
        $this->bibciteReferenceViewBuilder()
          ->view($entity, 'citation'),
        $entity
      );
    }
    elseif ($entity instanceof ParagraphInterface) {
      $hit_paragraphs[] = $this->highlightBuildView(
        $this->paragraphViewBuilder()->view($entity, 'search_result'),
        $entity
      );
    }

    return $hit_paragraphs;
  }

  protected function highlightBuildView($build, $cacheable_dependency) {
    $search_keys = $this->getSearchKeys();
    if (!$search_keys || !is_array($build)) {
      return $build;
    }

    $markup = $this->renderer->render($build);
    $build = [
      '#type' => 'processed_text',
      '#text' => orlando_interface_search_highlight((string) $markup, $search_keys, ['html' => TRUE]),
      '#format' => 'full_html',
    ];
    $cache = new CacheableMetadata();
    $cache->addCacheableDependency($cacheable_dependency);
    $cache->applyTo($build);
    return $build;
  }

  protected function getSearchKeys() {
    if (!isset($this->searchKeys)) {
      $parameters = $this->currentRequest->query->all();
      $keys = [];
      if (!empty($parameters['keys'])) {
        $keys = is_array($parameters['keys']) ? $parameters['keys'] : explode(' ', trim($parameters['keys']));
      }
      // Removing possible empty search keys.
      $this->searchKeys = array_values(array_filter($keys));
    }
    return $this->searchKeys;
  }

  private function paragraphStorage() {
    if (!isset($this->paragraphStorage)) {
      $this->paragraphStorage = $this->entityTypeManager
        ->getStorage('paragraph');
    }
    return $this->paragraphStorage;
  }

  private function paragraphViewBuilder() {
    if (!isset($this->paragraphViewBuilder)) {
      $this->paragraphViewBuilder = $this->entityTypeManager
        ->getViewBuilder('paragraph');
    }
    return $this->paragraphViewBuilder;
  }

  private function bibciteReferenceViewBuilder() {
    if (!isset($this->bibciteReferenceViewBuilder)) {
      $this->bibciteReferenceViewBuilder = $this->entityTypeManager
        ->getViewBuilder('bibcite_reference');
    }
    return $this->bibciteReferenceViewBuilder;
  }

}
