<?php

namespace Drupal\orlando_interface_search\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Orlando Interface Search basex facet widget annotation object.
 *
 * @see \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginManager
 * @see \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginInterface
 * @see \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginBase
 * @see plugin_api
 *
 * @Annotation
 */
class OrlandoInterfaceSearchBaseXFacetWidget extends Plugin {

  /**
   * The facet plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the facet plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
