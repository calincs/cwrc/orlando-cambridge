<?php

namespace Drupal\orlando_interface_search\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Orlando Interface Search baseX Xquery variable annotation object.
 *
 * @see \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginManager
 * @see \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginInterface
 * @see \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase
 * @see plugin_api
 *
 * @Annotation
 */
class OrlandoInterfaceSearchBaseXXqueryVariable extends Plugin {

  /**
   * The xquery variable plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the xquery variable plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * The reserved flag for the xquery variable.
   *
   * @var bool
   */
  public $reserved;

  /**
   * The "allow empty" flag for variable which can be sent empty.
   *
   * @var bool
   */
  public $allow_empty;

  /**
   * Weight of the xquery variable.
   *
   * @var int
   */
  public $weight;

}
