<?php

namespace Drupal\orlando_interface_search\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormState;
use Drupal\orlando_interface_search\Entity\FacetMappingInterface;
use Drupal\orlando_interface_search\Form\FacetMappingWidgetPreviewForm;

class FacetMappingController extends ControllerBase {

  public function page(FacetMappingInterface $basex_facet_mapping) {
    $render = [
      'metadata' => [
        '#theme' => 'basex_facet_mapping',
        '#facet_mapping' => $basex_facet_mapping,
      ],
    ];

    $form_state = new FormState();
    $facet_widget = $basex_facet_mapping->isEnabled() ? $basex_facet_mapping->getFacetWidget() : NULL;
    $form_state->set('facet_widget', $facet_widget);
    $render['widget_form'] = $this->formBuilder()->buildForm(FacetMappingWidgetPreviewForm::class, $form_state);
    return $render;
  }

  /**
   * Returns the page title for a facet mapping's "View" tab.
   *
   * @param \Drupal\orlando_interface_search\Entity\FacetMappingInterface $basex_facet_mapping
   *   The facet mapping that is displayed.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(FacetMappingInterface $basex_facet_mapping) {
    return new FormattableMarkup('@title', ['@title' => $basex_facet_mapping->label()]);
  }

}
