<?php

namespace Drupal\orlando_interface_search\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\orlando_interface_search\ItemHitsBuilderTrait;
use Drupal\orlando_interface_search\RequestSwapperTrait;
use Drupal\orlando_interface_search\ResultsRetrieverTrait;
use Drupal\orlando_interface_search\SearchResultsLazyBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ResultItemsHitsController extends ControllerBase {

  use RequestSwapperTrait;
  use ResultsRetrieverTrait;
  use ItemHitsBuilderTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $controller = parent::create($container);
    $controller->currentPathStack = $container->get('path.current');
    $controller->requestStack = $container->get('request_stack');
    $controller->router = $container->get('router');
    $controller->pathProcessor = $container->get('path_processor_manager');
    $controller->queryHelper = $container->get('orlando_interface_search.query');
    $controller->pagerParameters = $container->get('pager.parameters');
    $controller->entityTypeManager = $container->get('entity_type.manager');
    $controller->renderer = $container->get('renderer');
    return $controller;
  }

  public function ajaxCallback(Request $request, $entity_type_id, $entity_id, $item_id, $item_hit_count, $item_search_results_index, $nojs) {
    $query = $request->query->all();
    // Removing ajax related query parameter.
    if (isset($query['_wrapper_format'])) {
      unset($query['_wrapper_format']);
    }
    $url = Url::fromRoute('orlando_interface_search.page', [], [
      'query' => $query,
      'absolute' => TRUE,
    ]);
    $this->requestSwapWithPath($request, $url->toString(TRUE)->getGeneratedUrl());

    $this->searchExecute($this->requestStack()->getCurrentRequest());
    $search_result = $this->queryHelper()->viewResult[$item_search_results_index] ?? [];
    if ($search_result) {
      // Making sure that the current request property is instantiated with the
      // proper stack.
      $this->currentRequest = $this->requestStack()->getCurrentRequest();
      // Force the search keys to property to be populated before we restore
      // the ajax request.
      $this->getSearchKeys();
      $this->restoreRequestStack();
      $output = [
        '#type' => 'container',
        'hits' => $this->buildDocumentHitItems($search_result, TRUE),
      ];
    }
    else {
      $this->restoreRequestStack();
      $output = '<p class="tw-text-red-200 tw-font-bold">' . $this->t("Something went wrong and we couldn't retrieve the hits!") . '</p>';
    }

    if ($nojs !== 'ajax') {
      return new Response($output);
    }

    $wrapper_id = SearchResultsLazyBuilder::getItemHitsWrapperId($entity_type_id, $entity_id);
    $response = new AjaxResponse();
    $response->addCommand(new AppendCommand('#' . $wrapper_id, $output, ['oi_search_item_hits' => 'true']));
    return $response;
  }

}
