<?php

namespace Drupal\orlando_interface_search\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\orlando_interface_search\Event\ResultsSummaryEvent;
use Drupal\orlando_interface_search\Event\SearchEvents;
use Drupal\orlando_interface_search\ExtractSearchTermsTrait;
use Drupal\orlando_interface_search\Form\TagSearchForm;
use Drupal\orlando_interface_search\ResultsRetrieverTrait;
use Drupal\orlando_interface_search\Utility\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ResultsController extends ControllerBase {

  use ResultsRetrieverTrait;
  use ExtractSearchTermsTrait;

  const NUM_PER_PAGE = 25;

  protected RequestStack $requestStack;

  /**
   * The event dispatcher service.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs the ResultsController instance.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\orlando_interface_search\Utility\QueryHelper $query_helper
   *   The query helper service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(RequestStack $request_stack, QueryHelper $query_helper, FormBuilderInterface  $form_builder, EventDispatcherInterface $event_dispatcher) {
    $this->requestStack = $request_stack;
    $this->queryHelper = $query_helper;
    $this->formBuilder = $form_builder;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('orlando_interface_search.query'),
      $container->get('form_builder'),
      $container->get('event_dispatcher')
    );
  }

  public function page() {
    $num_per_page = self::NUM_PER_PAGE;
    $current_request = $this->requestStack->getCurrentRequest();
    $parameters = $current_request->query->all();

    // Executing the search to get the total number of results.
    $this->searchExecute($current_request);

    // Search execute manipulate the query and sometimes remove the page for
    // caching purpose. Let ensure that the page parameter value that was saved
    // before the search is executed is always the same.
    if (!empty($parameters['page'])) {
      $current_request->query->set('page', $parameters['page']);
    }

    // Now that we have the total number of results, initialize the pager.
    /** @var \Drupal\Core\Pager\PagerManagerInterface $pager_manager */
    $pager_manager = \Drupal::service('pager.manager');
    $total_rows = $this->queryHelper()->viewTotalRows;
    $pager_manager->createPager($total_rows, $num_per_page);

    // Content top.
    $build['content_top'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'tw-pb-6',
          'tw-space-y-3',
        ],
      ],
      'top' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'tw-flex',
            'tw-flex-col',
            'tw-space-y-4',
            'tw-items-baseline',
            'sm:tw-items-center',
            'sm:tw-place-content-between',
            'sm:tw-flex-row',
            'sm:tw-space-y-0',
          ]
        ],
        'result_summary' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->getResultsSummary($parameters, $total_rows),
        ],
        'sort' => $this->formBuilder()->getForm(TagSearchForm::class),
      ],
      'bottom' => [
        '#lazy_builder' => [
          'orlando_interface_search.results_builder:renderFilterItemsClear',
          [],
        ],
        '#create_placeholder' => TRUE,
      ],
    ];

    // Create a render array with the search results.
    $build['content_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'oi-search--results-content--wrapper',
          'tw-pt-8',
          'tw-border-t',
          'tw-border-gray-200',
          'tw-divide-gray-200',
          'tw-space-y-5',
          'tw-divide-y',
        ],
      ],
      '#attached' => [
        'library' => ['orlando_interface/lazy-builder-placeholder'],
      ],
    ];
    $build['content_wrapper']['results'] = [
      '#theme' => 'orlando_interface_search__results',
      '#items' => $this->getItems(),
      '#cache' => [
        'contexts' => [
          'user.roles',
          'url',
          'url.query_args',
        ],
      ],
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];

    $build += [
      '#cache' => [
        'contexts' => [
          'url',
          'url.query_args',
        ],
      ],
    ];
    return $build;
  }

  private function getItems() {
    $results = $this->queryHelper()->viewResult;
    $total_items = count($results);
    $list = [];
    foreach ($results as $position => $value) {
      $list[] = [
        '#lazy_builder' => [
          'orlando_interface_search.results_builder:renderResultItem',
          [$position, $total_items],
        ],
        '#create_placeholder' => TRUE,
      ];
    }
    return $list;
  }

  private function getResultsSummary(array $parameters, $total_rows) {
    $args = $args = ['@count' => $total_rows ?: 0];
    if (!empty($parameters['keys'])) {
      $message = '<span class="tw-font-bold">@count</span> results for <span class="tw-font-bold">@keys</span>';
      $args['@keys'] = Xss::filter($this->getSearchKeys($parameters['keys'], ', '));
    }
    else {
      $message = '<span class="tw-font-bold">@count</span> results';
    }
    $event = new ResultsSummaryEvent($message, $args, $parameters);
    $this->eventDispatcher->dispatch($event, SearchEvents::RESULT_SUMMARY);
    return $this->t($event->getMessage(), $event->getArgs());
  }

}
