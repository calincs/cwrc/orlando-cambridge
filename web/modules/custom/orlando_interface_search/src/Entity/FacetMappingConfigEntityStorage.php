<?php

namespace Drupal\orlando_interface_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

class FacetMappingConfigEntityStorage extends ConfigEntityStorage {

  /**
   * Loads enabled facet mappings.
   *
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface[]
   *   An array of facet mapping configuration keyed by their machine names.
   */
  public function loadEnabledMappings() {
    $mappings = [];
    foreach ($this->loadActives() as $mapping) {
      if (!$mapping->isEnabled()) {
        continue;
      }
      $mappings[$mapping->id()] = $mapping;
    }
    return $mappings;
  }

  /**
   * Loads enabled facet mappings by xquery variable.
   *
   * @param string $xquery_variable
   *   The xquery variable.
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface[]
   *   An array of facet mapping configuration keyed by their machine names.
   */
  public function loadEnabledMappingsByXqueryVariables(string $xquery_variable) {
    $mappings = [];
    foreach ($this->loadActives('xquery_variable', $xquery_variable) as $mapping) {
      if (!$mapping->isEnabled()) {
        continue;
      }
      $mappings[$mapping->id()] = $mapping;
    }
    return $mappings;
  }

  /**
   * Loads active facet mappings.
   *
   * @return \Drupal\orlando_interface_search\Entity\FacetMappingInterface[]
   *   An array of facet mapping configuration keyed by their machine names.
   */
  public function loadActives(string $field = '', string $value = '') {
    $query = $this->getQuery()
      ->condition('status', 1);
    if ($field && $value) {
      $query->condition($field, $value);
    }
    $ids = $query->sort('weight')->execute();
    return $this->loadMultiple($ids);
  }

}
