<?php

namespace Drupal\orlando_interface_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\orlando_interface_ingestion\Plugin\TypedRepositories\TaxonomyTermRepository;
use Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginInterface;
use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginInterface;
use Drupal\orlando_interface_search\OrlandoInterfaceSearchException;

/**
 * Defines the facet mappings entity.
 *
 * @ConfigEntityType(
 *   id = "basex_facet_mapping",
 *   label = @Translation("Basex facet Mapping"),
 *   label_collection = @Translation("Basex facet Mappings"),
 *   label_singular = @Translation("Basex facet mapping"),
 *   label_plural = @Translation("Basex facet mappings"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Basex facet mapping",
 *     plural = "@count Basex facet mappings",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\orlando_interface_search\Controller\FacetMappingsListBuilder",
 *     "storage" = "Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage",
 *     "form" = {
 *       "add" = "Drupal\orlando_interface_search\Form\FacetMappingForm",
 *       "edit" = "Drupal\orlando_interface_search\Form\FacetMappingForm",
 *       "delete" = "Drupal\orlando_interface_search\Form\FacetMappingDeleteConfirmForm"
 *     }
 *   },
 *   config_prefix = "basex_facet_mapping",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "weight" = "weight",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/search/basex/facets-mappings/add",
 *     "canonical" = "/admin/config/search/basex/facets-mappings/manage/{basex_facet_mapping}",
 *     "edit-form" = "/admin/config/search/basex/facets-mappings/manage/{basex_facet_mapping}/edit",
 *     "delete-form" = "/admin/config/search/basex/facets-mappings/manage/{basex_facet_mapping}/delete",
 *   },
 *   config_export = {
 *     "id",
 *     "title",
 *     "weight",
 *     "status",
 *     "source",
 *     "source_entity_type_id",
 *     "xquery_variable",
 *     "facet_widget",
 *     "facet_widget_config",
 *   }
 * )
 */
class FacetMapping extends ConfigEntityBase implements FacetMappingInterface {

  /**
   * The ID of the facet mapping.
   *
   * @var string
   */
  protected $id;

  /**
   * The displayed name of the facet mapping.
   *
   * @var string
   */
  protected $title;

  /**
   * @var int
   */
  protected $weight = 0;

  /**
   * The ID of the facet widget plugin.
   *
   * @var string
   */
  protected $facet_widget;

  /**
   * The mapping source of the data displayed by the facet.
   *
   * @var string
   */
  protected $source = 'file';

  /**
   * The mapping source entity type id when source is entity.
   *
   * @var string
   */
  protected $source_entity_type_id = '';

  /**
   * The facet widget plugin configuration.
   *
   * @var array
   */
  protected $facet_widget_config = [];

  /**
   * The baseX xquery variable the mapping will be added to.
   *
   * @var string
   */
  protected $xquery_variable = 'filters';

  /**
   * The facet widget plugin.
   *
   * @var \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginInterface
   */
  protected $facetWidgetPlugin;

  /**
   * The xquery variable plugin.
   *
   * @var \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginInterface
   */
  protected $xqueryVariablePlugin;

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    $status = parent::status();
    if ($this->getSource() === 'file') {
      $uri = $this->getFileUri();
      $file_exists = $uri && is_file($uri);
      return  $status && $file_exists && $this->getFacetWidget()->isValid();
    }
    return $status && !empty($this->getSourceEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public function getFacetWidgetId(): string {
    return $this->facet_widget ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setFacetWidgetId(string $facet_widget_id) {
    $this->facet_widget = $facet_widget_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFacetWidget(): BaseXFacetWidgetPluginInterface {
    if (!$this->facetWidgetPlugin) {
      $config = $this->facet_widget_config;
      $config['#facet_mapping'] = $this;
      if (!($this->facetWidgetPlugin = $this->facetWidgetPluginManager()->createInstance($this->getFacetWidgetId(), $config))) {
        $facet_widget_id = $this->getFacetWidgetId();
        $title = $this->label();
        throw new OrlandoInterfaceSearchException(sprintf('The facet widget with ID "%s" could not be retrieved for facet mapping "%s"', $facet_widget_id, $title));
      }
    }
    return $this->facetWidgetPlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getFacetWidgetConfig(): array {
    return $this->facet_widget_config;
  }

  /**
   * {@inheritdoc}
   */
  public function setFacetWidgetConfig(array $facet_widget_config) {
    $this->facet_widget_config = $facet_widget_config;
    if ($this->facetWidgetPlugin && $this->getFacetWidget()->getConfiguration() !== $facet_widget_config) {
      $this->getFacetWidget()->setConfiguration($facet_widget_config);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasValidFacetWidget(): bool {
    $facet_widget_definition = $this->facetWidgetPluginManager()->getDefinition($this->getFacetWidgetId(), FALSE);
    return !empty($facet_widget_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getSource(): string {
    return $this->source;
  }

  /**
   * {@inheritdoc}
   */
  public function setSource(string $source) {
    $this->source = $source;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceEntityTypeId(): string {
    return $this->source_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setSourceEntityTypeId(string $id) {
    $this->source_entity_type_id = $id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getXqueryVariable(): string {
    return $this->xquery_variable;
  }

  public function getXqueryVariablePlugin(): BaseXXqueryVariablePluginInterface {
    if (!$this->xqueryVariablePlugin) {
      if (!($this->xqueryVariablePlugin = $this->xqueryVariablePluginManager()->createInstance($this->getXqueryVariable()))) {
        throw new OrlandoInterfaceSearchException(sprintf('The %s facet mapping with "%s" as ID xquery variable plugin could not be retrieved', $this->label(), $this->id()));
      }
    }
    return $this->xqueryVariablePlugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setXqueryVariable(string $value) {
    $this->xquery_variable = $value;
    return $this;
  }

  public function getFileUri(): string {
    $id = $this->id();
    if (!$id || $this->source !== 'file') {
      return '';
    }
    return 'private://orlando-2-0-c-modelling/entities/' . $id . '_mapping.xml';
  }

  public function getMappings(): array {
    $mappings = [];
    if ($this->getSource() === 'entity') {
      return $mappings;
    }
    if ($filename = $this->getFileUri()) {
      $mappings = TaxonomyTermRepository::transformXmlToArray($filename);
    }
    return $mappings;
  }

  /**
   * Helper method to return the plugin manager.
   *
   * @return \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginManager
   *   The plugin manager.
   */
  private function facetWidgetPluginManager() {
    return \Drupal::service('plugin.manager.orlando_interface_search.basex_facet_widget');
  }

  /**
   * Helper method to return the plugin manager.
   *
   * @return \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginManager
   *   The plugin manager.
   */
  private function xqueryVariablePluginManager() {
    return \Drupal::service('plugin.manager.orlando_interface_search.basex_xquery_variable');
  }

}
