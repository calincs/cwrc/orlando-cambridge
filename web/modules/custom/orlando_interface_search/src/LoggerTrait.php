<?php

namespace Drupal\orlando_interface_search;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;

trait LoggerTrait {

  /**
   * The logging channel to use.
   *
   * @var \Psr\Log\LoggerInterface|null
   */
  protected $logger;

  /**
   * Retrieves the logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getDrupalLogger() {
    return $this->logger ?: \Drupal::service('logger.channel.orlando_interface_search');
  }

  /**
   * Sets the logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The new logger.
   *
   * @return \Drupal\orlando_interface_search\LoggerTrait
   */
  public function setDrupalLogger(LoggerInterface $logger) {
    $this->logger = $logger;
    return $this;
  }

  /**
   * Logs an exception.
   *
   * @param \Exception $exception
   *   The exception that is going to be logged.
   * @param string|null $message
   *   (optional) The message to display in the log, which can use variables
   *   retrieved from the exception (like "%type" or "@message"). Or NULL to use
   *   the default message.
   * @param array $variables
   *   (optional) Array of variables to replace in the message when it is
   *   displayed, or NULL if the message should not be translated. The normal
   *   patterns for translation variables can be used.
   * @param int $severity
   *   (optional) The severity of the message, as per RFC 3164.
   * @param string|null $link
   *   (optional) A link to associate with the message, if any.
   *
   * @see watchdog_exception()
   * @see \Drupal\Core\Utility\Error::decodeException()
   */
  protected function drupalLogException(\Exception $exception, $message = NULL, array $variables = [], $severity = RfcLogLevel::ERROR, $link = NULL) {
    // Use a default value if $message is not set.
    if (empty($message)) {
      $message = '%type: @message in %function (line %line of %file).';
    }

    if ($link) {
      $variables['link'] = $link;
    }

    $variables += Error::decodeException($exception);

    $this->getDrupalLogger()->log($severity, $message, $variables);
  }

}
