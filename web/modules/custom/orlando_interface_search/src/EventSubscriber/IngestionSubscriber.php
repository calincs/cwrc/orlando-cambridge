<?php

namespace Drupal\orlando_interface_search\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orlando_interface_ingestion\Event\IngestionEvent;
use Drupal\orlando_interface_ingestion\Event\IngestionEvents;
use Drupal\orlando_interface_ingestion\Event\IngestionStartEvent;
use Drupal\orlando_interface_ingestion\WrappedEntities\Nodes\AuthorProfile;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class IngestionSubscriber implements EventSubscriberInterface {

  protected $database;

  protected $entityTypeManager;

  protected $fileSystem;

  /**
   * IngestionSubscriber constructor.
   *
   * @param \Drupal\orlando_interface_search\Driver\Database\basex\Connection $connection
   *   The basex database connection.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      IngestionEvents::INGESTION_STARTED => ['onIngestionStart'],
      IngestionEvents::CONNECTION_INGESTION => ['onIngestion'],
      IngestionEvents::ITEM_INGESTION => ['onIngestion'],
      IngestionEvents::ITEM_DELETE => ['onDeletion'],
      IngestionEvents::INGESTION_FINISHED => ['onIngestionFinished'],
    ];
  }

  /**
   * Inserts a document in basex on ingestion.
   *
   * @param \Drupal\orlando_interface_ingestion\Event\IngestionEvent $event
   *   The ingestion event.
   */
  public function onIngestion(IngestionEvent $event) {
    $type = $event->getType();
    $id = $event->getId();
    $this->database->insert([
      'name' => $id,
      'document_type' => $type,
      'filepath' => $event->getFilepath(),
    ]);

    // Inserting author profile embedded events.
    if ($type === 'entry') {
      $entry_dir = AuthorProfile::getAuthorProfileXmlUri($id);
      $entry_dir_realpath = $this->fileSystem()->realpath($entry_dir);
      $pattern = $entry_dir_realpath . '/events/*.{xml}';
      $embedded_events = glob($pattern, GLOB_BRACE);
      foreach ($embedded_events as $embedded_event_path) {
        $info = pathinfo($embedded_event_path);
        if (!$embedded_event_path) {
          continue;
        }
        $this->database->insert([
          'name' => $info['filename'],
          'document_type' => 'embedded_event/' . $id,
          'filepath' => $embedded_event_path,
        ]);
      }
    }
  }

  public function onDeletion(IngestionEvent $event) {
    try {
      $this->database->delete([
        'name' => $event->getId(),
        'document_type' => $event->getType(),
      ]);
    }
    catch (\Exception $e) {}
  }

  public function onIngestionFinished() {
    $this->database->openDatabase();
    $this->database->initSettings();
    $this->database->createIndex();
    $this->database->closeSession();
  }

  public function onIngestionStart(IngestionStartEvent $event) {
    /** @var \Drupal\orlando_interface_search\Entity\FacetMappingConfigEntityStorage $facet_mapping_storage */
    $facet_mapping_storage = $this->entityTypeManager->getStorage('basex_facet_mapping');
    $facet_mappings = $facet_mapping_storage->loadEnabledMappings();
    foreach ($facet_mappings as $mapping) {
      if ($mapping_file_uri = $mapping->getFileUri()) {
        $options['is_mapping'] = TRUE;
        $options['name'] = $mapping->id();
        $options['filepath'] = $mapping_file_uri;
        $this->database->insert($options);
      }

      foreach ($mapping->getFacetWidget()->getFileUris() as $id => $filepath) {
        if (!$filepath) {
          continue;
        }
        $options['name'] = $id . '_' . $options['name'];
        $options['filepath'] = $filepath;
        $this->database->insert($options);
      }
    }
  }

  protected function fileSystem() {
    if (!isset($this->fileSystem)) {
      $this->fileSystem = \Drupal::service('file_system');
    }
    return $this->fileSystem;
  }

}
