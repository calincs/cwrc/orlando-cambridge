<?php

namespace Drupal\orlando_interface_search\EventSubscriber;

use Drupal\cambridge_core_api\Event\StatisticsApiEvents;
use Drupal\cambridge_core_api\Event\StatisticsApiMetadataCollectorEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CambridgeCoreApiStatisticsSubscriber implements EventSubscriberInterface {

  const SEARCH_PAGE_EVENT_CODE = 'SE-RE';

  protected RequestStack $requestStack;

  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      StatisticsApiEvents::METADATA_COLLECTOR => ['onMetadataCollection'],
    ];
  }

  public function onMetadataCollection(StatisticsApiMetadataCollectorEvent $event) {
    $route_match = $event->getRouteMatch();
    if ($route_match->getRouteName() !== 'orlando_interface_search.page') {
      return;
    }

    $current_request = $this->requestStack->getCurrentRequest();
    $search_keys = $current_request->query->get('keys', '');
    $event->setProperty('event_code', static::SEARCH_PAGE_EVENT_CODE);
    $event->setProperty('search_string', (string) $search_keys);
  }

}
