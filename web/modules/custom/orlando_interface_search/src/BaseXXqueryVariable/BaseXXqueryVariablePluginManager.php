<?php

namespace Drupal\orlando_interface_search\BaseXXqueryVariable;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXXqueryVariable;

/**
 * Manages BaseX Xquery Variable plugins.
 *
 * @see \Drupal\orlando_interface_search\Annotation\OrlandoInterfaceSearchBaseXXqueryVariable
 * @see \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginInterface
 * @see \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginBase
 * @see plugin_api
 */
class BaseXXqueryVariablePluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/orlando_interface_search/XqueryVariable', $namespaces, $module_handler, BaseXXqueryVariablePluginInterface::class, OrlandoInterfaceSearchBaseXXqueryVariable::class);

    $this->alterInfo('orlando_interface_search_basex_xquery_variable_info');
    $this->setCacheBackend($cache_backend, 'orlando_interface_search_basex_xquery_variables');
  }

  public function getUnreservedDefinitions() {
    $definitions = [];
    foreach ($this->getSortedDefinitions() as $id => $definition) {
      /** @var \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginInterface $definition */
      if (!empty($definition['reserved'])) {
        continue;
      }
      $definitions[$id] = $definition;
    }
    return $definitions;
  }

  public function getSortedDefinitions() {
    $definitions = $this->getDefinitions();
    uasort($definitions, ['Drupal\Component\Utility\SortArray', 'sortByWeightElement']);
    return $definitions;
  }

  /**
   * Gets plugin instances.
   *
   * @return \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginInterface[]
   *   The instances.
   */
  public function getPluginInstances() {
    $instances = [];
    foreach ($this->getSortedDefinitions() as $id => $definition) {
      try {
        $instances[$id] = $this->createInstance($id);
      }
      catch (PluginException $e) {
        continue;
      }
    }
    return $instances;
  }

}
