<?php

namespace Drupal\orlando_interface_search\BaseXXqueryVariable;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginDependencyTrait;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BaseXXqueryVariablePluginBase extends PluginBase implements BaseXXqueryVariablePluginInterface {

  use PluginDependencyTrait;

  /**
   * The human-readable name of the xquery variable plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  protected $label;

  /**
   * The reserved flag for the xquery variable.
   *
   * @var bool
   */
  protected $reserved;

  /**
   * Weight of the xquery variable.
   *
   * @var int
   */
  protected $weight;

  /**
   * The "allow empty" flag for variable which can be sent empty.
   *
   * @var bool
   */
  protected $allow_empty;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->label = $plugin_definition['label'];
    $this->reserved = !empty($plugin_definition['reserved']);
    $this->allow_empty = !empty($plugin_definition['allow_empty']);
    $this->weight = $plugin_definition['weight'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function isReserved(): bool {
    return $this->reserved;
  }

  /**
   * {@inheritdoc}
   */
  public function allowEmpty(): bool {
    return $this->allow_empty;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function buildXqueryVariable(Connection $connection, array $parameters = []): string;

}
