<?php

namespace Drupal\orlando_interface_search\BaseXXqueryVariable;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\orlando_interface_search\Driver\Database\basex\Connection;

interface BaseXXqueryVariablePluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the label for use on the user interface.
   *
   * @return string
   *   The user interface label.
   */
  public function label();

  /**
   * Determine if the xquery variable is variable and exclude it when others
   * are bulk loaded.
   *
   * @return bool
   *   True when reserved, false otherwise.
   */
  public function isReserved(): bool;

  /**
   * Determines if the variable is allowed to be sent as empty.
   *
   * @return bool
   *   True when empty variable is allowed, false otherwise.
   */
  public function allowEmpty(): bool;

  /**
   * Gets the weight of the xquery variable.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int;

  /**
   * Builds the Xquery variable string.
   *
   * @param \Drupal\orlando_interface_search\Driver\Database\basex\Connection $connection
   *   The Basex database connection.
   * @param array $parameters
   *   The query parameters.
   *
   * @return string
   *   The built xquery string.
   */
  public function buildXqueryVariable(Connection $connection, array $parameters = []): string;

}
