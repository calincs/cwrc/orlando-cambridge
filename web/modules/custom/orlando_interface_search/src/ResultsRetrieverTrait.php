<?php

namespace Drupal\orlando_interface_search;

use Drupal\orlando_interface_search\Controller\ResultsController;
use Symfony\Component\HttpFoundation\Request;

trait ResultsRetrieverTrait {

  /**
   * The query helper service.
   *
   * @var \Drupal\orlando_interface_search\Utility\QueryHelper
   */
  protected $queryHelper;

  /**
   * The pager parameters service.
   *
   * @var \Drupal\Core\Pager\PagerParametersInterface
   */
  protected $pagerParameters;

  public function getResultSetsFromRequest(Request $request) {
    $this->searchExecute($request);
    return $this->queryHelper()->getResultSets();
  }

  protected function searchExecute(Request $request) {
    // Perform the query, using the requested offset from
    // PagerManagerInterface::findPage(). This comes from a URL parameter, so
    // here we are assuming that the URL parameter corresponds to an actual
    // page of results that will exist within the set.
    $page = $this->pagerParameters()->findPage();
    $num_per_page = ResultsController::NUM_PER_PAGE;
    $offset =  $num_per_page * $page;
    $parameters = $request->query->all();
    $query_helper = $this->queryHelper();
    // Perform the query to get the data.
    $query_helper->setSearchParameters($parameters, $offset, $page, $num_per_page)
      ->execute();
  }

  /**
   * Gets the query helper.
   *
   * @return \Drupal\orlando_interface_search\Utility\QueryHelper
   *   The query helper.
   */
  protected function queryHelper() {
    if (!$this->queryHelper) {
      $this->queryHelper = \Drupal::service('orlando_interface_search.query');
    }
    return $this->queryHelper;
  }

  /**
   * @return \Drupal\Core\Pager\PagerParametersInterface
   *   The pager parameters.
   */
  protected function pagerParameters() {
    if (!$this->pagerParameters) {
      $this->pagerParameters = \Drupal::service('pager.parameters');
    }
    return $this->pagerParameters;
  }

}
