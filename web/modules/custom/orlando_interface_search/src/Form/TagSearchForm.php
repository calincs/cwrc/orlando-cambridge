<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TagSearchForm extends BaseSearchForm {

  /**
   * The xquery variable plugin.
   *
   * @var \Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable\Sorting
   */
  protected $sortingXqueryVariablePlugin;

  /**
   * Constructs the TagSearchForm object.
   *
   * @param \Drupal\orlando_interface_search\BaseXXqueryVariable\BaseXXqueryVariablePluginManager $xquery_variable_plugin_manager
   *   The xquery variable plugin manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(BaseXXqueryVariablePluginManager $xquery_variable_plugin_manager) {
    $this->sortingXqueryVariablePlugin = $xquery_variable_plugin_manager->createInstance('sorting');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.orlando_interface_search.basex_xquery_variable')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orlando_interface_search_tag_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('display_plain_text_search', FALSE);
    $form = parent::buildForm($form, $form_state);
    $parameters = $this->getRequest()->query->all();
    $form['#query_parameters'] = $parameters;

    $options = $this->sortingXqueryVariablePlugin->getMapping(TRUE);
    $default_value = $parameters['sort'] ?? 'relevance';
    // Ensure that we only allow supported sorting values.
    if (!is_string($default_value) || !isset($options[$default_value])) {
      $default_value = 'relevance';
    }

    $form['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort By'),
      '#options' => $options,
      '#default_value' => $default_value,
      '#attributes' => [
        'class' => ['tw-ml-2'],
        'onchange' => 'this.form.submit();',
      ],
      '#wrapper_attributes' => [
        'class' => ['tw-flex', 'tw-items-center'],
      ],
    ];
    $form['submit']['#attributes']['class'][] = 'visually-hidden';
    $form['submit']['#attributes']['class'][] = 'tw-sr-only';
    return $form;
  }

  protected function getQueries(array $form, FormStateInterface $form_state): array {
    $queries = $form['#query_parameters'];
    $queries['sort'] = $form_state->getValue('sort');

    // Removing page query since we want the user to return to page 1 after
    // changing the sorting.
    if (isset($queries['page'])) {
      unset($queries['page']);
    }
    return $queries;
  }

}
