<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class FacetMappingWidgetPreviewForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orlando_interface_search__basex_facet_mapping__widget_preview';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\orlando_interface_search\BaseXFacetWidget\BaseXFacetWidgetPluginInterface $facet_widget */
    $facet_widget = $form_state->get('facet_widget');
    if (!$facet_widget) {
      $this->messenger()->addError('No facet widget was loaded!');
      return $form;
    }

    $form_state->set('is_facet_widget_preview', TRUE);
    $form['facet_widget'] = $facet_widget->formElement($form, $form_state, []);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
