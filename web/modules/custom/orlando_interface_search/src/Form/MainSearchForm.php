<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\orlando_interface_search\ExtractSearchTermsTrait;
use Drupal\orlando_interface_search\Plugin\orlando_interface_search\XqueryVariable\Stemming;
use Drupal\orlando_interface_search\Utility\QueryHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MainSearchForm extends BaseSearchForm {

  use ExtractSearchTermsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID_PREFIX . 'main_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_state->set('display_keys_input', TRUE);
    $parameters = $this->getRequest()->query->all();
    $form = parent::buildForm($form, $form_state);

    $form['#attached']['library'][] = 'orlando_interface/search-form';

    $form['submit_top'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => -97,
    ];

    $form['keys']['#weight'] = -99;
    $form['keys']['#required'] = TRUE;
    $form['keys']['#default_value'] = $this->getSearchKeys($parameters['keys'] ?? '', ' ');

    $form['exact_match'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exact match'),
      '#default_value' => Stemming::processStemmingParameterValue($parameters),
      '#return_value' => TRUE,
      '#no_space_y' => TRUE,
      '#weight' => -98,
    ];

    $facet_mappings = $this->getFacetMappings();
    if ($facet_mappings) {
      $facet_results = $this->getQueryHelper()->getResultSets()->getFacets();
      $form['#query_parameters'] = $parameters;
      $form['#facet_results'] = $facet_results;
      $form['#facet_mappings'] = $facet_mappings;
      foreach ($facet_mappings as $type => $facet_mapping) {
        $facet_widget = $facet_mapping->getFacetWidget();
        $result_keys = $facet_widget->getResultKeys();
        $always_display = $facet_widget->alwaysDisplayed();
        $results = [];
        if (count($result_keys) === 1) {
          $results = $facet_results[$result_keys[0]]['items'] ?? [];
          if (!$always_display && !$results) {
            continue;
          }
          $form[$type] = $facet_widget->formElement($form, $form_state, $results);
        }
        else {
          foreach ($result_keys as $result_key) {
            if (!$always_display && !isset($facet_results[$result_key]['items'])) {
              continue;
            }
            $results[$result_key] = $facet_results[$result_key]['items'];
          }
          $form[$type] = $facet_widget->formElement($form, $form_state, $results);
        }
      }
    }

    $form['#cache']['tags'][] = 'config:basex_facet_mapping_list';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getQueries(array $form, FormStateInterface $form_state): array {
    $queries = [
      'keys' => $form_state->getValue('keys'),
      'exact-match' => $form_state->getValue('exact_match'),
    ];
    if (!empty($form['#facet_results'])) {
      /** @var \Drupal\orlando_interface_search\Entity\FacetMappingInterface[] $facet_mappings */
      $facet_mappings = $form['#facet_mappings'];
      foreach ($facet_mappings as $type => $facet_mapping) {
        $widget = $facet_mapping->getFacetWidget();
        if (!isset($form['#facet_results'][$type]) && !$widget->alwaysDisplayed()) {
          continue;
        }
        $raw_values = $form_state->getValue([$type]);
        if (!$raw_values) {
          $user_inputs = $form_state->getUserInput();
          $raw_values = $user_inputs[$type] ?? [];
        }
        $values = $widget->extractSelectedValues($raw_values);
        if ($values) {
          foreach ($widget->getQueryKeys() as $query_key) {
            if (!empty($values[$query_key])) {
              $queries[$query_key] = $values[$query_key];
            }
          }
        }
      }
    }

    // Let put back the sort parameter if it's available.
    if (isset($form['#query_parameters']['sort'])) {
      $queries['sort'] = $form['#query_parameters']['sort'];
    }

    return $queries;
  }

}
