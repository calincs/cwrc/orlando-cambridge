<?php

namespace Drupal\orlando_interface_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for BaseX.
 */
class BaseXSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'orlando_interface_search_basex_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['orlando_interface_search.basex_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('orlando_interface_search.basex_settings');

    $form['using_new_version'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Using new version'),
      '#description' => $this->t('Either this BaseX is the latest version using 6 parameters instead of 5 from the old version.'),
      '#default_value' => (bool) ($settings->get('using_new_version') ?? FALSE),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('orlando_interface_search.basex_settings')
      ->set('using_new_version', (bool) $form_state->getValue('using_new_version'))
      ->save();
  }

}
