<?php

namespace Drupal\orlando_interface_search;

trait ExtractSearchTermsTrait {

  public function getSearchKeys($parameter_keys, $separator) {
    $keys = '';
    if (!empty($parameter_keys)) {
      $keys = is_array($parameter_keys) ? implode($separator, array_values(array_filter($parameter_keys))) : $parameter_keys;
    }
    return trim($keys);
  }

}
