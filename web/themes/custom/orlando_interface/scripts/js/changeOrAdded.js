const fs = require('fs');
const log = require('./log');
const compile = require('./compile');

module.exports = (filePath) => {
  log(`'${filePath}' is being processed.`);
  // Transform the file.
  compile(filePath, function write(code) {
    const fileName = filePath.slice(0, -7)
      .split('/')
      .slice(-1)[0];
    // Write the result to the filesystem.
    fs.writeFile(`${__dirname}/../../assets/js/dist/${fileName}.js`, code, () => {
      log(`'${filePath}' is finished.`);
    });
  });
}
