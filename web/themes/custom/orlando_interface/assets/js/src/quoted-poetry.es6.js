(function(Drupal, once) {

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.oiQuotedPoetry = {
    attach: function (context) {
      once('quotedPoetryBreak', 'span[data-oi-element-style="quote"] br').forEach(
        (br) => {
          if (br.parentNode.classList.contains('tw-block')) {
            return;
          }
          br.parentNode.classList.add('tw-block');
          br.parentNode.classList.add('tw-pl-5');
        },
      );
    }
  };
})(Drupal, once);
