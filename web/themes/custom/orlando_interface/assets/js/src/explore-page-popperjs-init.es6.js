(function(Popper) {
  const enabledPopperElements = document.querySelectorAll('.ois--svg-icon--wrapper [data-popperjs-enabled="true"]');

  const show = function (popperInstance) {
    return function (event) {
      // Make the tooltip visible.
      popperInstance.state.elements.popper.classList.toggle('tw-hidden');
      // Enable the event listeners.
      popperInstance.setOptions({
        modifiers: [{ name: 'eventListeners', enabled: true }],
      });
      // Update its position.
      popperInstance.update();
    };
  }

  const hide = function (popperInstance) {
    return function (event) {
      // Hide the tooltip
      popperInstance.state.elements.popper.classList.toggle('tw-hidden');

      // Disable the event listeners
      popperInstance.setOptions({
        modifiers: [{ name: 'eventListeners', enabled: false }],
      });
    };
  }

  enabledPopperElements.forEach(elt => {
    const reference = document.getElementById(elt.getAttribute('id'));
    const popper = document.getElementById(elt.dataset.popperjsTooltipId);
    const popperInstance = Popper.createPopper(reference, popper,{
      placement: 'bottom',
      modifiers: [
        {
          name: 'offset',
          options: {
            offset: [0, 12],
          },
        },
      ],
    });

    const showEvents = ['mouseenter', 'focus'];
    const hideEvents = ['mouseleave', 'blur'];

    showEvents.forEach(event => {
      elt.addEventListener(event, show(popperInstance));
    });

    hideEvents.forEach(event => {
      elt.addEventListener(event, hide(popperInstance));
    });
  });
})(Popper);
