(function(Drupal, once) {

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.oiXmlDownloadLinkStatisticsPostData = {
    attach: function (context, settings) {
      once('oi-xml-download-link-statistics-post-data-initialized', 'a[data-oi-xml-download-link-statistics-post-data]').forEach(
        (link) => {
          link.addEventListener('click', (e) => {
            let data = {...settings.cambridgeCoreApi.statistics.data};
            data.event_code = 'DL-DI';
            Drupal.cambridgeCoreApi.statistics.postData(settings.cambridgeCoreApi.statistics.url, data);
          });
        },
      );
    }
  };
})(Drupal, once);
