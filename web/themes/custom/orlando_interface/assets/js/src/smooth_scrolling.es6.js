((drupalSettings) => {
  'use strict';

  const default_scroll_to = drupalSettings.orlando_interface.scroll_to;

  function init() {
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
      anchor.addEventListener('click', function (e) {
        e.preventDefault();

        let el = document.querySelector(this.getAttribute('href'));
        el = el ? el : document.getElementById(default_scroll_to);
        if (el) {
          el.scrollIntoView({
            behavior: 'smooth'
          });
        }
      });
    });
  }
  init();
})(drupalSettings);
