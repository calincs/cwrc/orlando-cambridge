<?php

namespace DrupalProject\robo;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class FixturesLogger extends \ArrayObject {

  protected $startedAt;
  protected $finishedAt;
  protected $lastUpdated;
  protected $entityType;
  protected $options;
  protected $items = [];

  public function __construct(string $entity_type, array $options) {
    parent::__construct([], 0, 'ArrayIterator');
    $this['entityType'] = $entity_type;
    $this['options'] = $options;
    $this['startedAt'] = time();
  }

  public function getFinishedAt() {
    return $this['finishedAt'];
  }

  public function getStartedAt() {
    return $this['startedAt'];
  }

  public function getLastUpdated() {
    return $this['lastUpdated'];
  }

  public function getEntityType() {
    return $this['entityType'];
  }

  public function getOptions() {
    return $this['options'];
  }

  public function getItems() {
    return $this['items'];
  }

  public function setFinishedAt($time) {
    $this['finishedAt'] = $time;
    return $this;
  }

  public function setLastUpdated($time) {
    $this['lastUpdated'] = $time;
    return $this;
  }

  public function setOptions(array $options) {
    $this['options'] = json_encode($options);
    return $this;
  }

  public function addItem($item_id, \stdClass $object) {
    if (!isset($this['items'][$item_id])) {
      $this['items'][$item_id] = [];
    }
    $this['items'][$item_id][] = $object;
    return $this;
  }

  public function save() {
    $json = json_encode($this, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    $destination = '/mnt/files/private/orlando-2-0-c-modelling/fixtures-log.json';
    if (is_file($destination)) {
      $fs = new Filesystem();
      $fs->remove($destination);
    }
    file_put_contents($destination, $json);
    return $json;
  }

}
