ARG build_environment=prod
ARG code_dir=./
ARG base_image_tag=8.2.27-1.22.1-1a536f1d-saxon
ARG composer_version=2.5.1
ARG node_version=18.20.6

#
# Stage 1: PHP Dependencies
#
FROM composer:${composer_version} AS composer-build
ARG code_dir
ARG build_environment
ENV COMPOSER_INSTALL_FLAGS \
  --ansi \
  --prefer-dist \
  --no-interaction \
  --ignore-platform-reqs

ENV DRUPAL_COMPOSER_DIRECTORIES \
  web/core \
  web/modules/contrib \
  web/profiles/contrib \
  web/themes/contrib \
  web/libraries \
  drush/contrib

# Add bitbucket and github to known hosts for ssh needs
WORKDIR /root/.ssh
RUN chmod 0600 /root/.ssh \
  && ssh-keyscan -t rsa bitbucket.org >> known_hosts \
  && ssh-keyscan -t rsa github.com >> known_hosts

WORKDIR /app

COPY ${code_dir}/composer.json ${code_dir}/composer.lock ./
COPY ${code_dir}/scripts/clean-library-folders.sh ./scripts/clean-library-folders.sh
COPY ${code_dir}/scripts/robo/FixturesLogger.php ./scripts/robo/FixturesLogger.php
COPY ${code_dir}/patches ./patches

RUN set -eux; \
  flags="${COMPOSER_INSTALL_FLAGS}"; \
  # Excluding dev dependencies when build environment is prod.
  [ "$build_environment" == "prod" ] && flags="${COMPOSER_INSTALL_FLAGS} --no-dev"; \
  # Installing the dependencies.
  composer install $flags \
  # Adding various drupal folder in case composer didn't create them.
  && for dir in $DRUPAL_COMPOSER_DIRECTORIES; do \
    if [ ! -d $dir ]; then \
      mkdir -p $dir; \
    fi; \
  done;

#
# Stage 2: Orlando Interface custom theme front end build
#
FROM node:${node_version} AS frontend
ARG code_dir

WORKDIR /app

COPY ${code_dir}/web/themes/custom/orlando_interface/package.json \
  ${code_dir}/web/themes/custom/orlando_interface/yarn.lock \
  # copying postcss and tailwind config scripts.
  ${code_dir}/web/themes/custom/orlando_interface/tailwind.config.js \
  ${code_dir}/web/themes/custom/orlando_interface/postcss.config.js ./

COPY ${code_dir}/web/themes/custom/orlando_interface/scripts/js ./scripts/js

COPY ${code_dir}/web/themes/custom/orlando_interface/assets/css/src ./assets/css/src
COPY ${code_dir}/web/themes/custom/orlando_interface/assets/js/src ./assets/js/src

RUN mkdir -p assets/css/dist assets/js/dist; \
  yarn install; \
  yarn build

#
# Stage 3: The app
#
FROM registry.gitlab.com/nikathone/drupal-docker-good-defaults/php-nginx:${base_image_tag} AS base
ARG code_dir
ARG app_runner_user=drupal
ARG app_runner_user_id=1000
ARG app_runner_group=drupal
ARG app_runner_group_id=1000

ENV PATH=${PATH}:${APP_ROOT}/vendor/bin \
  PHP_EXPOSE_PHP=Off \
  PHP_FPM_USER=${app_runner_user:-drupal} \
  PHP_FPM_GROUP=${app_runner_group:-drupal} \
  NGINX_LISTEN_PORT=8080 \
  DEFAULT_USER=${app_runner_user:-drupal} \
  APP_NAME=drupal \
  AUTO_INSTALL=false \
  APP_RUNNER_USER=${app_runner_user:-drupal} \
  APP_RUNNER_USER_ID=${app_runner_user_id:-1000} \
  APP_RUNNER_GROUP=${app_runner_group:-drupal} \
  APP_RUNNER_GROUP_ID=${app_runner_group_id:-1000} \
  PHP_MEMORY_LIMIT=1024M \
  PHP_MAX_INPUT_TIME=900

# Overwrite the default virtual host innherited from the base image.
COPY ${code_dir}/scripts/templates/vhost.conf.tmpl /confd_templates/templates/vhost.conf.tmpl

## Manually reinstall GD to fix jpeg support.
#RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp; \
#    docker-php-ext-install -j "$(nproc)" gd

# Copy custom configuration template files for PHP and NGINX
RUN set -xe; \
  mkdir -p /etc/confd && cp -R /confd_templates/* /etc/confd/; \
  mkdir -p /etc/nginx/conf.d; \
  # Copy custom executable scripts for drupal including the default entrypoint.
  mv /drupal/bin/* /usr/local/bin/; \
  # Make sure docker-webserver-entrypoint and other scripts are executable
  chmod -R +x /usr/local/bin/; \
  # apply custom configurations based on confd templates
  /usr/local/bin/confd -onetime -backend env \
  # clean the content of confd so that the app can add it's templates later in the process
  && rm -rf /etc/confd/* \
  # Move the .env template file for the drupal app. We then run confd in the docker
  # entrypoint to place it under <codebase>/.env.
  && cp -R /drupal/confd/* /etc/confd/ && rm -rf /drupal/confd

# Add and configure app runner user
RUN set -xe; \
  # Delete existing user/group if uid/gid occupied.
  existing_group=$(getent group "${APP_RUNNER_GROUP_ID}" | cut -d: -f1); \
  [ -n "${existing_group}" ] && delgroup "${existing_group}"; \
  existing_user=$(getent passwd "${APP_RUNNER_USER_ID}" | cut -d: -f1); \
  [ -n "${existing_user}" ] && deluser "${existing_user}"; \
  \
  # Ensure app runner user/group exists
  addgroup --system --gid ${APP_RUNNER_GROUP_ID} ${APP_RUNNER_GROUP}; \
  adduser --system --disabled-password --ingroup ${APP_RUNNER_GROUP} --shell /bin/bash --uid ${APP_RUNNER_USER_ID} ${APP_RUNNER_USER}; \
  usermod --append --groups ${NGINX_USER_GROUP} ${APP_RUNNER_USER} \
  # Other app runner user related configurations. See bin/config_app_runner_user
  && config_app_runner_user \
  \
  # Make sure that files dir have proper permissions.
  && mkdir -p ${FILES_DIR}/public; \
  mkdir -p ${FILES_DIR}/private; \
  # Ensure the files dir is owned by nginx user
  chown -R ${NGINX_USER}:${NGINX_USER_GROUP} ${FILES_DIR}; \
  # Ensure app user has ownership of the main app root folder folder.
  mkdir -p ${APP_ROOT} && chown -R ${APP_RUNNER_USER}:${APP_RUNNER_GROUP} ${APP_ROOT}

EXPOSE $NGINX_LISTEN_PORT
ENTRYPOINT [ "docker-webserver-entrypoint" ]
CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

WORKDIR ${APP_ROOT}

# Copy code and composer generated files and directories
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/vendor ./vendor
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/core ./web/core
# Scalfolded files. @TODO Find a better way to load all of them
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/index.php \
  /app/web/robots.txt \
  /app/web/update.php  ./web/
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/sites/default/default.services.yml ./web/sites/default/default.services.yml

COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/modules/contrib ./web/modules/contrib
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/themes/contrib ./web/themes/contrib
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/profiles/contrib ./web/profiles/contrib
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/web/libraries ./web/libraries
COPY --from=composer-build --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} /app/drush/contrib ./drush/contrib

# Copy theme css and js artifacts.
COPY --from=frontend --chown=${APP_RUNNER_USER}:${NGINX_USER_GROUP} /app/assets ./web/themes/custom/orlando_interface/assets

# Copy entire code files. The .dockerignore file ensures that ignored paths are
# not copied from the host machine into the image.
COPY --chown=${APP_RUNNER_USER}:${APP_RUNNER_GROUP} ${code_dir} ./

# Overwrite the entrypoint inherited from the base image.
RUN rm /usr/local/bin/docker-webserver-entrypoint; \
 mv scripts/docker-webserver-entrypoint.sh /usr/local/bin/docker-webserver-entrypoint; \
 chmod +x /usr/local/bin/docker-webserver-entrypoint; \
 # Overwrite the app.env.tmpl inherited from the base image to include basex
 # envs.
 rm /etc/confd/templates/app.env.tmpl; \
 mv scripts/templates/app.env.tmpl /etc/confd/templates/app.env.tmpl; \
 # Remove virtual host template.
 if [ -f "scripts/templates/vhost.conf.tmpl" ]; then \
   rm scripts/templates/vhost.conf.tmpl; \
 fi

#
# Stage 4: The production setup
#
FROM base AS prod
ENV APP_ENV=prod

# Using the production php.ini
RUN mv ${PHP_INI_DIR}/php.ini-production ${PHP_INI_DIR}/php.ini; \
  # Remove the confd templates altogether.
  rm -rf /confd_templates

USER ${APP_RUNNER_USER}

#
# Stage 5: The dev setup
#
FROM base AS dev
ARG composer_version
ARG node_version
ARG PHP_XDEBUG
ARG PHP_XDEBUG_DEFAULT_ENABLE
ARG PHP_XDEBUG_REMOTE_CONNECT_BACK
ARG PHP_XDEBUG_REMOTE_HOST
ARG PHP_IDE_CONFIG

ENV APP_ENV=dev \
  DEBUG=true

# Install development tools.
RUN pecl install xdebug-3.3.2; \
  docker-php-ext-enable xdebug; \
  # Adding the dev php.ini
  mv ${PHP_INI_DIR}/php.ini-development ${PHP_INI_DIR}/php.ini; \
  # Overwrite default configuration for xdebug with our custom one. \
  mv scripts/templates/docker-php-ext-xdebug.ini.tmpl /confd_templates/templates/docker-php-ext-xdebug.ini.tmpl; \
  # Copy xdebug configurations templates.
  cp /confd_templates/conf.d/docker-php-ext-xdebug.ini.toml /etc/confd/conf.d/docker-php-ext-xdebug.ini.toml; \
  cp /confd_templates/templates/docker-php-ext-xdebug.ini.tmpl /etc/confd/templates/docker-php-ext-xdebug.ini.tmpl; \
  # Apply xdebug configurations.
  /usr/local/bin/confd -onetime -backend env \
  # Delete xdebug configuration template files.
  && rm /etc/confd/conf.d/docker-php-ext-xdebug.ini.toml /etc/confd/templates/docker-php-ext-xdebug.ini.tmpl \
  # Remove the confd templates altogether.
  && rm -rf /confd_templates

# Copy composer binary from official Composer build.
COPY --from=composer-build /usr/bin/composer /usr/bin/composer

# Install front end tools needed for development and/or testing
ENV NODE_VERSION=${node_version}

RUN apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -y gnupg \
  && ARCH= && dpkgArch="$(dpkg --print-architecture)" \
  && case "${dpkgArch##*-}" in \
    amd64) ARCH='x64';; \
    ppc64el) ARCH='ppc64le';; \
    s390x) ARCH='s390x';; \
    arm64) ARCH='arm64';; \
    armhf) ARCH='armv7l';; \
    i386) ARCH='x86';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && export GNUPGHOME="$(mktemp -d)" \
  # gpg keys listed at https://github.com/nodejs/node#release-keys
  && set -ex \
  && for key in \
    C0D6248439F1D5604AAFFB4021D900FFDB233756 \
    DD792F5973C6DE52C432CBDAC77ABFA00DDBF2B7 \
    CC68F5A3106FF448322E48ED27F5E38D5B0A215F \
    8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
    890C08DB8579162FEE0DF9DB8BEAB4DFCF555EF4 \
    C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
    108F52B48DB57BB0CC439B2997B01419BD92F80A \
    A363A499291CBBC940DD62E41F10027AF002F8B0 \
  ; do \
      gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
  && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
  && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
  && gpgconf --kill all \
  && rm -rf "$GNUPGHOME" \
  && grep " node-v$NODE_VERSION-linux-$ARCH.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
  && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
  && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
  && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
  # smoke tests
  && node --version \
  && npm --version

# YARN
ENV YARN_VERSION 1.22.22

RUN set -ex \
  && for key in \
    6A010C5166006599AA17F08146C2130DFD2497F5 \
  ; do \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys "$key" || \
    gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" ; \
  done \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz" \
  && curl -fsSLO --compressed "https://yarnpkg.com/downloads/$YARN_VERSION/yarn-v$YARN_VERSION.tar.gz.asc" \
  && gpg --batch --verify yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
  && gpgconf --kill all \
  && rm -rf "$GNUPGHOME" \
  && mkdir -p /opt \
  && tar -xzf yarn-v$YARN_VERSION.tar.gz -C /opt/ \
  && ln -s /opt/yarn-v$YARN_VERSION/bin/yarn /usr/local/bin/yarn \
  && ln -s /opt/yarn-v$YARN_VERSION/bin/yarnpkg /usr/local/bin/yarnpkg \
  && rm yarn-v$YARN_VERSION.tar.gz.asc yarn-v$YARN_VERSION.tar.gz \
  # smoke test
  && yarn --version \
  && rm -rf /tmp/*

# Install chromedriver dependency
RUN apt install --no-install-recommends --no-install-suggests -y libnss3 libx11-xcb-dev \
  && apt-get remove --purge --auto-remove -y gnupg && rm -rf /var/lib/apt/lists/* \
  && mv scripts/templates/core.env ${APP_ROOT}/web/core/.env

EXPOSE 9003

USER ${APP_RUNNER_USER}
